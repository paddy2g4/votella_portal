@extends('layouts.verify_vote_form_master')

@section('content')

<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-transparent" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Details-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Verify Voters</h5>
                <!--end::Title-->
                <!--begin::Separator-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
                <!--end::Separator-->
                <!--begin::Search Form-->
                <div class="d-flex align-items-center" id="kt_subheader_search">
                    <span class="text-dark-50 font-weight-bold" id="kt_subheader_total">service_label</span>
                </div>
                <!--end::Search Form-->
            </div>
            <!--end::Details-->
            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">
                <!--begin::Button-->
                <a href="{{ url()->previous() }}" class="btn btn-default font-weight-bold">Back</a>
                <!--end::Button-->
                <!--begin::Dropdown-->
                {{-- <div class="btn-group ml-2">
                    <button type="button" class="btn btn-primary font-weight-bold">Submit</button>
                    <button type="button" class="btn btn-primary font-weight-bold dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                    <div class="dropdown-menu dropdown-menu-sm p-0 m-0 dropdown-menu-right">
                        <ul class="navi py-5">
                            <li class="navi-item">
                                <a href="#" class="navi-link">
                                    <span class="navi-icon">
                                        <i class="flaticon2-writing"></i>
                                    </span>
                                    <span class="navi-text">Save &amp; continue</span>
                                </a>
                            </li>
                            <li class="navi-item">
                                <a href="#" class="navi-link">
                                    <span class="navi-icon">
                                        <i class="flaticon2-medical-records"></i>
                                    </span>
                                    <span class="navi-text">Save &amp; add new</span>
                                </a>
                            </li>
                            <li class="navi-item">
                                <a href="#" class="navi-link">
                                    <span class="navi-icon">
                                        <i class="flaticon2-hourglass-1"></i>
                                    </span>
                                    <span class="navi-text">Save &amp; exit</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div> --}}
                <!--end::Dropdown-->
            </div>
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container-fluid">
            <!--begin::Card-->
            <div class="card card-custom gutter-b">
                <!--begin::Body-->
                <div class="card-body p-0">

                    <div class="container">

                        <form class="form" id="kt_category_add_form">
                            <br><br>
                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                <h3 class="mb-10 font-weight-bold text-dark">Search Voter/Electorate Details:</h3>
                                <div class="row">

                                    <div class="col-xl-12">
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Voter Name</label>
                                            <div class="col-lg-6 col-xl-6">
                                                    <select class="form-control select2" id="voter_search_val" name="voter_search_val">
                                                        <option value="">Search Voter</option>
                                                        @foreach($voters_list as $item)
                                                            <option value="{{$item->voter_id}}">{{$item->name}} - {{$item->mobile_number}} - {{$item->index_number}}</option>
                                                        @endforeach
                                                    </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xl-12" id="search_btn_div">
                                        @can('search_electorate_details')
                                            <button type="button" id="btn_search_voter" class="btn btn-primary font-weight-bold text-uppercase px-9 py-4 btn_search_voter" style="position:relative; margin: -20px -50px; top:50%; left:50%;">Search</button>
                                        @endcan
                                    </div><br><br><br><br>
                                </div>
                            </div>
                           
                            <div class="pb-5 nominee_class"  id="voter_details_div">
                                <h3 class="mb-10 font-weight-bold text-dark">Registered Voter Details:</h3>

                                <div class="row">

                                    <div class="col-xl-12">
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Name</label>
                                            <div class="col-lg-6 col-xl-6">
                                                <input name="voter_id" id="voter_id" type="hidden" value="">
                                                <input class="form-control form-control-lg form-control-solid" id="voter_name" type="text" maxlength="150" placeholder="Voter's Name" value="" disabled/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="separator separator-dashed my-5"></div>
                                </div>

                                <div class="row">
                                    <div class="col-xl-12">
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Index Number</label>
                                            <div class="col-lg-6 col-xl-6">
                                                <input class="form-control form-control-lg form-control-solid" name="voter_index_number" id="voter_index_number" type="text" maxlength="150" placeholder="Voter's Index Number" value="" disabled/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="separator separator-dashed my-5"></div>
                                </div>

                                <div class="row">
                                    <div class="col-xl-12">
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Phone Number</label>
                                            <div class="col-lg-6 col-xl-6">
                                                <input class="form-control form-control-lg form-control-solid" name="voter_phone" id="voter_phone" type="text" maxlength="150" placeholder="Voter's Identifier" value="" disabled/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="separator separator-dashed my-5"></div>
                                </div>

                                <div class="row">

                                    <div class="col-xl-12">
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Department & Level</label>
                                            <div class="col-lg-6 col-xl-6">
                                                <input class="form-control form-control-lg form-control-solid" name="voter_department_level" id="voter_department_level" type="text" maxlength="150" placeholder="Department and Level of Voter" disabled/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="separator separator-dashed my-5"></div>
                                </div>

                                <div class="row">

                                    <div class="col-xl-12">
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Vote Status</label>
                                            <div class="col-lg-6 col-xl-6">
                                                <input class="form-control form-control-lg form-control-solid" name="voter_vote_status" id="voter_vote_status" type="text" maxlength="150" placeholder="Voter's Vote Status" disabled/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="separator separator-dashed my-5"></div>
                                </div>

                                <div class="row">
                                    <div class="col-xl-12">
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Account Status</label>
                                            <div class="col-lg-6 col-xl-6">
                                                <input class="form-control form-control-lg form-control-solid" name="voter_account_status" id="voter_account_status" type="text" maxlength="150" placeholder="Account Status" disabled/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="separator separator-dashed my-5"></div>
                                </div>

                                <div class="row">
                                    @can('deactivate_electorates')
                                        <div class="col-xl-12" id="search_btn_div">
                                            <button type="button" id="btn_activate_account" class="btn btn-success font-weight-bold text-uppercase px-9 py-4 btn_activate_account" style="position:relative; margin: -20px -50px; top:50%; left:50%;">Activate Access</button>
                                            <button type="button" id="btn_deactivate_account" class="btn btn-danger font-weight-bold text-uppercase px-9 py-4 btn_deactivate_account" style="position:relative; margin: -20px -50px; top:50%; left:50%;">Deactivate Access</button>
                                        </div><br><br><br><br>
                                    @endcan
                                </div>

                            </div>
                            <!--end::Form Wizard Step 3-->
                            
                        </form>

                    </div>

                    <!--end::Wizard-->
                </div>
                <!--end::Body-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>

@endsection




