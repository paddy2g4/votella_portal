@extends('layouts.login_app')

@section('content')

	<div class="login login-2 login-signin-on d-flex flex-column flex-lg-row flex-column-fluid bg-white" id="kt_login">
		<!--begin::Aside-->
		<div class="login-aside order-2 order-lg-1 d-flex flex-row-auto position-relative overflow-hidden">
			<!--begin: Aside Container-->
			<div class="d-flex flex-column-fluid flex-column justify-content-between py-9 px-7 py-lg-13 px-lg-35">
				<!--begin::Logo-->
				{{-- <a href="#" class="text-center pt-2">
					<img src="{{ asset('assets/media/logos/votella_logo.jpg') }}" class="max-h-75px" alt="" />
				</a> --}}
				<h3 class="app_title">VOTELLA</h3>
				<!--end::Logo-->
				<!--begin::Aside body-->
				<div class="d-flex flex-column-fluid flex-column flex-center">
					<!--begin::Signin-->
					<div class="login-form login-signin py-11">
						<!--begin::Form-->
						
						<form method="POST" action="{{ route('login') }}">
							@csrf
							<div class="text-center pb-8">
								<h2 class="font-weight-bolder font-size-h2 font-size-h1-lg" style="color: #06083;">Sign In</h2>
							</div>
							<div class="form-group">
								<label class="font-size-h6 font-weight-bolder text-dark">{{ __('E-Mail Address') }}</label>
								<input id="email" type="email" class="form-control form-control-solid h-auto py-7 px-6 rounded-lg @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="off" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

							</div>
							<!--begin::Form group-->
							<div class="form-group">
								<div class="d-flex justify-content-between mt-n5">
									<label class="font-size-h6 font-weight-bolder text-dark pt-5">{{ __('Password') }}</label>									

									{{-- @if (Route::has('password.request'))
										<a href="{{ route('password.request') }}" class="text-primary font-size-h6 font-weight-bolder text-hover-primary pt-5" id="kt_login_forgot">{{ __('Forgot Your Password?') }}</a>
                                	@endif --}}

								</div>

								<input id="password" type="password" class="form-control form-control-solid h-auto py-7 px-6 rounded-lg @error('password') is-invalid @enderror" name="password" required autocomplete="off">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
							</div>
							<!--end::Form group-->
							<!--begin::Action-->
							<div class="text-center pt-2">
								<button type="submit" id="kt_login_signin_submit" class="btn btn-dark font-weight-bolder font-size-h6 px-8 py-4 my-3">{{ __('Login') }}</button>
							</div>
							<!--end::Action-->
						</form>
						<!--end::Form-->
					</div>
					<!--end::Signin-->
					
					<!--begin::Forgot-->
					<div class="login-form login-forgot pt-11">
						<!--begin::Form-->
						<form class="form" novalidate="novalidate" id="kt_login_forgot_form">
							<!--begin::Title-->
							<div class="text-center pb-8">
								<h2 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">Forgotten Password ?</h2>
								<p class="text-muted font-weight-bold font-size-h4">Enter your email to reset your password</p>
							</div>
							<!--end::Title-->
							<!--begin::Form group-->
							<div class="form-group">
								<input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg font-size-h6" type="email" placeholder="Email" name="email" autocomplete="off" />
							</div>
							<!--end::Form group-->
							<!--begin::Form group-->
							<div class="form-group d-flex flex-wrap flex-center pb-lg-0 pb-3">
								<button type="button" id="kt_login_forgot_submit" class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mx-4">Submit</button>
								<button type="button" id="kt_login_forgot_cancel" class="btn btn-light-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mx-4">Cancel</button>
							</div>
							<!--end::Form group-->
						</form>
						<!--end::Form-->
					</div>
					<!--end::Forgot-->
				</div>
				<!--end::Aside body-->
				
			</div>
			<!--end: Aside Container-->
		</div>
		<!--begin::Aside-->
		<!--begin::Content-->
		<div class="content order-1 order-lg-2 d-flex flex-column w-100 pb-0 login_right_section">
			<!--begin::Title-->
			<div class="d-flex flex-column justify-content-center text-center pt-lg-40 pt-md-5 pt-sm-5 px-lg-0 pt-5 px-7">
				{{-- <h3 class=" app_title">VOTELLA</h3> --}}
				{{-- <p class="opacity-70 app_desc">Voting redefined</p> --}}
			</div>
			<!--end::Title-->
			<!--begin::Image-->
			{{-- <div class="content-img d-flex flex-row-fluid bgi-no-repeat bgi-position-y-bottom bgi-position-x-center" style="background-image: url(assets/media/svg/illustrations/login-visual-2.svg);"></div> --}}
			<!--end::Image-->
		</div>
		<!--end::Content-->
	</div>

@endsection