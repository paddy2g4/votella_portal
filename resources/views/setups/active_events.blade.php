@extends('layouts.votella_tables')

@section('content')


<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-transparent" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-2 mr-5">Events</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">Setup</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">Institutions</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">Events</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
            
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container-fluid">
            <div class="alert alert-custom alert-white alert-shadow gutter-b" role="alert">
                <div class="alert-icon">
                    <span class="svg-icon svg-icon-xl">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24" />
                                <path d="M7.07744993,12.3040451 C7.72444571,13.0716094 8.54044565,13.6920474 9.46808594,14.1079953 L5,23 L4.5,18 L7.07744993,12.3040451 Z M14.5865511,14.2597864 C15.5319561,13.9019016 16.375416,13.3366121 17.0614026,12.6194459 L19.5,18 L19,23 L14.5865511,14.2597864 Z M12,3.55271368e-14 C12.8284271,3.53749572e-14 13.5,0.671572875 13.5,1.5 L13.5,4 L10.5,4 L10.5,1.5 C10.5,0.671572875 11.1715729,3.56793164e-14 12,3.55271368e-14 Z" fill="#000000" opacity="0.3" />
                                <path d="M12,10 C13.1045695,10 14,9.1045695 14,8 C14,6.8954305 13.1045695,6 12,6 C10.8954305,6 10,6.8954305 10,8 C10,9.1045695 10.8954305,10 12,10 Z M12,13 C9.23857625,13 7,10.7614237 7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 C17,10.7614237 14.7614237,13 12,13 Z" fill="#000000" fill-rule="nonzero" />
                            </g>
                        </svg>
                    </span>
                </div>
                <div class="alert-text">
                    {{-- Each column has an optional rendering control called columns.render which can be used to process the content of each cell before the data is used. See official documentation --}}
                    {{-- <a class="font-weight-bold" href="https://datatables.net/examples/advanced_init/column_render.html" target="_blank">here</a>. --}}
                </div>
            </div>

            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header flex-wrap py-5">
                    <div class="card-title">
                        <h3 class="card-label">Active Event
                        <div class="text-muted pt-2 font-size-sm"></div></h3>
                    </div>
                    <div class="card-toolbar">
                        {{-- <div class="dropdown dropdown-inline mr-2">
                            <button type="button" class="btn btn-light-primary font-weight-bolder dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="svg-icon svg-icon-md">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <path d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z" fill="#000000" opacity="0.3" />
                                        <path d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z" fill="#000000" />
                                    </g>
                                </svg>
                            </span>Export</button>

                            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                <ul class="navi flex-column navi-hover py-2">
                                    <li class="navi-header font-weight-bolder text-uppercase font-size-sm text-primary pb-2">Choose an option:</li>
                                    <li class="navi-item">
                                        <a href="#" class="navi-link">
                                            <span class="navi-icon">
                                                <i class="la la-print"></i>
                                            </span>
                                            <span class="navi-text">Print</span>
                                        </a>
                                    </li>
                                    <li class="navi-item">
                                        <a href="#" class="navi-link">
                                            <span class="navi-icon">
                                                <i class="la la-copy"></i>
                                            </span>
                                            <span class="navi-text">Copy</span>
                                        </a>
                                    </li>
                                    <li class="navi-item">
                                        <a href="#" class="navi-link">
                                            <span class="navi-icon">
                                                <i class="la la-file-excel-o"></i>
                                            </span>
                                            <span class="navi-text">Excel</span>
                                        </a>
                                    </li>
                                    <li class="navi-item">
                                        <a href="#" class="navi-link">
                                            <span class="navi-icon">
                                                <i class="la la-file-text-o"></i>
                                            </span>
                                            <span class="navi-text">CSV</span>
                                        </a>
                                    </li>
                                    <li class="navi-item">
                                        <a href="#" class="navi-link">
                                            <span class="navi-icon">
                                                <i class="la la-file-pdf-o"></i>
                                            </span>
                                            <span class="navi-text">PDF</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div> --}}
                        @can('add_events')
                            <button type="button" class="btn btn-primary mr-2" data-toggle="modal" data-target="#activeEventModal">Set New Event As Active</button>
                        @endcan
                    

                    </div>
                </div>
                <div class="card-body">

                    <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Event</th>
                                <th>Start At</th>
                                <th>End At</th>
                                <th>Type</th>
                                <th>Status</th>
                                <th>Created At</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($data as $key=>$item)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>
                                
                                    <div class="d-flex align-items-center">
                                        <div class="symbol symbol-50 flex-shrink-0">
                                            <img src="{{ asset('assets/media/users/100_2.jpg') }}" alt="photo">
                                        </div>
                                        <div class="ml-3">
                                            <span class="text-dark-75 font-weight-bold line-height-sm d-block pb-2"> {{$item->div_name}} </span>
                                            <a href="#" class="text-muted text-hover-primary">{{$item->service_label}}</a><br>
                                            <a href="#" class="text-muted text-hover-primary">{{$item->entity_name}}</a>
                                        </div>
                                    </div>

                                </td>
                                <td>{{$item->start_date}}</td>
                                <td>{{$item->end_date}}</td>
                                
                                <td>
                                    @if ($item->activity_type_code == 'VOT')
                                        Voting
                                    @else 
                                        Other
                                    @endif
                                </td>

                                <td>
                                    @if ($item->active_status == 1)
                                        <span class="label label-inline label-light-success font-weight-bold">
                                            Active
                                        </span>
                                    @else 
                                        <span class="label label-inline label-light-danger font-weight-bold">
                                            In active
                                        </span>
                                    @endif

                                </td>
                                <td>{{ date('d M Y h:i A', strtotime($item->created_at)) }}</td>
                                <td nowrap="nowrap">

                                    <div class="dropdown dropdown-inline">
                                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown">
                                            <i class="la la-cog"></i>
                                        </a>
                                          <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                            <ul class="nav nav-hoverable flex-column">
                                                {{-- <li class="nav-item"><a class="nav-link" href="/event_configs/{{$item->assigned_code}}"><i class="nav-icon la la-edit"></i><span class="nav-text">Event Configs</span></a></li> --}}
                                                @can('view_categories')
                                                    <li class="nav-item"><a class="nav-link" href="/categories/{{$item->assigned_code}}"><i class="nav-icon la la-edit"></i><span class="nav-text">Categories</span></a></li>
                                                @endcan

                                                @can('view_electorates')
                                                    <li class="nav-item"><a class="nav-link" href="/electorates/{{$item->assigned_code}}"><i class="nav-icon la la-edit"></i><span class="nav-text">Electorates</span></a></li>
                                                @endcan

                                                {{-- @can('view_event_details')
                                                    <li class="nav-item"><a class="nav-link" href="#"><i class="nav-icon la la-leaf"></i><span class="nav-text">View Details</span></a></li>
                                                @endcan --}}
                                                {{-- <li class="nav-item"><a class="nav-link" href="#"><i class="nav-icon la la-print"></i><span class="nav-text">Print</span></a></li> --}}
                                            </ul>
                                          </div>
                                    </div>
                                    
                                    @can('edit_event')
                                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Edit details">
                                            <i class="la la-edit"></i>
                                        </a>
                                    @endcan

                                    @can('deactivate_events')
                                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Delete">
                                            <i class="la la-trash"></i>
                                        </a>
                                    @endcan

                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                    {{ $data->links() }}
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>

    <!--begin::Modal-->
    
    <div class="modal fade" id="activeEventModal" tabindex="-1" role="dialog" aria-labelledby="activeEventModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Active Event Config (Choose event to set as active)</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    
                    <form class="form" id="kt_category_add_form">
                        
                        <!--begin::Form Wizard Step 3-->
                        <div class="pb-5 nominee_class" data-wizard-type="step-content" id="nominee_class">
                            <div class="row" id="nominee_row">

                                <div class="col-xl-12">
                                    <div class="form-group row">
                                        <label class="col-xl-6 col-lg-6 col-form-label">Event</label>
                                        <select name="active_events_select_event_id" id="active_events_select_event_id" class="form-control select2 col-lg-6 col-xl-6" required>
                                            @foreach($event_list as $item)
                                                <option value="{{ $item->assigned_code }}">{{ $item->service_label }} - {{$item->entity_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="separator separator-dashed my-5"></div>
                            </div>

                        </div>
                        <!--end::Form Wizard Step 3-->
                        
                        <!--begin::Wizard Actions-->
                        <div class="d-flex justify-content-between border-top pt-10">
                            <div class="mr-2">
                                {{-- <button type="button" class="btn btn-light-warning font-weight-bold text-uppercase px-9 py-4" id="add_nom_div">Add more</button> --}}
                            </div>
                            <div>
                                <button type="button" class="btn btn-success font-weight-bold text-uppercase px-9 py-4" data-wizard-type="action-submit" onclick="set_event_active();">Set Active</button>
                                <br><br>
                            </div>
                        </div>
                        <!--end::Wizard Actions-->
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!--end::Modal-->

    <!--end::Entry-->
</div>

@endsection