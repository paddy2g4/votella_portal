<!DOCTYPE html>
<html>
<head>
    <head><base href="">
		<meta charset="utf-8" />
		<title>{{ config('app.name', 'Votella') }}</title>
		<meta name="description" content="Column rendering datatables examples" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	</head>
</head>
<body>
	<h1 style="text-align: center;">{{$entity_name}} </h1>
	<h1 style="text-align: center;">{{$event_name}} </h1>
	<br>
    <p>
        The results shown below is a true and accurate representation of the total valid votes cast in the {{$event_name}} on {{ $event_date}} [{{$event_start_time}} - {{$event_end_time}}]</p>

    {!! $content !!}

    <br><br><br>

    <table id="example3" class="table table-bordered" style="font-size: 0.725rem; border:1px solid black;margin-left:auto;margin-right:auto;">
        <tbody>
            <tr>
               <td style="font-weight: bold; text-align: left;">TOTAL REGISTERED ELECTORATES: {{$total_electorates_count}}</td>
               {{-- <td style="font-weight: bold; text-align: right;"></td> --}}
            </tr>
            <tr>
                <td style="font-weight: bold; text-align: left;">TOTAL TURNOUT: {{$voted_electorates_count}}</td>
                {{-- <td style="font-weight: bold; text-align: right;"></td> --}}
             </tr>
        </tbody>
    </table>


    <br><br><br>

    <h2>Signature</h2>
    <table class="table width-100" cellspacing="0" cellpadding="0" style="font-size: 0.725rem; border:none solid black;margin-left:auto;margin-right:auto;">
        <tbody>
            <tr style="border:none;">
                <td style="font-weight: bold; width:50%; text-align: left; height: 40px; position: bottom;"></td>
                <td style="font-weight: bold; width:50%; text-align: left; height: 40px; position: bottom;"></td>
             </tr>
            <tr style="border:none;">
               <td style="font-weight: bold; width:50%; text-align: left;">............................................................</td>
               <td style="font-weight: bold; width:50%; text-align: left;">............................................................</td>
            </tr>
            <tr style="border:none;">
                <td style="font-weight: bold; text-align: left;">Votella Rep 1</td>
                <td style="font-weight: bold; text-align: left;">Votella Rep 2</td>
             </tr>
             <tr style="border:none;">
                <td style="font-weight: bold; text-align: left;">Name: </td>
                <td style="font-weight: bold; text-align: left;">Name: </td>
             </tr>
        </tbody>
    </table>

    <br>
    <table class="table width-100" cellspacing="0" cellpadding="0" style="font-size: 0.725rem; border:none solid black;margin-left:auto;margin-right:auto;">
        <tbody>
            <tr style="border:none;">
                <td style="font-weight: bold; width:50%; text-align: left; height: 40px; position: bottom;"></td>
                <td style="font-weight: bold; width:50%; text-align: left; height: 40px; position: bottom;"></td>
             </tr>
            <tr style="border:none;">
               <td style="font-weight: bold; width:50%; text-align: left;">............................................................</td>
               <td style="font-weight: bold; width:50%; text-align: left;">............................................................</td>
            </tr>
            <tr>
                <td style="font-weight: bold; text-align: left;">Electoral Commission Rep 1</td>
                <td style="font-weight: bold; text-align: left;">Electoral Commission Rep 2</td>
             </tr>
             <tr>
                <td style="font-weight: bold; text-align: left;">Name: </td>
                <td style="font-weight: bold; text-align: left;">Name: </td>
             </tr>
        </tbody>
    </table>


    <div style="position: absolute; bottom: 5px;">
        Printed on: {{ date('Y-m-d H:i:s') }} - Votella (C) {{ date('Y') }}
    </div>
    


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
        
</body>
</html>