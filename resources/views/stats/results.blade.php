@extends('layouts.results_form_master')

@section('content')

<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-transparent" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Details-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Results</h5>
                <!--end::Title-->
                <!--begin::Separator-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
                <!--end::Separator-->
            </div>
            <!--end::Details-->
            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">
                <!--begin::Button-->
                <a href="{{ url()->previous() }}" class="btn btn-default font-weight-bold">Back</a>
                <!--end::Button-->
            </div>
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container-fluid">
            <!--begin::Card-->
            <div class="card card-custom gutter-b">
                <!--begin::Body-->
                <div class="card-body p-0">
                    <!--begin::Wizard-->

                    <div class="container">

                        <form class="form" id="kt_category_add_form">
                            <br><br>
                           
                            <!--begin::Form Wizard Step 3-->
                            <div class="pb-5 nominee_class" data-wizard-type="step-content" id="nominee_class">
                                <h3 class="mb-10 font-weight-bold text-dark">Choose Event to view results:</h3>
                                <div class="row" id="nominee_row">

                                    <div class="col-xl-12">
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Event</label>
                                            <select name="select_event_id" id="select_event_id" class="form-control select2 col-lg-6 col-xl-6" required>
                                                @foreach($events as $item)
                                                    <option value="{{ $item->assigned_code }}">{{ $item->service_label }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="separator separator-dashed my-5"></div>
                                </div>

                            </div>
                            <!--end::Form Wizard Step 3-->
                            
                            <!--begin::Wizard Actions-->
                            <div class="d-flex justify-content-between border-top pt-10">
                                <div class="mr-2">
                                    {{-- <button type="button" class="btn btn-light-warning font-weight-bold text-uppercase px-9 py-4" id="add_nom_div">Add more</button> --}}
                                </div>
                                <div>
                                    <button type="button" class="btn btn-success font-weight-bold text-uppercase px-9 py-4" data-wizard-type="action-submit" onclick="view_results();">View Results</button>
                                    <br><br>
                                </div>
                            </div>
                            <!--end::Wizard Actions-->
                        </form>


                    </div>


                    <!--end::Wizard-->
                </div>
                <!--end::Body-->
            </div>
            <!--end::Card-->



            <div class="card card-custom gutter-b">
                <!--begin::Body-->
                <div class="card-body p-0">
                    <!--begin::Wizard-->

                    <div class="container">

                        <form class="form" action="{{ route('download_result') }}" method="POST">
                            @csrf
                            <br><br>
                           
                            <div class="pb-5 nominee_class" data-wizard-type="step-content" id="nominee_class">
                                <h3 class="mb-10 font-weight-bold text-dark result_event_header"></h3>
                                <input type="hidden" id="entity_div_code" name="entity_div_code">
                            </div>
                            
                            <div class="d-flex justify-content-between border-top pt-10 result_div">
                            </div>

                            <div class="d-flex justify-content-between border-top pt-10 download_div">
                                @can('download_results')
                                    <div class="mr-2"></div><div>
                                        <button type="submit" class="btn btn-success font-weight-bold text-uppercase px-9 py-4">Download Results</button>
                                        <br><br> </div>
                                @endcan    
                            </div>
                        </form>

                    </div>

                    <!--end::Wizard-->
                </div>
                <!--end::Body-->
            </div>

        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>

@endsection




