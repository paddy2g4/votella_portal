$(function() {

    var initToast = function (message, title, type) {

		toastr.options = {
			"closeButton": false,
			"debug": false,
			"newestOnTop": false,
			"progressBar": true,
			"positionClass": "toast-top-right",
			"preventDuplicates": false,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		  };
	
		switch (type) {
			case "success":
				toastr.success(message, title);
				break;
	
			case "info":
				toastr.info(message, title);
				break;
	
			case "warning":
				toastr.warning(message, title);
				break;
	
			  case "error":
				toastr.error(message, title);
				break;
				
			default:
				toastr.success(message, title);
				break;
		}
	}

    $("#voter_details_div").hide();
    $("#btn_deactivate_account").hide();

    $(".btn_search_voter").click(function(event){
        event.preventDefault();

        $("#voter_details_div").show();
    
        // let entity_div_code = document.getElementById("entity_div_code").value;
        let voter_id = document.getElementById("voter_search_val").value;
    
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: "/voter_verification/queryVoter",
            type:"POST",
            data:{
                voter_id: voter_id
            },
            success:function(response){
                if(response) {
    
                    if (response.resp_code == '000') {

                        var department=""
                        var level="";

                        document.getElementById('voter_id').value = response.voter_id;
                        document.getElementById('voter_name').value = response.name;
                        document.getElementById('voter_phone').value = response.mobile_number;
                        document.getElementById('voter_index_number').value = response.index_number;
                        
                        if (response.department != null) {
                            department=response.department; 
                        }

                        if (response.department != null) {
                            level=response.level; 
                        }
                        document.getElementById('voter_department_level').value = department + " " + level;
                        document.getElementById('voter_vote_status').value = response.vote_status;
                        document.getElementById('voter_account_status').value = response.account_status;
                        if (response.account_status_val == 1) {
                            $("#btn_activate_account").hide();
                            $("#btn_deactivate_account").show();
                        }else{
                            $("#btn_activate_account").show();
                            $("#btn_deactivate_account").hide();
                        }
                        
                        $("#voter_details_div").show();

                        // initToast(response.resp_desc, "Setup", "success");
                        // setTimeout(function(){	
                        //     window.location.href = '/events/'+entity_id;
                        // }, 3000);
    
                    } else {
                        initToast(response.resp_desc, "Voter Search","error");
                    }
                
                }
            },
         });
    });


    $(".btn_activate_account").click(function(event){
        event.preventDefault();
    
        let voter_id = document.getElementById("voter_id").value;
    
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: "/voter_verification/account_action",
            type:"POST",
            data:{
                voter_id: voter_id,
                action_status: 1
            },
            success:function(response){
                if(response) {
    
                    if (response.resp_code == '000') {

                        document.getElementById('voter_name').value = null;
                        document.getElementById('voter_index_number').value = null;                        
                        document.getElementById('voter_phone').value = null;                        
                        document.getElementById('voter_department_level').value = null;
                        document.getElementById('voter_vote_status').value = null;
                        document.getElementById('voter_account_status').value = null;
                        
                        $("#voter_details_div").hide();

                        initToast(response.resp_desc, "Account Status", "success");
    
                    } else {
                        initToast(response.resp_desc, "Account Status","error");
                    }
                
                }
            },
            error: function(xhr, status, error){
                var errorMessage = xhr.status + ': ' + xhr.statusText
                initToast(errorMessage, "Account Status","error");
            }
         });
    });


    $(".btn_deactivate_account").click(function(event){
        event.preventDefault();
    
        let voter_id = document.getElementById("voter_id").value;
    
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: "/voter_verification/account_action",
            type:"POST",
            data:{
                voter_id: voter_id,
                action_status: 0
            },
            success:function(response){
                if(response) {
    
                    if (response.resp_code == '000') {

                        document.getElementById('voter_name').value = null;
                        document.getElementById('voter_index_number').value = null;                        
                        document.getElementById('voter_phone').value = null;                        
                        document.getElementById('voter_department_level').value = null;
                        document.getElementById('voter_vote_status').value = null;
                        document.getElementById('voter_account_status').value = null;
                        
                        $("#voter_details_div").hide();

                        initToast(response.resp_desc, "Account Status", "success");
    
                    } else {
                        initToast(response.resp_desc, "Account Status","error");
                    }
                
                }
            },
            error: function(xhr, status, error){
                var errorMessage = xhr.status + ': ' + xhr.statusText
                initToast(errorMessage, "Account Status","error");
            }
         });
    });


});



