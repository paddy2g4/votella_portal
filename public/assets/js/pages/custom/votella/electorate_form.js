$(function() {

    var initToast = function (message, title, type) {

		toastr.options = {
			"closeButton": false,
			"debug": false,
			"newestOnTop": false,
			"progressBar": true,
			"positionClass": "toast-top-right",
			"preventDuplicates": false,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		  };
	
		switch (type) {
			case "success":
				toastr.success(message, title);
				break;
	
			case "info":
				toastr.info(message, title);
				break;
	
			case "warning":
				toastr.warning(message, title);
				break;
	
			  case "error":
				toastr.error(message, title);
				break;
				
			default:
				toastr.success(message, title);
				break;
		}
	}

    $(".btn_create_electorate").click(function(event){
        event.preventDefault();

        console.log("btn_create_electorate clicked");
    
        let entity_div_code = document.getElementById("entity_div_code").value;
        let voter_name = document.getElementById("voter_name").value;
        let voter_phone = document.getElementById("voter_phone").value;
        let voter_email = document.getElementById("voter_email").value;
        let voter_department = document.getElementById("voter_department").value;
        let voter_level = document.getElementById("voter_level").value;
    
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: "/electorates/save",
            type:"POST",
            data:{
                entity_div_code: entity_div_code,
                voter_name: voter_name,
                voter_phone: voter_phone,
                voter_email: voter_email,
                voter_department: voter_department,
                voter_level: voter_level
            },
            success:function(response){
                if(response) {
    
                    if (response.resp_code == '000') {

                        document.getElementById('voter_name').value = null;
                        document.getElementById('voter_phone').value = null;
                        document.getElementById('voter_email').value = null;
                        document.getElementById('voter_department').value = null;
                        document.getElementById('voter_level').value = null;
                    
                        initToast(response.resp_desc, "Electorates", "success");
                        setTimeout(function(){	
                            window.location.href = '/electorates/'+entity_div_code;
                        }, 1000);
    
                    } else {
                        initToast(response.resp_desc, "Electorate","error");
                    }
                
                }
            },
         });
    });


});



