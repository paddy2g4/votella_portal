"use strict";

// Class definition
var KTContactsAdd = function () {
	// Base elements
	var _wizardEl;
	var _formEl;
	var _wizard;
	var _avatar;
	var _validations = [];


	var initToast = function (message, title, type) {

		toastr.options = {
			"closeButton": false,
			"debug": false,
			"newestOnTop": false,
			"progressBar": true,
			"positionClass": "toast-top-right",
			"preventDuplicates": false,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		  };
	
		switch (type) {
			case "success":
				toastr.success(message, title);
				break;
	
			case "info":
				toastr.info(message, title);
				break;
	
			case "warning":
				toastr.warning(message, title);
				break;
	
			  case "error":
				toastr.error(message, title);
				break;
				
			default:
				toastr.success(message, title);
				break;
		}
	}
	

	$(".save-whitelisting").click(function(event){
		event.preventDefault();
	
		let entity_div_code = document.getElementById("select_entity_div_code").value;
		let remote_ip = document.getElementById("remote_ip").value;
		let phone_number = document.getElementById("phone_number").value;
		let desc = document.getElementById("desc").value;
		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url: "/whitelisting/save",
			type:"POST",
			data:{
				entity_div_code: entity_div_code,
				remote_ip: remote_ip,
				phone_number: phone_number,
				desc: desc
			},
			success:function(response){
				console.log(response);
				if(response) {

					if (response.resp_code == '000') {
						
						$("#kt_nominee_add_form")[0].reset();
						initToast(response.resp_desc, "Setup", "success");
						setTimeout(function(){	
							window.location.href = '/whitelisting';
						}, 1000);

					} else {
						initToast(response.resp_desc, "Setup","error");
					}
				
				}
			},
		 });
	});

	$(".save-blacklisting").click(function(event){
		event.preventDefault();
	
		let entity_div_code = document.getElementById("select_entity_div_code").value;
		let remote_ip = document.getElementById("remote_ip").value;
		let phone_number = document.getElementById("phone_number").value;
		let desc = document.getElementById("desc").value;
		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url: "/blacklisting/save",
			type:"POST",
			data:{
				entity_div_code: entity_div_code,
				remote_ip: remote_ip,
				phone_number: phone_number,
				desc: desc
			},
			success:function(response){
				console.log(response);
				if(response) {

					if (response.resp_code == '000') {
						
						$("#kt_nominee_add_form")[0].reset();
						initToast(response.resp_desc, "Setup", "success");
						setTimeout(function(){	
							window.location.href = '/blacklisting';
						}, 1000);

					} else {
						initToast(response.resp_desc, "Setup","error");
					}
				
				}
			},
		 });
	});


	return {
		// public functions
		init: function () {
			_wizardEl = KTUtil.getById('kt_contact_add');
			_formEl = KTUtil.getById('kt_nominee_add_form');

			// initWizard();
			// initValidation();
			// initAvatar();
		}
	};
}();

jQuery(document).ready(function () {
	KTContactsAdd.init();
});
