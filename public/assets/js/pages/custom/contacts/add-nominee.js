"use strict";

// Class definition
var KTContactsAdd = function () {
	// Base elements
	var _wizardEl;
	var _formEl;
	var _wizard;
	var _avatar;
	var _validations = [];

	var initAvatar = function () {
		_avatar = new KTImageInput('kt_profile_avatar');
	}


	var initToast = function (message, title, type) {

		toastr.options = {
			"closeButton": false,
			"debug": false,
			"newestOnTop": false,
			"progressBar": true,
			"positionClass": "toast-top-right",
			"preventDuplicates": false,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		  };
	
		switch (type) {
			case "success":
				toastr.success(message, title);
				break;
	
			case "info":
				toastr.info(message, title);
				break;
	
			case "warning":
				toastr.warning(message, title);
				break;
	
			  case "error":
				toastr.error(message, title);
				break;
				
			default:
				toastr.success(message, title);
				break;
		}
	}
	kt_nominee_add_form
	

	$(".save-nominee").click(function(event){
		event.preventDefault();
	
		let cat_id = document.getElementById("cat_id").value;
		let nom_name = document.getElementById("nom_name").value;

		var nom_pic = $('#profile_avatar')[0].files[0]
		if (nom_pic){
			console.log(nom_pic.name);
			console.log(nom_pic);
		}


		console.log(cat_id, nom_name);
		console.log(_avatar);

		var formdata = new FormData($('form').get(0));

		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url: "/nominees/save",
			type:"POST",
			data: formdata,
			success:function(response){
				console.log(response);
				if(response) {

					if (response.resp_code == '000') {
						
						$("#kt_nominee_add_form")[0].reset();
						initToast(response.resp_desc, "Setup", "success");
						setTimeout(function(){	
							window.location.href = '/nominees/'+cat_id;
						}, 1000);

					} else {
						initToast(response.resp_desc, "Setup","error");
					}
				
				}
			},
		 });
	});

	$(".update-nominee").click(function(event){
		event.preventDefault();
	
		let nom_id = document.getElementById("nom_id").value;
		let cat_id = document.getElementById("select_cat_id").value;
		let nom_name = document.getElementById("nom_name").value;
		let nom_pic = "";
		console.log(cat_id, nom_name);

		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url: "/nominees/update",
			type:"POST",
			data:{
				nom_id: nom_id,
				cat_id: cat_id,
				nom_name: nom_name,
				nom_pic: nom_pic,
				// cat_type: cat_type,
				// start_date: start_date,
				// end_date: end_date,
				// sms_sender_id: sms_sender_id,
				// show_client_result: show_client_result,
				// auth_identify: auth_identify,
				// auth_class: auth_class,
			},
			success:function(response){
				if(response) {

					if (response.resp_code == '000') {
						
						$("#kt_nominee_add_form")[0].reset();
						initToast(response.resp_desc, "Setup", "success");
						setTimeout(function(){	
							window.location.href = '/nominees/'+cat_id;
						}, 1000);

					} else {
						initToast(response.resp_desc, "Setup","error");
					}
				
				}
			},
		 });
	});


	return {
		// public functions
		init: function () {
			_wizardEl = KTUtil.getById('kt_contact_add');
			_formEl = KTUtil.getById('kt_nominee_add_form');

			// initWizard();
			// initValidation();
			initAvatar();
		}
	};
}();

jQuery(document).ready(function () {
	KTContactsAdd.init();
});



function initToast (message, title, type) {

	toastr.options = {
		"closeButton": false,
		"debug": false,
		"newestOnTop": false,
		"progressBar": true,
		"positionClass": "toast-top-right",
		"preventDuplicates": false,
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	  };

	switch (type) {
		case "success":
			toastr.success(message, title);
			break;

		case "info":
			toastr.info(message, title);
			break;

		case "warning":
			toastr.warning(message, title);
			break;

		  case "error":
			toastr.error(message, title);
			break;
			
		default:
			toastr.success(message, title);
			break;
	}
}


function deactivate_nominee(nom_id) {
	
	if (confirm('This record will be deactivated. Click OK to proceed or Cancel to abort this action.')) {
		
		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url: "/nominees/actions",
			type:"POST",
			data:{
				nom_id: nom_id,
				action: "D",//Deactivate
			},
			success:function(response){
				if(response) {
	
					if (response.resp_code == '000') {
						
						initToast(response.resp_desc, "Setup", "success");
						setTimeout(function(){	
							location.reload();
						}, 1000);
	
					} else {
						initToast(response.resp_desc, "Setup","error");
					}
				
				}
			},
		 });

	  }
		
}


function activate_nominee(nom_id) {
	if (confirm('This record will be activated. Click OK to proceed or Cancel to abort this action.')) {
		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url: "/nominees/actions",
			type:"POST",
			data:{
				nom_id: nom_id,
				action: "A",//Deactivate
			},
			success:function(response){
				console.log(response);
				if(response) {
	
					if (response.resp_code == '000') {
						
						initToast(response.resp_desc, "Setup", "success");
						setTimeout(function(){	
							location.reload();
						}, 1000);
	
					} else {
						initToast(response.resp_desc, "Setup","error");
					}
				
				}
			},
		});
	}
}


function set_event_active() {
	event.preventDefault();

	console.log("Table reload");

	let entity_div_code = document.getElementById("active_events_select_event_id").value;

	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url: "/events/set_in_use",
		type:"POST",
		data:{
			entity_div_code: entity_div_code,
		},
		success:function(response){

			if (response.resp_code == '000') {
						
				initToast(response.resp_desc, "Setup", "success");
				setTimeout(function(){	
					location.reload();
				}, 1000);

			} else {
				initToast(response.resp_desc, "Setup","error");
			}
			// if(response) {
			// 	$('#kt_datatable').KTDatatable('reload');
			// 	// $('#kt_datatable_active_events').KTDatatable('reload');
			// 	$("#activeEventModal").modal('hide');
			// }
		},
	});
}