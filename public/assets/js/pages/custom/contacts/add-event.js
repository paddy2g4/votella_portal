"use strict";

// Class definition
var KTContactsAdd = function () {
	// Base elements
	var _wizardEl;
	var _formEl;
	var _wizard;
	var _avatar;
	var _validations = [];

	// Private functions
	var initWizard = function () {
		// Initialize form wizard
		_wizard = new KTWizard(_wizardEl, {
			startStep: 1, // initial active step number
			clickableSteps: true  // allow step clicking
		});

		// Validation before going to next page
		_wizard.on('beforeNext', function (wizard) {
			// Don't go to the next step yet
			_wizard.stop();

			// Validate form
			var validator = _validations[wizard.getStep() - 1]; // get validator for currnt step
			validator.validate().then(function (status) {
				if (status == 'Valid') {
					document.getElementById('summ_div_name').innerHTML = document.getElementById("div_name").value;
					document.getElementById('summ_service_label').innerHTML = document.getElementById("service_label").value;

					var e = document.getElementById("cat_type");
					var cat_type_value=e.value;// get selected option value
					var cat_type_text=e.options[e.selectedIndex].text;//get the selected option text
					document.getElementById('summ_cat_type').innerHTML = cat_type_text;
					document.getElementById('summ_start_date').innerHTML = document.getElementById("kt_datetimepicker_event_startdate").value;
					document.getElementById('summ_end_date').innerHTML = document.getElementById("kt_datetimepicker_event_enddate").value;
					document.getElementById('summ_sender_id').innerHTML = document.getElementById("sms_sender_id").value;

					switch (document.getElementById("show_client_result").value) {
						case "true":
							document.getElementById('summ_show_client_result').innerHTML = "Show Voting Results";
							break;
						case "false":
							document.getElementById('summ_show_client_result').innerHTML = "Hide Voting Results";
							break;
						default:
							document.getElementById('summ_show_client_result').innerHTML = document.getElementById("show_client_result").value;
							break;
					}
					
					switch (document.getElementById("auth_identify").value) {
						case "D":
							document.getElementById('summ_auth_identify').innerHTML = "Student ID";
							break;
						case "M":
							document.getElementById('summ_auth_identify').innerHTML = "Mobile Number";
							break;
						case "E":
							document.getElementById('summ_auth_identify').innerHTML = "Email";
							break;
					
						default:
							document.getElementById('summ_auth_identify').innerHTML = document.getElementById("auth_identify").value;
							break;
					}


					switch (document.getElementById("auth_class").value) {
						case "A":
							document.getElementById('summ_auth_class').innerHTML = "No Password (Activation)";
							break;
						case "P":
							document.getElementById('summ_auth_class').innerHTML = "Password";
							break;
						default:
							document.getElementById('summ_auth_class').innerHTML = document.getElementById("auth_class").value;
							break;
					}
				
					console.log(`step: ${wizard.getStep()}`);
					_wizard.goNext();
					KTUtil.scrollTop();
				} else {
					Swal.fire({
						text: "Sorry, looks like there are some errors detected, please try again.",
						icon: "error",
						buttonsStyling: false,
						confirmButtonText: "Ok, got it!",
						customClass: {
							confirmButton: "btn font-weight-bold btn-light"
						}
					}).then(function () {
						KTUtil.scrollTop();
					});
				}
			});
		});

		// Change Event
		_wizard.on('change', function (wizard) {
			KTUtil.scrollTop();
		});
	}

	var initValidation = function () {
		// Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/

		// Step 1
		_validations.push(FormValidation.formValidation(
			_formEl,
			{
				fields: {
					div_name: {
						validators: {
							notEmpty: {
								message: 'Event Name is required'
							}
						}
					},
					service_label: {
						validators: {
							notEmpty: {
								message: 'Event display name is required'
							}
						}
					},
					cat_type: {
						validators: {
							notEmpty: {
								message: 'Category is required'
							}
						}
					}
					// phone: {
					// 	validators: {
					// 		notEmpty: {
					// 			message: 'Phone is required'
					// 		},
					// 		phone: {
					// 			country: 'US',
					// 			message: 'The value is not a valid US phone number. (e.g 5554443333)'
					// 		}
					// 	}
					// },
					// email: {
					// 	validators: {
					// 		notEmpty: {
					// 			message: 'Email is required'
					// 		},
					// 		emailAddress: {
					// 			message: 'The value is not a valid email address'
					// 		}
					// 	}
					// },
					// companywebsite: {
					// 	validators: {
					// 		notEmpty: {
					// 			message: 'Website URL is required'
					// 		}
					// 	}
					// }
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap()
				}
			}
		));

		// Step 2
		_validations.push(FormValidation.formValidation(
			_formEl,
			{
				fields: {
					// Step 2
					kt_datetimepicker_event_startdate: {
						validators: {
							notEmpty: {
								message: 'Event Start date is required'
							}
						}
					},
					kt_datetimepicker_event_enddate: {
						validators: {
							notEmpty: {
								message: 'Event end date is required'
							}
						}
					},
					sms_sender_id: {
						validators: {
							notEmpty: {
								message: 'SMS Sender ID is required'
							}
						}
					},	
					show_client_result: {
						validators: {
							notEmpty: {
								message: 'Please select whether or not you wish to show voting results.'
							}
						}
					},
					auth_identify: {
						validators: {
							notEmpty: {
								message: 'Please select voter authentication identifier'
							}
						}
					},
					auth_class: {
						validators: {
							notEmpty: {
								message: 'Please select voter authentication method'
							}
						}
					}
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap()
				}
			}
		));

		// Step 3
		// _validations.push(FormValidation.formValidation(
		// 	_formEl,
		// 	{
		// 		fields: {
		// 			address1: {
		// 				validators: {
		// 					notEmpty: {
		// 						message: 'Address is required'
		// 					}
		// 				}
		// 			},
		// 			postcode: {
		// 				validators: {
		// 					notEmpty: {
		// 						message: 'Postcode is required'
		// 					}
		// 				}
		// 			},
		// 			city: {
		// 				validators: {
		// 					notEmpty: {
		// 						message: 'City is required'
		// 					}
		// 				}
		// 			},
		// 			state: {
		// 				validators: {
		// 					notEmpty: {
		// 						message: 'state is required'
		// 					}
		// 				}
		// 			},
		// 			country: {
		// 				validators: {
		// 					notEmpty: {
		// 						message: 'Country is required'
		// 					}
		// 				}
		// 			},
		// 		},
		// 		plugins: {
		// 			trigger: new FormValidation.plugins.Trigger(),
		// 			bootstrap: new FormValidation.plugins.Bootstrap()
		// 		}
		// 	}
		// ));
	}

	var initAvatar = function () {
		_avatar = new KTImageInput('kt_contact_add_avatar');
	}


	var initToast = function (message, title, type) {

		toastr.options = {
			"closeButton": false,
			"debug": false,
			"newestOnTop": false,
			"progressBar": true,
			"positionClass": "toast-top-right",
			"preventDuplicates": false,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		  };
	
		switch (type) {
			case "success":
				toastr.success(message, title);
				break;
	
			case "info":
				toastr.info(message, title);
				break;
	
			case "warning":
				toastr.warning(message, title);
				break;
	
			  case "error":
				toastr.error(message, title);
				break;
				
			default:
				toastr.success(message, title);
				break;
		}
	}
	

	$(".save-event").click(function(event){
		event.preventDefault();
	
		let entity_id = document.getElementById("entity_id").value;
		let div_name = document.getElementById("div_name").value;
		let service_label = document.getElementById("service_label").value;
		let cat_type = document.getElementById("cat_type").value;
		let start_date = document.getElementById("kt_datetimepicker_event_startdate").value;
		let end_date = document.getElementById("kt_datetimepicker_event_enddate").value;
		let sms_sender_id = document.getElementById("sms_sender_id").value;
		let show_client_result = document.getElementById("show_client_result").value;
		let auth_identify = document.getElementById("auth_identify").value;
		let auth_class = document.getElementById("auth_class").value;
		let event_logo = document.getElementById("profile_avatar").files[0];

		var formData = new FormData();
		formData.append("entity_id", entity_id);
		formData.append("div_name", div_name);
		formData.append("service_label", service_label);
		formData.append("cat_type", cat_type);
		formData.append("start_date", start_date);
		formData.append("end_date", end_date);
		formData.append("sms_sender_id",sms_sender_id);
		formData.append("show_client_result", show_client_result);
		formData.append("auth_identify", auth_identify);
		formData.append("auth_class", auth_class);
		formData.append("event_logo", event_logo);

		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url: "/events/save",
			type:"POST",
			data: formData,
			processData: false,
        	contentType: false,
			// data:{
			// 	entity_id: entity_id,
			// 	div_name: div_name,
			// 	service_label: service_label,
			// 	cat_type: cat_type,
			// 	start_date: start_date,
			// 	end_date: end_date,
			// 	sms_sender_id: sms_sender_id,
			// 	show_client_result: show_client_result,
			// 	auth_identify: auth_identify,
			// 	auth_class: auth_class,
			// 	event_logo: event_logo,
			// },
			success:function(response){
				if(response) {

					if (response.resp_code == '000') {
						
						$("#kt_contact_add_form")[0].reset();
						initToast(response.resp_desc, "Setup", "success");
						setTimeout(function(){	
							window.location.href = '/events/'+entity_id;
						}, 3000);

					} else {
						initToast(response.resp_desc, "Setup","error");
					}
				
				}
			},
		 });
	});


	return {
		// public functions
		init: function () {
			_wizardEl = KTUtil.getById('kt_contact_add');
			_formEl = KTUtil.getById('kt_contact_add_form');

			initWizard();
			initValidation();
			initAvatar();
		}
	};
}();

jQuery(document).ready(function () {
	KTContactsAdd.init();
});
