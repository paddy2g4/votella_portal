"use strict";

// Class definition
var KTContactsAdd = function () {
	// Base elements
	var _wizardEl;
	var _formEl;
	var _wizard;
	var _avatar;
	var _validations = [];

	

	var initAvatar = function () {
		_avatar = new KTImageInput('kt_contact_add_avatar');
	}


	var initToast = function (message, title, type) {

		toastr.options = {
			"closeButton": false,
			"debug": false,
			"newestOnTop": false,
			"progressBar": true,
			"positionClass": "toast-top-right",
			"preventDuplicates": false,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		  };
	
		switch (type) {
			case "success":
				toastr.success(message, title);
				break;
	
			case "info":
				toastr.info(message, title);
				break;
	
			case "warning":
				toastr.warning(message, title);
				break;
	
			  case "error":
				toastr.error(message, title);
				break;
				
			default:
				toastr.success(message, title);
				break;
		}
	}
	

	$(".save-permission").click(function(event){
		event.preventDefault();
	
		let name = document.getElementById("name").value;
		let slug = document.getElementById("slug").value;

		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url: "/permissions/save",
			type:"POST",
			data:{
				name: name,
				slug: slug,
			},
			success:function(response){
				console.log(response);
				if(response) {

					if (response.resp_code == '000') {
						
						$("#kt_nominee_add_form")[0].reset();
						initToast(response.resp_desc, "Setup", "success");
						setTimeout(function(){	
							window.location.href = '/permissions';
						}, 1000);

					} else {
						initToast(response.resp_desc, "Setup","error");
					}
				
				}
			},
		 });
	});


	$(".save-role").click(function(event){
		event.preventDefault();
	
		let name = document.getElementById("name").value;
		let slug = document.getElementById("slug").value;
		const selected = document.querySelectorAll('#select_role_permission option:checked');
		const role_permissions = Array.from(selected).map(el => el.value);

		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url: "/roles/save",
			type:"POST",
			data:{
				name: name,
				slug: slug,
				role_permission: role_permissions,
			},
			success:function(response){
				console.log(response);
				if(response) {

					if (response.resp_code == '000') {
						
						$("#kt_nominee_add_form")[0].reset();
						initToast(response.resp_desc, "Setup", "success");
						setTimeout(function(){	
							window.location.href = '/roles';
						}, 1000);

					} else {
						initToast(response.resp_desc, "Setup","error");
					}
				
				}
			},
		 });
	});


	$(".update-role").click(function(event){
		event.preventDefault();
	
		let role_id = document.getElementById("role_id").value;
		let name = document.getElementById("name").value;
		let slug = document.getElementById("slug").value;
		const selected = document.querySelectorAll('#select_role_permission option:checked');
		const role_permissions = Array.from(selected).map(el => el.value);

		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url: "/roles/update",
			type:"POST",
			data:{
				role_id: role_id,
				name: name,
				slug: slug,
				role_permission: role_permissions,
			},
			success:function(response){
				console.log(response);
				if(response) {

					if (response.resp_code == '000') {
						
						$("#kt_nominee_add_form")[0].reset();
						initToast(response.resp_desc, "Setup", "success");
						setTimeout(function(){	
							window.location.href = '/roles';
						}, 1000);

					} else {
						initToast(response.resp_desc, "Setup","error");
					}
				
				}
			},
		 });
	});


	$(".update-permission").click(function(event){
		event.preventDefault();
	
		let permission_id = document.getElementById("permission_id").value;
		let name = document.getElementById("name").value;
		let slug = document.getElementById("slug").value;
		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url: "/permissions/update",
			type:"POST",
			data:{
				permission_id: permission_id,
				name: name,
				slug: slug,
			},
			success:function(response){
				if(response) {

					if (response.resp_code == '000') {
						
						$("#kt_nominee_add_form")[0].reset();
						initToast(response.resp_desc, "Setup", "success");
						setTimeout(function(){	
							window.location.href = '/permissions';
						}, 1000);

					} else {
						initToast(response.resp_desc, "Setup","error");
					}
				
				}
			},
		 });
	});


	$(".save-user").click(function(event){
		event.preventDefault();

		console.log("clicked");
	
		let name = document.getElementById("name").value;
		let email = document.getElementById("email").value;
		let password = document.getElementById("password").value;
		let password_confirmation = document.getElementById("confirm_password").value;
		let select_user_role_id = document.getElementById("select_user_role_id").value;

		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url: "/users/save",
			type:"POST",
			data:{
				name: name,
				email: email,
				password: password,
				password_confirmation: password_confirmation,
				role: select_user_role_id,
			},
			success:function(response){
				console.log(response);
				if(response) {

					if (response.resp_code == '000') {
						
						$("#kt_nominee_add_form")[0].reset();
						initToast(response.resp_desc, "Setup", "success");
						setTimeout(function(){	
							window.location.href = '/users';
						}, 1000);

					} else {
						initToast(response.resp_desc, "Setup","error");
					}
				
				}
			},
		 });
	});


	$(".update-user").click(function(event){
		event.preventDefault();
	
		let user_id = document.getElementById("user_id").value;
		let name = document.getElementById("name").value;
		let email = document.getElementById("email").value;
		let select_user_role_id = document.getElementById("select_user_role_id").value;

		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url: "/users/update",
			type:"POST",
			data:{
				user_id: user_id,
				name: name,
				email: email,
				role_id: select_user_role_id,
			},
			success:function(response){
				console.log(response);
				if(response) {

					if (response.resp_code == '000') {
						
						$("#kt_nominee_add_form")[0].reset();
						initToast(response.resp_desc, "Setup", "success");
						setTimeout(function(){	
							window.location.href = '/users';
						}, 1000);

					} else {
						initToast(response.resp_desc, "Setup","error");
					}
				
				}
			},
		 });
	});



	return {
		// public functions
		init: function () {
			_wizardEl = KTUtil.getById('kt_contact_add');
			_formEl = KTUtil.getById('kt_nominee_add_form');

			// initWizard();
			// initValidation();
			initAvatar();
		}
	};
}();

jQuery(document).ready(function () {
	KTContactsAdd.init();
});



function initToast (message, title, type) {

	toastr.options = {
		"closeButton": false,
		"debug": false,
		"newestOnTop": false,
		"progressBar": true,
		"positionClass": "toast-top-right",
		"preventDuplicates": false,
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	  };

	switch (type) {
		case "success":
			toastr.success(message, title);
			break;

		case "info":
			toastr.info(message, title);
			break;

		case "warning":
			toastr.warning(message, title);
			break;

		  case "error":
			toastr.error(message, title);
			break;
			
		default:
			toastr.success(message, title);
			break;
	}
}


function deactivate_permission(permission_id) {
	
	if (confirm('This permission will be deactivated. Click OK to proceed or Cancel to abort this action.')) {
		
		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url: "/permissions/actions",
			type:"POST",
			data:{
				permission_id: permission_id,
				action: "D",//Deactivate
			},
			success:function(response){
				console.log(response);
				if(response) {
	
					if (response.resp_code == '000') {
						
						initToast(response.resp_desc, "Setup", "success");
						setTimeout(function(){	
							window.location.href = '/permissions';
						}, 1000);
	
					} else {
						initToast(response.resp_desc, "Setup","error");
					}
				
				}
			},
		 });

	  }
		
}


function activate_permission(permission_id) {
	if (confirm('This permission will be activated. Click OK to proceed or Cancel to abort this action.')) {
		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url: "/permissions/actions",
			type:"POST",
			data:{
				permission_id: permission_id,
				action: "A",//Deactivate
			},
			success:function(response){
				console.log(response);
				if(response) {
	
					if (response.resp_code == '000') {
						
						initToast(response.resp_desc, "Setup", "success");
						setTimeout(function(){	
							window.location.href = '/permissions';
						}, 1000);
	
					} else {
						initToast(response.resp_desc, "Setup","error");
					}
				
				}
			},
		});
	}
}


function deactivate_role(role_id) {
	
	if (confirm('This role will be deactivated. Click OK to proceed or Cancel to abort this action.')) {
		
		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url: "/roles/actions",
			type:"POST",
			data:{
				role_id: role_id,
				action: "D",//Deactivate
			},
			success:function(response){
				console.log(response);
				if(response) {
	
					if (response.resp_code == '000') {
						
						initToast(response.resp_desc, "Setup", "success");
						setTimeout(function(){	
							window.location.href = '/roles';
						}, 1000);
	
					} else {
						initToast(response.resp_desc, "Setup","error");
					}
				
				}
			},
		 });

	  }
		
}


function activate_role(role_id) {
	if (confirm('This role will be activated. Click OK to proceed or Cancel to abort this action.')) {
		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url: "/roles/actions",
			type:"POST",
			data:{
				role_id: role_id,
				action: "A",//Deactivate
			},
			success:function(response){
				console.log(response);
				if(response) {
	
					if (response.resp_code == '000') {
						
						initToast(response.resp_desc, "Setup", "success");
						setTimeout(function(){	
							window.location.href = '/roles';
						}, 1000);
	
					} else {
						initToast(response.resp_desc, "Setup","error");
					}
				
				}
			},
		});
	}
}


function deactivate_user(user_id) {
	
	if (confirm('This user will be deactivated. Click OK to proceed or Cancel to abort this action.')) {
		
		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url: "/users/actions",
			type:"POST",
			data:{
				user_id: user_id,
				action: "D",//Deactivate
			},
			success:function(response){
				console.log(response);
				if(response) {
	
					if (response.resp_code == '000') {
						
						initToast(response.resp_desc, "Setup", "success");
						setTimeout(function(){	
							window.location.href = '/users';
						}, 1000);
	
					} else {
						initToast(response.resp_desc, "Setup","error");
					}
				
				}
			},
		 });

	  }
		
}


function activate_user(user_id) {
	if (confirm('This user will be activated. Click OK to proceed or Cancel to abort this action.')) {
		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url: "/users/actions",
			type:"POST",
			data:{
				user_id: user_id,
				action: "A",//Deactivate
			},
			success:function(response){
				if(response) {
	
					if (response.resp_code == '000') {
						
						initToast(response.resp_desc, "Setup", "success");
						setTimeout(function(){	
							window.location.href = '/users';
						}, 1000);
	
					} else {
						initToast(response.resp_desc, "Setup","error");
					}
				
				}
			},
		});
	}
}