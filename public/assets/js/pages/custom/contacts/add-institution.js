"use strict";

// Class definition
var KTContactsAdd = function () {

	// Base elements
	var _wizardEl;
	var _formEl;
	var _wizard;
	var _avatar;
	var _validations = [];

	// Private functions
	var initWizard = function () {
		// Initialize form wizard
		_wizard = new KTWizard(_wizardEl, {
			startStep: 1, // initial active step number
			clickableSteps: true  // allow step clicking
		});

		// Validation before going to next page
		_wizard.on('beforeNext', function (wizard) {
			// Don't go to the next step yet
			_wizard.stop();

			// Validate form
			var validator = _validations[wizard.getStep() - 1]; // get validator for currnt step
			validator.validate().then(function (status) {
				if (status == 'Valid') {

					document.getElementById('summ_name').innerHTML = document.getElementById("name").value;
					document.getElementById('summ_alias').innerHTML = document.getElementById("alias").value;

					var e = document.getElementById("cat_type");
					var cat_type_value=e.value;// get selected option value
					var cat_type_text=e.options[e.selectedIndex].text;//get the selected option text
					document.getElementById('summ_cat_type').innerHTML = cat_type_text;
					document.getElementById('summ_phone').innerHTML = document.getElementById("phone").value;
					document.getElementById('summ_email').innerHTML = document.getElementById("email").value;
					document.getElementById('summ_other_details').innerHTML = document.getElementById("other_details").value;
					
					console.log(`step: ${wizard.getStep()}`);
					_wizard.goNext();
					KTUtil.scrollTop();
				} else {
					Swal.fire({
						text: "Sorry, looks like there are some errors detected, please try again.",
						icon: "error",
						buttonsStyling: false,
						confirmButtonText: "Ok, got it!",
						customClass: {
							confirmButton: "btn font-weight-bold btn-light"
						}
					}).then(function () {
						KTUtil.scrollTop();
					});
				}
			});
		});

		// Change Event
		_wizard.on('change', function (wizard) {
			KTUtil.scrollTop();
		});
	}

	var initValidation = function () {
		// Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/

		// Step 1
		_validations.push(FormValidation.formValidation(
			_formEl,
			{
				fields: {
					name: {
						validators: {
							notEmpty: {
								message: 'Institution Name is required'
							}
						}
					},
					alias: {
						validators: {
							notEmpty: {
								message: 'Institution Alias is required'
							}
						}
					},
					cat_type: {
						validators: {
							notEmpty: {
								message: 'Category is required'
							}
						}
					},
					
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap()
				}
			}
		));

		// Step 2
		_validations.push(FormValidation.formValidation(
			_formEl,
			{
				fields: {
					// Step 2
					phone: {
						validators: {
							notEmpty: {
								message: 'Phone is required'
							},
							phone: {
								country: 'GH',
								message: 'The value is not a valid GH phone number. (e.g 233200000000)'
							}
						}
					},
					email: {
						validators: {
							notEmpty: {
								message: 'Email is required'
							},
							emailAddress: {
								message: 'The value is not a valid email address'
							}
						}
					},
					other_details: {
						validators: {
							notEmpty: {
								message: 'Website URL is required'
							}
						}
					},
					// communication: {
					// 	validators: {
					// 		choice: {
					// 			min: 1,
					// 			message: 'Please select at least 1 option'
					// 		}
					// 	}
					// },
					// language: {
					// 	validators: {
					// 		notEmpty: {
					// 			message: 'Please select a language'
					// 		}
					// 	}
					// },
					// timezone: {
					// 	validators: {
					// 		notEmpty: {
					// 			message: 'Please select a timezone'
					// 		}
					// 	}
					// }
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap()
				}
			}
		));

		// Step 3
		_validations.push(FormValidation.formValidation(
			_formEl,
			{
				fields: {
					address1: {
						validators: {
							notEmpty: {
								message: 'Address is required'
							}
						}
					},
					postcode: {
						validators: {
							notEmpty: {
								message: 'Postcode is required'
							}
						}
					},
					city: {
						validators: {
							notEmpty: {
								message: 'City is required'
							}
						}
					},
					state: {
						validators: {
							notEmpty: {
								message: 'state is required'
							}
						}
					},
					country: {
						validators: {
							notEmpty: {
								message: 'Country is required'
							}
						}
					},
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap()
				}
			}
		));
	}

	var initAvatar = function () {
		_avatar = new KTImageInput('kt_contact_add_avatar');
	}


	var initToast = function (message, title, type) {

		toastr.options = {
			"closeButton": false,
			"debug": false,
			"newestOnTop": false,
			"progressBar": true,
			"positionClass": "toast-top-right",
			"preventDuplicates": false,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		  };
	
		switch (type) {
			case "success":
				toastr.success(message, title);
				break;
	
			case "info":
				toastr.info(message, title);
				break;
	
			case "warning":
				toastr.warning(message, title);
				break;
	
			  case "error":
				toastr.error(message, title);
				break;
				
			default:
				toastr.success(message, title);
				break;
		}
	}
	

	$(".save-institution").click(function(event){
		event.preventDefault();
	
		let name = document.getElementById("name").value;
		let alias = document.getElementById("alias").value;
		let cat_type = document.getElementById("cat_type").value;
		let phone = document.getElementById("phone").value;
		let email = document.getElementById("email").value;
		let other_details = document.getElementById("other_details").value;
	
		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url: "/institutions/save",
			type:"POST",
			data:{
				name: name,
				alias: alias,
				cat_type: cat_type,
				phone: phone,
				email: email,
				other_details: other_details,
			},
			success:function(response){
				if(response) {

					if (response.resp_code == '000') {
						
						$("#kt_contact_add_form")[0].reset();
						initToast(response.resp_desc, "Setup", "success");
						setTimeout(function(){	
							window.location.href = '/institutions';
						}, 3000);

					} else {
						initToast(response.resp_desc, "Setup","error");
					}
				
				}
			},
		 });
	});


	return {
		// public functions
		init: function () {
			_wizardEl = KTUtil.getById('kt_contact_add');
			_formEl = KTUtil.getById('kt_contact_add_form');

			initWizard();
			initValidation();
			initAvatar();
		}
	};
}();

jQuery(document).ready(function () {
	KTContactsAdd.init();

});


function initToast (message, title, type) {

	toastr.options = {
		"closeButton": false,
		"debug": false,
		"newestOnTop": false,
		"progressBar": true,
		"positionClass": "toast-top-right",
		"preventDuplicates": false,
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	  };

	switch (type) {
		case "success":
			toastr.success(message, title);
			break;

		case "info":
			toastr.info(message, title);
			break;

		case "warning":
			toastr.warning(message, title);
			break;

		  case "error":
			toastr.error(message, title);
			break;
			
		default:
			toastr.success(message, title);
			break;
	}
}


function activate_institution(entity_id) {
	if (confirm('This permission will be activated. Click OK to proceed or Cancel to abort this action.')) {
		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url: "/institutions/actions",
			type:"POST",
			data:{
				entity_id: entity_id,
				action: "A",//Deactivate
			},
			success:function(response){
				if(response) {
	
					if (response.resp_code == '000') {
						
						initToast(response.resp_desc, "Setup", "success");
						setTimeout(function(){	
							window.location.href = '/institutions';
						}, 1000);
	
					} else {
						initToast(response.resp_desc, "Setup","error");
					}
				
				}
			},
		});
	}
}


function deactivate_institution(entity_id) {
	
	if (confirm('This record will be deactivated. Click OK to proceed or Cancel to abort this action.')) {
		
		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url: "/institutions/actions",
			type:"POST",
			data:{
				entity_id: entity_id,
				action: "D",//Deactivate
			},
			success:function(response){
				if(response) {
	
					if (response.resp_code == '000') {
						
						initToast(response.resp_desc, "Setup", "success");
						setTimeout(function(){	
							window.location.href = '/institutions';
						}, 1000);
	
					} else {
						initToast(response.resp_desc, "Setup","error");
					}
				
				}
			},
		 });

	  }
		
}


function activate_electorate(voter_id) {
	if (confirm('This record will be activated. Click OK to proceed or Cancel to abort this action.')) {
		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url: "/electorates/actions",
			type:"POST",
			data:{
				voter_id: voter_id,
				action: "A",//Deactivate
			},
			success:function(response){
				if(response) {
	
					if (response.resp_code == '000') {
						
						initToast(response.resp_desc, "Setup", "success");
						setTimeout(function(){	
							location.reload();
						}, 1000);
	
					} else {
						initToast(response.resp_desc, "Setup","error");
					}
				
				}
			},
		});
	}
}


function deactivate_electorate(voter_id) {
	
	if (confirm('This record will be deactivated. Click OK to proceed or Cancel to abort this action.')) {
		
		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url: "/electorates/actions",
			type:"POST",
			data:{
				voter_id: voter_id,
				action: "D",//Deactivate
			},
			success:function(response){
				if(response) {
	
					if (response.resp_code == '000') {
						
						initToast(response.resp_desc, "Setup", "success");
						setTimeout(function(){	
							location.reload();
						}, 1000);
	
					} else {
						initToast(response.resp_desc, "Setup","error");
					}
				}
			},
		 });

	  }
		
}






