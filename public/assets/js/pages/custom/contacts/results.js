"use strict";

// Class definition
var KTContactsAdd = function () {
	// Base elements
	var _wizardEl;
	var _formEl;
	var _wizard;
	var _avatar;
	var _validations = [];

	// Private functions
	var initWizard = function () {
		// Initialize form wizard
		_wizard = new KTWizard(_wizardEl, {
			startStep: 1, // initial active step number
			clickableSteps: true  // allow step clicking
		});

		// Validation before going to next page
		_wizard.on('beforeNext', function (wizard) {
			// Don't go to the next step yet
			_wizard.stop();

			// Validate form
			var validator = _validations[wizard.getStep() - 1]; // get validator for currnt step
			validator.validate().then(function (status) {
				if (status == 'Valid') {
					document.getElementById('summ_div_name').innerHTML = document.getElementById("div_name").value;
					document.getElementById('summ_service_label').innerHTML = document.getElementById("service_label").value;

					
					document.getElementById('summ_start_date').innerHTML = document.getElementById("kt_datetimepicker_event_startdate").value;
					document.getElementById('summ_end_date').innerHTML = document.getElementById("kt_datetimepicker_event_enddate").value;
					document.getElementById('summ_sender_id').innerHTML = document.getElementById("sms_sender_id").value;

					switch (document.getElementById("show_client_result").value) {
						case "true":
							document.getElementById('summ_show_client_result').innerHTML = "Show Voting Results";
							break;
						case "false":
							document.getElementById('summ_show_client_result').innerHTML = "Hide Voting Results";
							break;
						default:
							document.getElementById('summ_show_client_result').innerHTML = document.getElementById("show_client_result").value;
							break;
					}
					
					switch (document.getElementById("auth_identify").value) {
						case "D":
							document.getElementById('summ_auth_identify').innerHTML = "Student ID";
							break;
						case "M":
							document.getElementById('summ_auth_identify').innerHTML = "Mobile Number";
							break;
						case "E":
							document.getElementById('summ_auth_identify').innerHTML = "Email";
							break;
					
						default:
							document.getElementById('summ_auth_identify').innerHTML = document.getElementById("auth_identify").value;
							break;
					}


					switch (document.getElementById("auth_class").value) {
						case "A":
							document.getElementById('summ_auth_class').innerHTML = "No Password (Activation)";
							break;
						case "P":
							document.getElementById('summ_auth_class').innerHTML = "Password";
							break;
						default:
							document.getElementById('summ_auth_class').innerHTML = document.getElementById("auth_class").value;
							break;
					}
				
					console.log(`step: ${wizard.getStep()}`);
					_wizard.goNext();
					KTUtil.scrollTop();
				} else {
					Swal.fire({
						text: "Sorry, looks like there are some errors detected, please try again.",
						icon: "error",
						buttonsStyling: false,
						confirmButtonText: "Ok, got it!",
						customClass: {
							confirmButton: "btn font-weight-bold btn-light"
						}
					}).then(function () {
						KTUtil.scrollTop();
					});
				}
			});
		});

		// Change Event
		_wizard.on('change', function (wizard) {
			KTUtil.scrollTop();
		});
	}

	var initValidation = function () {
		// Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/

		// Step 1
		_validations.push(FormValidation.formValidation(
			_formEl,
			{
				fields: {
					div_name: {
						validators: {
							notEmpty: {
								message: 'Event Name is required'
							}
						}
					},
					service_label: {
						validators: {
							notEmpty: {
								message: 'Event display name is required'
							}
						}
					},
					// cat_type: {
					// 	validators: {
					// 		notEmpty: {
					// 			message: 'Category is required'
					// 		}
					// 	}
					// }
					// phone: {
					// 	validators: {
					// 		notEmpty: {
					// 			message: 'Phone is required'
					// 		},
					// 		phone: {
					// 			country: 'US',
					// 			message: 'The value is not a valid US phone number. (e.g 5554443333)'
					// 		}
					// 	}
					// },
					// email: {
					// 	validators: {
					// 		notEmpty: {
					// 			message: 'Email is required'
					// 		},
					// 		emailAddress: {
					// 			message: 'The value is not a valid email address'
					// 		}
					// 	}
					// },
					// companywebsite: {
					// 	validators: {
					// 		notEmpty: {
					// 			message: 'Website URL is required'
					// 		}
					// 	}
					// }
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap()
				}
			}
		));

		// Step 2
		_validations.push(FormValidation.formValidation(
			_formEl,
			{
				fields: {
					// Step 2
					// kt_datetimepicker_event_startdate: {
					// 	validators: {
					// 		notEmpty: {
					// 			message: 'Event Start date is required'
					// 		}
					// 	}
					// },
					// kt_datetimepicker_event_enddate: {
					// 	validators: {
					// 		notEmpty: {
					// 			message: 'Event end date is required'
					// 		}
					// 	}
					// },
					// sms_sender_id: {
					// 	validators: {
					// 		notEmpty: {
					// 			message: 'SMS Sender ID is required'
					// 		}
					// 	}
					// },	
					// show_client_result: {
					// 	validators: {
					// 		notEmpty: {
					// 			message: 'Please select whether or not you wish to show voting results.'
					// 		}
					// 	}
					// },
					// auth_identify: {
					// 	validators: {
					// 		notEmpty: {
					// 			message: 'Please select voter authentication identifier'
					// 		}
					// 	}
					// },
					// auth_class: {
					// 	validators: {
					// 		notEmpty: {
					// 			message: 'Please select voter authentication method'
					// 		}
					// 	}
					// }
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap()
				}
			}
		));

		// Step 3
		// _validations.push(FormValidation.formValidation(
		// 	_formEl,
		// 	{
		// 		fields: {
		// 			address1: {
		// 				validators: {
		// 					notEmpty: {
		// 						message: 'Address is required'
		// 					}
		// 				}
		// 			},
		// 			postcode: {
		// 				validators: {
		// 					notEmpty: {
		// 						message: 'Postcode is required'
		// 					}
		// 				}
		// 			},
		// 			city: {
		// 				validators: {
		// 					notEmpty: {
		// 						message: 'City is required'
		// 					}
		// 				}
		// 			},
		// 			state: {
		// 				validators: {
		// 					notEmpty: {
		// 						message: 'state is required'
		// 					}
		// 				}
		// 			},
		// 			country: {
		// 				validators: {
		// 					notEmpty: {
		// 						message: 'Country is required'
		// 					}
		// 				}
		// 			},
		// 		},
		// 		plugins: {
		// 			trigger: new FormValidation.plugins.Trigger(),
		// 			bootstrap: new FormValidation.plugins.Bootstrap()
		// 		}
		// 	}
		// ));
	}

	var initAvatar = function () {
		_avatar = new KTImageInput('kt_contact_add_avatar');
	}


	var initToast = function (message, title, type) {

		toastr.options = {
			"closeButton": false,
			"debug": false,
			"newestOnTop": false,
			"progressBar": true,
			"positionClass": "toast-top-right",
			"preventDuplicates": false,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		  };
	
		switch (type) {
			case "success":
				toastr.success(message, title);
				break;
	
			case "info":
				toastr.info(message, title);
				break;
	
			case "warning":
				toastr.warning(message, title);
				break;
	
			  case "error":
				toastr.error(message, title);
				break;
				
			default:
				toastr.success(message, title);
				break;
		}
	}



	return {
		// public functions
		init: function () {
			_wizardEl = KTUtil.getById('kt_contact_add');
			_formEl = KTUtil.getById('kt_category_add_form');

			initWizard();
			initValidation();
			initAvatar();
		}
	};
}();

jQuery(document).ready(function () {
	KTContactsAdd.init();
});



function initToast (message, title, type) {

	toastr.options = {
		"closeButton": false,
		"debug": false,
		"newestOnTop": false,
		"progressBar": true,
		"positionClass": "toast-top-right",
		"preventDuplicates": false,
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	  };

	switch (type) {
		case "success":
			toastr.success(message, title);
			break;

		case "info":
			toastr.info(message, title);
			break;

		case "warning":
			toastr.warning(message, title);
			break;

		  case "error":
			toastr.error(message, title);
			break;
			
		default:
			toastr.success(message, title);
			break;
	}
}


function deactivate_category(cat_id) {
	
	if (confirm('This record will be deactivated. Click OK to proceed or Cancel to abort this action.')) {
		
		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url: "/categories/actions",
			type:"POST",
			data:{
				cat_id: cat_id,
				action: "D",//Deactivate
			},
			success:function(response){
				if(response) {
	
					if (response.resp_code == '000') {
						
						initToast(response.resp_desc, "Setup", "success");
						setTimeout(function(){	
							// window.location.href = '/categories/';
							location.reload();
						}, 1000);
	
					} else {
						initToast(response.resp_desc, "Setup","error");
					}
				
				}
			},
		 });

	  }
		
}


function activate_category(cat_id) {
	if (confirm('This record will be activated. Click OK to proceed or Cancel to abort this action.')) {
		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url: "/categories/actions",
			type:"POST",
			data:{
				cat_id: cat_id,
				action: "A",//Deactivate
			},
			success:function(response){
				console.log(response);
				if(response) {
	
					if (response.resp_code == '000') {
						
						initToast(response.resp_desc, "Setup", "success");
						setTimeout(function(){	
							// window.location.href = '/permissions';
							location.reload();
						}, 1000);
	
					} else {
						initToast(response.resp_desc, "Setup","error");
					}
				
				}
			},
		});
	}
}


function downloadReport(){

	console.log("download");

	let entity_div_code = document.getElementById("select_event_id").value;

	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url: "/results/download_result",
		type:"POST",
		data:{
			entity_div_code: entity_div_code,
		},
		success:function(response){
			if(response) {
				console.log(response.entity_div_code);
				$(".result_event_header").text(response.event_name)
				$('div.result_div').html(response.result_view);

				$(".download_div").show();
			}
		},
	 });
	 
}
