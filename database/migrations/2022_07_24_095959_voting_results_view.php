<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class VotingResultsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("CREATE VIEW voting_results AS SELECT a.service_label AS plan_name, c.entity_div_code AS entity_div_code, c.cat_name AS cat_name, c.cat_id AS cat_id, n.nom_name AS nom_name,  n.nom_code AS nom_code, n.nom_id AS nom_id, vs.vote_count AS vote_count from voting_nominees n left join voting_summary vs on n.nom_id = vs.nom_id left join voting_categories c on n.cat_id = c.cat_id left join entity_divisions a on c.entity_div_code = a.assigned_code where ((a.active_status = true) and (a.del_status = false) and (n.active_status = true) and (n.del_status = false) and (c.active_status = true) and (c.del_status = false)) order by c.cat_name, vs.vote_count desc"); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("DROP VIEW voting_results"); 
    }
}
