<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntityDivisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entity_divisions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('assigned_code')->index();
            $table->string('entity_id')->index();
            $table->string('div_name')->index();
            $table->string('service_label')->index();
            $table->string('activity_type_code')->index();
            $table->string('image_path')->nullable()->index();
            $table->string('image_data')->nullable()->index();
            $table->dateTime('start_date')->index();
            $table->dateTime('end_date')->index();
            $table->boolean('in_use')->nullable();
            $table->text('comment')->nullable();
            $table->bigInteger('user_id')->index();
            $table->boolean('active_status')->default(true);
            $table->boolean('del_status')->nullable()->default(false);
            $table->timestamp('created_at')->useCurrent()->index();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entity_divisions');
    }
}
