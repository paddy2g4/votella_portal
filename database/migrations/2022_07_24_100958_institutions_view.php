<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InstitutionsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("CREATE VIEw institutions AS SELECT em.entity_id, em.entity_name, em.entity_alias, em.cat_type, ee.phone_number, ee.email, ee.other_details, ee.image_path, ee.image_data, ee.image_type, em.user_id, em.active_status, em.del_status, em.created_at FROM entity_masters em left join entity_extras ee on em.entity_id = ee.entity_id where em.del_status = false"); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("DROP VIEW institutions"); 
    }
}
