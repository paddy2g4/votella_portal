<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWhitelistMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('whitelist_masters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('entity_div_code')->index();
            $table->string('phone_number')->nullable()->default('')->index();
            $table->string('desc')->nullable()->index();
            $table->string('remote_ip')->nullable()->index();
            $table->string('mac_address', 1000)->nullable();
            $table->dateTime('start_date')->nullable()->index();
            $table->dateTime('end_date')->nullable()->index();
            $table->bigInteger('user_id')->index();
            $table->boolean('active_status')->default(true);
            $table->boolean('del_status')->nullable()->default(false);
            $table->string('activity_type', 11)->nullable()->index();
            $table->timestamp('created_at')->useCurrent()->index();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('whitelist_masters');
    }
}
