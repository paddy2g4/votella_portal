<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoterAuthTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voter_auth', function (Blueprint $table) {
            $table->increments('id');
            $table->string('voter_id')->nullable()->index();
            $table->string('username', 100)->default('')->index();
            $table->string('password')->default('')->index();
            $table->boolean('is_activated')->nullable()->default(false);
            $table->boolean('is_notify')->nullable()->default(false);
            $table->string('src', 200)->nullable()->index();
            $table->timestamp('logged_in_at')->nullable()->index();
            $table->timestamp('logged_out_at')->nullable()->index();
            $table->timestamp('voted_at')->nullable()->index();
            $table->boolean('is_voted')->nullable()->default(false);
            $table->string('ip_address')->nullable()->index();
            $table->string('mac_address')->nullable();
            $table->timestamp('vote_started_at')->nullable()->index();
            $table->string('vote_started_ip_address')->nullable()->index();
            $table->integer('user_id')->nullable()->index();
            $table->boolean('active_status')->nullable()->default(true);
            $table->boolean('del_status')->nullable()->default(false);
            $table->timestamp('created_at')->useCurrent()->index();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voter_auth');
    }
}
