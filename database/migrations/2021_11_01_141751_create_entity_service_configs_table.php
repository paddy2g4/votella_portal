<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntityServiceConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entity_service_configs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('entity_div_code')->index();
            $table->string('sms_sender_id')->index();
            $table->boolean('show_client_result');
            $table->string('auth_identify')->index();
            $table->string('auth_class')->index();
            $table->text('comment')->nullable();
            $table->bigInteger('user_id')->index();
            $table->boolean('active_status')->default(true);
            $table->boolean('del_status')->nullable()->default(false);
            $table->timestamp('created_at')->useCurrent()->index();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entity_service_configs');
    }
}
