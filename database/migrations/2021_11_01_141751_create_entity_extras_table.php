<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntityExtrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entity_extras', function (Blueprint $table) {
            $table->increments('id');
            $table->string('entity_id')->default('')->index('entity_extras_entity_id');
            $table->string('phone_number')->default('')->index('entity_extras_phone_number');
            $table->string('email')->nullable()->default('')->index('entity_extras_email');
            $table->string('other_details')->nullable()->default('')->index('entity_extras_other_details');
            $table->string('image_path')->nullable()->default('')->index('entity_extras_image_path');
            $table->string('image_data')->nullable()->default('')->index('entity_extras_image_data');
            $table->string('image_type')->nullable()->default('')->index('entity_extras_image_type');
            $table->text('comment')->nullable();
            $table->boolean('active_status')->nullable()->default(true);
            $table->boolean('del_status')->nullable()->default(false);
            $table->integer('user_id')->nullable()->default(false)->index('entity_extras_user_id');
            $table->timestamp('created_at')->useCurrent()->index('entity_extras_created_at');
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable()->index('entity_extras_updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entity_extras');
    }
}
