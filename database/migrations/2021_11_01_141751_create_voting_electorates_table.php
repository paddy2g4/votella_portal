<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVotingElectoratesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voting_electorates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('entity_div_code')->index();
            $table->string('voter_id')->index();
            $table->string('name')->index();
            $table->string('index_number')->nullable()->index();
            $table->string('mobile_number')->nullable()->index();
            $table->string('email')->nullable()->index();
            $table->string('level')->nullable()->index();
            $table->string('department')->nullable()->index();
            $table->text('other')->nullable();
            $table->bigInteger('user_id')->index();
            $table->boolean('active_status')->default(true);
            $table->boolean('del_status')->nullable()->default(false);
            $table->timestamp('created_at')->useCurrent()->index();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voting_electorates');
    }
}
