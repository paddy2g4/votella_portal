<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVotingCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voting_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('entity_div_code')->index();
            $table->string('cat_id')->index();
            $table->string('cat_name')->index();
            $table->string('cat_desc')->nullable()->index();
            $table->string('image_path')->nullable()->index();
            $table->string('image_data')->nullable()->index();
            $table->text('comment')->nullable();
            $table->bigInteger('user_id')->index();
            $table->boolean('active_status')->default(true);
            $table->boolean('del_status')->nullable()->default(false);
            $table->timestamp('created_at')->useCurrent()->index();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voting_categories');
    }
}
