<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVotingNomineesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voting_nominees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cat_id')->index();
            $table->string('nom_id')->index();
            $table->string('nom_name')->index();
            $table->string('nom_code')->index();
            $table->string('image_path')->nullable()->index();
            $table->string('image_data')->nullable()->index();
            $table->text('comment')->nullable();
            $table->bigInteger('user_id')->index();
            $table->boolean('active_status')->default(true);
            $table->boolean('del_status')->nullable()->default(false);
            $table->timestamp('created_at')->useCurrent()->index();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voting_nominees');
    }
}
