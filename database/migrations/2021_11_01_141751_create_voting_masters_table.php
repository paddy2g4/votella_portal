<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVotingMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voting_masters', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('nom_id')->index();
            $table->bigInteger('vote_info_id')->nullable()->index();
            $table->string('voter_id')->index();
            $table->string('ip_address')->index();
            $table->decimal('vote_count', 11,1)->nullable()->default(0)->index();
            $table->decimal('vote_before', 11,1)->nullable()->default(0)->index();
            $table->decimal('vote_after', 11,1)->nullable()->default(0)->index();
            $table->boolean('active_status')->nullable()->default(true);
            $table->boolean('del_status')->nullable()->default(false);
            $table->timestamp('created_at')->useCurrent()->index();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voting_masters');
    }
}
