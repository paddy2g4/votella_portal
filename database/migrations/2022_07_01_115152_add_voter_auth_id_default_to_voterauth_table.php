<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVoterAuthIdDefaultToVoterauthTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("ALTER TABLE public.voter_auth ALTER COLUMN voter_id DROP NOT NULL"); 
        \DB::statement("ALTER TABLE public.voter_auth ALTER COLUMN voter_id SET DEFAULT (''::text || to_char(currval('voter_auth_id_seq'::regclass), 'FM000000000'::text))"); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("ALTER TABLE public.voter_auth ALTER COLUMN voter_id SET DEFAULT ('')"); 
    }
}
