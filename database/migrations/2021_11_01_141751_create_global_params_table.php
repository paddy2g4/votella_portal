<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGlobalParamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('global_params', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('assigned_code')->index();
            $table->string('param_name')->index();
            $table->string('param_val')->nullable();
            $table->bigInteger('user_id')->index();
            $table->boolean('active_status')->default(true);
            $table->boolean('del_status')->nullable()->default(false);
            $table->timestamp('created_at')->useCurrent()->index();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('global_params');
    }
}
