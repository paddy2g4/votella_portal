<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntityMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entity_masters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('entity_id')->default('')->index('entity_masters_entity_id');
            $table->string('entity_name')->default('')->index('entity_masters_entity_name');
            $table->string('entity_alias')->nullable()->default('')->index('entity_masters_entity_alias');
            $table->string('cat_type')->nullable()->default('')->index('entity_masters_cat_type');
            $table->text('comment')->nullable();
            $table->boolean('active_status')->nullable()->default(true);
            $table->boolean('del_status')->nullable()->default(false);
            $table->integer('user_id')->nullable()->default(false)->index('entity_masters_user_id');
            $table->timestamp('created_at')->useCurrent()->index('entity_masters_created_at');
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable()->index('entity_masters_updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entity_masters');
    }
}
