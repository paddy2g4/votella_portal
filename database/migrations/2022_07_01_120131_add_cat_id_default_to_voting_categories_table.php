<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCatIdDefaultToVotingCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("ALTER TABLE public.voting_categories ALTER COLUMN cat_id DROP NOT NULL"); 
        \DB::statement("ALTER TABLE public.voting_categories ALTER COLUMN cat_id SET DEFAULT (''::text || to_char(currval('voting_categories_id_seq'::regclass), 'FM000000000'::text))"); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("ALTER TABLE public.voting_categories ALTER COLUMN cat_id SET DEFAULT ('')"); 
    }
}
