<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->truncate();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Super Admin',
                'slug' => 'super_admin',
                'active_status' => true,
                'del_status' => false,
                'created_at' => '2021-08-19 16:20:32',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Administrator',
                'slug' => 'admin',
                'active_status' => true,
                'del_status' => false,
                'created_at' => '2021-08-19 16:20:32',
                'updated_at' => NULL,
            )
        ));
        
        
    }
}
