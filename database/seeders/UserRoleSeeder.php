<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users_roles')->truncate();
        
        \DB::table('users_roles')->insert(array (
            0 => 
            array (
                'user_id' => 1,
                'role_id' => 1,
                'active_status' => true,
                'del_status' => false,
                'created_at' => '2022-08-01 16:20:32',
                'updated_at' => NULL,
            )
        ));
        
        
    }
}
