<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->truncate();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'view institutions',
                'slug' => 'view_institutions',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'add institution',
                'slug' => 'add_institution',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'deactivate institution',
                'slug' => 'deactivate_institution',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'view institution details',
                'slug' => 'view_institution_details',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'view events',
                'slug' => 'view_events',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'add events',
                'slug' => 'add_events',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'deactivate events',
                'slug' => 'deactivate_events',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'edit event',
                'slug' => 'edit_event',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'view event details',
                'slug' => 'view_event_details',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'view electorates',
                'slug' => 'view_electorates',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'add electorates',
                'slug' => 'add_electorates',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'deactivate electorates',
                'slug' => 'deactivate_electorates',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'view electorate details',
                'slug' => 'view_electorate_details',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'view categories',
                'slug' => 'view_categories',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'add categories',
                'slug' => 'add_categories',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'deactivate categories',
                'slug' => 'deactivate_categories',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'edit categories',
                'slug' => 'edit_categories',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'view category details',
                'slug' => 'view_category_details',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'view nominees',
                'slug' => 'view_nominees',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'add nominees',
                'slug' => 'add_nominees',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'deactivate nominees',
                'slug' => 'deactivate_nominees',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'edit nominees',
                'slug' => 'edit_nominees',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'view nominee details',
                'slug' => 'view_nominee_details',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'search electorate details',
                'slug' => 'search_electorate_details',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'activate electorate details',
                'slug' => 'activate_electorate_details',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'name' => 'view reports',
                'slug' => 'view_reports',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'name' => 'download reports',
                'slug' => 'download_reports',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'name' => 'view results',
                'slug' => 'view_results',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'name' => 'download results',
                'slug' => 'download_results',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'name' => 'view statistics',
                'slug' => 'view_statistics',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'name' => 'view users',
                'slug' => 'view_users',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'name' => 'add user',
                'slug' => 'add_user',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'name' => 'deactivate user',
                'slug' => 'deactivate_user',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'name' => 'edit user details',
                'slug' => 'edit_user_details',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'name' => 'view user details',
                'slug' => 'view_user_details',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'name' => 'view permissions',
                'slug' => 'view_permissions',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'name' => 'add permission',
                'slug' => 'add_permission',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'name' => 'deactivate permission',
                'slug' => 'deactivate_permission',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'name' => 'edit permission details',
                'slug' => 'edit_permission_details',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'name' => 'view permission details',
                'slug' => 'view_permission_details',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => 41,
                'name' => 'view roles',
                'slug' => 'view_roles',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => 42,
                'name' => 'add roles',
                'slug' => 'add_roles',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => 43,
                'name' => 'deactivate roles',
                'slug' => 'deactivate_roles',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => 44,
                'name' => 'edit role details',
                'slug' => 'edit_role_details',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => 45,
                'name' => 'view role details',
                'slug' => 'view_role_details',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => 46,
                'name' => 'view whitelisted IPs',
                'slug' => 'view_whitelisted_IPs',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => 47,
                'name' => 'add whitelsted IPs',
                'slug' => 'add_whitelsted_IPs',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => 48,
                'name' => 'deactivate whitelisted IP',
                'slug' => 'deactivate_whitelisted_IP',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => 49,
                'name' => 'edit whitelisted IP details',
                'slug' => 'edit_whitelisted_IP_details',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => 50,
                'name' => 'view whitelisted IP details',
                'slug' => 'view_whitelisted_IP_details',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'id' => 51,
                'name' => 'view blacklisted IPs',
                'slug' => 'view_blacklisted_Ips',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'id' => 52,
                'name' => 'add blacklisted IPs',
                'slug' => 'add_blacklisted_IPs',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'id' => 53,
                'name' => 'deactivate blacklisted IPs',
                'slug' => 'deactivate_blacklisted_IPs',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'id' => 54,
                'name' => 'edit blacklisted IP details',
                'slug' => 'edit_blacklisted_IP_details',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'id' => 55,
                'name' => 'view blacklisted IP details',
                'slug' => 'view_blacklisted_IP_details',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'id' => 56,
                'name' => 'view audit trails',
                'slug' => 'view_audit_trails',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            56 => 
            array (
                'id' => 57,
                'name' => 'view audit trail details',
                'slug' => 'view_audit_trail_details',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ),
            57 => 
            array (
                'id' => 58,
                'name' => 'export audit trail report',
                'slug' => 'export_audit_trail_report',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:34:09',
                'updated_at' => NULL,
            ), 
            58 => 
            array (
                'id' => 59,
                'name' => 'Public Graph Viewing',
                'slug' => 'public_graph_view',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:35:00',
                'updated_at' => NULL,
            ),
            59 => 
            array (
                'id' => 60,
                'name' => 'Strongroom Graph Viewing',
                'slug' => 'strongroom_graph_view',
                'active_status' => 1,
                'del_status' => 0,
                'created_at' => '2021-09-04 09:56:00',
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}
