<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->truncate();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Paddy',
                'email' => 'paddy@gmail.com',
                'password' => '$2y$10$M6GowEpqJF1D8m6xCWgsLu9jsR7UW1YMBz.XqHyJJFgT7hbhFmUyC',
                'active_status' => true,
                'del_status' => false,
                'created_at' => '2022-08-01 16:20:32',
                'updated_at' => NULL,
            )
        ));
        
        
    }
}
