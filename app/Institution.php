<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Institution extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //only the `deleted` event will get logged automatically
    protected static $recordEvents = ['retrieved','created','updated','deleted'];
    
    protected $table = 'institutions';

    /**
     * Validation rules
     *
     * @var array
     */
    public  $rules = [
    ];

    public function logActivity($activity){
        activity()->performedOn($this)->log($activity);
    }
}
