<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Whitelist extends Model implements Auditable
{  
    use \OwenIt\Auditing\Auditable;
    protected static $recordEvents = ['retrieved','created','updated','deleted'];

    protected $table = 'whitelist_masters';

    public function entitydivision()
    {
        return $this->belongsTo('App\EntityDivision', 'entity_div_code', 'assigned_code');
    }

    public function created_by()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public $fillable = [
        'entity_div_code',
        'phone_number',
        'desc',
        'remote_ip',
        'mac_address',
        'activity_type',
        'start_date',
        'end_date',
        'user_id',
        'active_status',
        'del_status',
        'created_at',
        'updated_at'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'entity_div_code'=>['required','min:1','max:100','string'],
        'phone_number'=>['nullable','max:200',"string"],
        'desc'=>['nullable','max:200',"string"],
        'remote_ip'=>['string','max:200'],
        'mac_address'=>['nullable','string','max:200'],
        'start_date'=>['nullable','date'],
        'end_date'=>['nullable','date'],
        'active_status'=>['integer','max:2'],
        'del_status'=>['integer','max:2'],
    ];

    public function logActivity($activity){
        activity()->performedOn($this)->log($activity);
    }

}
