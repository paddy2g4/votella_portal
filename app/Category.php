<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
// use Spatie\Activitylog\Traits\LogsActivity;
// use Spatie\Activitylog\LogOptions;

class Category extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    // use LogsActivity;

    //only the `deleted` event will get logged automatically
    protected static $recordEvents = ['retrieved','created','updated','deleted'];

    protected $table = 'voting_categories';

    // public function getActivitylogOptions(): LogOptions
    // {
    //     return LogOptions::defaults()
    //     ->logOnly(['name', 'text']);
    //     // Chain fluent methods for configuration options
    // }

    public function entitydivision()
    {
        return $this->belongsTo('App\EntityDivision', 'entity_div_code', 'assigned_code');
    }

    public $fillable = [
        'entity_div_code',
        'cat_id',
        'cat_name',
        'cat_desc',
        'image_path',
        'image_data',
        'comment',
        'user_id',
        'active_status',
        'del_status',
        'created_at',
        'updated_at'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'entity_div_code'=>['required','min:1','max:100','string'],
        'cat_id'=>['required','min:1','max:200',"string"],
        'cat_name'=>['required','min:1','max:200',"string"],
        'cat_desc'=>['required','min:1','max:200',"string"],
        'image_path'=>['required','min:1','max:255',"string"],
        'image_data'=>['min:1','max:255',"string"],
        'comment'=>['min:1','max:255','string'],
        "user_id" => 'required|integer|exists:users,id',
        'act_status'=>['integer','max:2'],
        'del_status'=>['integer','max:2'],
    ];


    public function logActivity($activity){
        activity()->performedOn($this)->log($activity);
    }

}
