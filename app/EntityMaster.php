<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class EntityMaster extends Model implements Auditable
{  
    use \OwenIt\Auditing\Auditable;
    //only the `deleted` event will get logged automatically
    protected static $recordEvents = ['retrieved','created','updated','deleted'];


    public function contact()
    {
        return $this->hasOne('App\EntityExtra', 'entity_id', 'entity_id');
    }

    public $fillable = [
        'entity_id',
        'entity_name',
        'cat_type',
        'comment',
        'user_id',
        'act_status',
        'del_status',
        'created_at',
        'updated_at'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'entity_id'=>['required','min:1','max:100','string'],
        'entity_name'=>['required','min:1','max:200',"regex:/^[\p{L}\p{P}\p{Zs}\p{N}]+$/"],
        'cat_type'=>['required','min:1','max:100','string'],
        'comment'=>['min:1','max:255','string'],
        "user_id" => 'required|integer|exists:users,id',
        'act_status'=>['integer','max:2'],
        'del_status'=>['integer','max:2'],
    ];

    public function logActivity($activity){
        activity()->performedOn($this)->log($activity);
    }
}
