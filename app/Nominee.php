<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Nominee extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected static $recordEvents = ['retrieved','created','updated','deleted'];
    
    protected $table = 'voting_nominees';

    public function category()
    {
        return $this->belongsTo('App\Category', 'cat_id', 'cat_id');
    }

    public $fillable = [
        'cat_id',
        'nom_id',
        'nom_name',
        'nom_code',
        'image_path',
        'image_data',
        'user_id',
        'active_status',
        'del_status',
        'created_at',
        'updated_at'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'cat_id'=>['required','min:1','max:100','string'],
        'nom_id'=>['required','min:1','max:200',"string"],
        'nom_name'=>['required','min:1','max:200',"string"],
        'nom_code'=>['max:200',"string"],
        'image_path'=>['max:255',"string"],//['min:1','max:255',"string"],
        'image_data'=>['max:255',"string"]
        // "user_id" => 'required|integer|exists:users,id',
        // 'act_status'=>['integer','max:2'],
        // 'del_status'=>['integer','max:2'],
    ];

    public function logActivity($activity){
        activity()->performedOn($this)->log($activity);
    }

}
