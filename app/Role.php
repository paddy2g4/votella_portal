<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Role extends Model implements Auditable
{
   use \OwenIt\Auditing\Auditable;

    //only the `deleted` event will get logged automatically
    protected static $recordEvents = ['retrieved','created','updated','deleted'];


    public function permissions() {

        return $this->belongsToMany(Permission::class,'roles_permissions');
            
     }
     
     public function users() {
     
        return $this->belongsToMany(User::class,'users_roles');
            
     }

   public function logActivity($activity){
      activity()->performedOn($this)->log($activity);
   }
}
