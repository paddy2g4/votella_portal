<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Permissions\HasPermissionsTrait;
use OwenIt\Auditing\Contracts\Auditable;

class User extends Authenticatable implements Auditable
{
    use Notifiable;
    use HasPermissionsTrait; //Import The Trait
    use \OwenIt\Auditing\Auditable;

    protected static $recordEvents = ['retrieved','created','updated','deleted'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    // public function roles() {
     
    //     return $this->hasOne(UserRole::class,'user_id', 'id');
    //     // return $this->hasOne(Role::class, 'entity_div_code', 'assigned_code');
            
    //  }

    public function logActivity($activity){
        activity()->performedOn($this)->log($activity);
    }
}
