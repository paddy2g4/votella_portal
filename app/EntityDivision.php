<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class EntityDivision extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //only the `deleted` event will get logged automatically
    protected static $recordEvents = ['retrieved','created','updated','deleted'];

    public function entitymaster()
    {
        return $this->belongsTo('App\EntityMaster', 'entity_id');
    }

    public function entitycontact()
    {
        return $this->belongsTo('App\EntityContact', 'entity_id');
    }

    public function service_config()
    {
        return $this->hasOne('App\EntityServiceConfig', 'entity_div_code', 'assigned_code');
    }

    public $fillable = [
        'entity_id',
        'assigned_code',
        'div_name',
        'service_label',
        'activity_type_code',
        'image_path',
        'image_data',
        'start_date',
        'end_date',
        'comment',
        'user_id',
        'active_status',
        'del_status',
        'created_at',
        'updated_at'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'entity_id'=>['required','min:1','max:100','string'],
        'assigned_code'=>['required','min:1','max:100','string'],
        'div_name'=>['required','min:1','max:200',"string"],
        'service_label'=>['required','min:1','max:200',"string"],
        'activity_type_code'=>['required','min:1','max:50',"string"],
        'image_path'=>['required','min:1','max:255',"string"],
        'image_data'=>['min:1','max:255',"string"],
        'cat_type'=>['required','min:1','max:100','string'],
        'comment'=>['min:1','max:255','string'],
        "user_id" => 'required|integer|exists:users,id',
        'act_status'=>['integer','max:2'],
        'del_status'=>['integer','max:2'],
    ];

    /**
    * The attributes that should be cast.
    *
    * @var array
    */
    protected $casts = [
        'start_at' => 'datetime:Y-m-d h:i:s',
        'end_at' => 'datetime:Y-m-d h:i:s'
    ];


    public function logActivity($activity){
        activity()->performedOn($this)->log($activity);
    }
}
