<?php

namespace App\Imports;

use App\Electorate;
use App\EntityServiceConfig;
use App\VoterAuth;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\Importable;

class ElectoratesImport implements ToModel, WithHeadingRow//ToModel
{
    use Importable;

    public function  __construct($entity_div_code, $user_id)
    {
        $this->entity_div_code = $entity_div_code;
        $this->user_id = $user_id;
    }

    // public function collection(Collection $rows)
    // {
    //     foreach ($rows as $row) 
    //     {
    //         dd($row);
    //         // Electorate::create([
    //         //     'name' => $row[0],
    //         // ]);
    //     }
    // }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
      
            $voter_login = $this->getLoginField($row);
            $voter_id = $this->new_id();
            $Electorate = Electorate::firstOrCreate(
                [
                    'entity_div_code' => $this->entity_div_code,
                    'name' => $row['name'],
                    'index_number' => $row['index_number'],
                ],
                [
                    'entity_div_code' => $this->entity_div_code,
                    'voter_id'     => $voter_id,
                    'name'     => $row['name'],
                    'index_number'    => $row['index_number'], 
                    'mobile_number'    => $row['mobile_number'], 
                    'email'    => $row['email'], 
                    'level'    => $row['level'], 
                    'department'    => $row['department'], 
                    'other'    => $row['others'], 
                    'user_id' => $this->user_id,
                ]
            );
            
            if($Electorate){
                $VoterAuth=new VoterAuth;
                $VoterAuth->voter_id = $voter_id;
                $VoterAuth->username = $voter_login;
                $VoterAuth->user_id = $this->user_id;
                $VoterAuth->save();

                // $last_audit = $Electorate->audits()->latest()->first();
                // activity()->performedOn($Electorate)->withProperties(['audit_id' => $last_audit->id])->log("$Electorate->name record created");
                return $Electorate;
            }
            return false;
            
    }


    public function new_id(){
        $electorate_rec = Electorate::orderBy('created_at','DESC')->first();
        $electorate_rec_id = ($electorate_rec) ? $electorate_rec->id : 0 ;
        return ''.str_pad($electorate_rec_id + 1, 8, "0", STR_PAD_LEFT);
    }

    public function validateDuplicateRec($row){
        return Electorate::where([ 
                ['entity_div_code', '=', $this->entity_div_code], ['name', '=', $row['name']], ['index_number', '=', $row['index_number']], ['level', '=', $row['level']], ['department', '=', $row['department']]
            ])->exists();
    }


    public function getLoginField($row){
        $voter_login=$row['index_number'];
        $entity_service_config=EntityServiceConfig::where([['entity_div_code', '=', $this->entity_div_code], ['active_status', '=', true], ['del_status', '=', false] ])->first();
        if ($entity_service_config->auth_identify) {
            switch ($entity_service_config->auth_identify) {
                case 'D':
                    $voter_login=$row['index_number'];
                    break;
                case 'M':
                    $voter_login=$row['mobile_number'];
                    break;  
                case 'E':
                    $voter_login=$row['email'];
                    break;
                default:
                    $voter_login=$row['index_number'];
                    break;
            }
        }
        return $voter_login;
    }
}