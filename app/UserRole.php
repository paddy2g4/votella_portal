<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class UserRole extends Model implements Auditable
{
   use \OwenIt\Auditing\Auditable;
    //only the `deleted` event will get logged automatically
   protected static $recordEvents = ['retrieved','created','updated','deleted'];

   protected $table = 'users_roles';

    public function roles() {

        return $this->hasOne(Roles::class,'role_id', 'id');
            
     }
     
     public function users() {
     
        return $this->hasOne(User::class,'user_id', 'id');
            
     }

     public function logActivity($activity){
      activity()->performedOn($this)->log($activity);
  }
}
