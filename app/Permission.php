<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Permission extends Model implements Auditable
{
   use \OwenIt\Auditing\Auditable;

   protected static $recordEvents = ['retrieved','created','updated','deleted'];

    public function roles() {

        return $this->belongsToMany(Role::class,'roles_permissions');
            
     }
     
     public function users() {
     
        return $this->belongsToMany(User::class,'users_permissions');
            
     }

   public function logActivity($activity){
      activity()->performedOn($this)->log($activity);
   }
}
