<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\UserRole;
use App\EntityDivision;
use App\EntityServiceConfig;
use App\Whitelist;
use Validator;
use Carbon\Carbon;


class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function users_index()
    {   

        if (\Auth::user()->can('view_users')) {
            // dd($user->hasRole('super_admin')); //will return true, if user has role
            // dd($user->givePermissionsTo('create-tasks'));// will return permission, if not null
            // dd($user->can('list-admin')); // will return true, if user has permission
            // $data = User::where('active_status', 1)->orderBy('created_at','DESC')->first();
            

            $data = \DB::table('users')
            ->leftJoin('users_roles', 'users.id', '=', 'users_roles.user_id')
            ->leftJoin('roles', 'users_roles.role_id', '=', 'roles.id')
            ->select('users.id', 'users.name', 'users.email', 'users.created_at', 'users.active_status', 'roles.name as role_name')
            ->where('users.del_status',0)
            ->orderBy('users.created_at','DESC')
            ->paginate(10);
            // dd($data);

            $User=new User;
            $User->logActivity("Users page viewed");

            // $data = User::where('active_status', 1)->orderBy('created_at','DESC')->paginate(10);

            return view('user_management.user', ['data' => $data]);
        }else{
            abort(403, 'Unauthorized action.');
        }
        
    }


    public function users_new()
    {   
        (!\Auth::user()->can('add_user')) ? abort(403, 'Unauthorized action.') : null;

        if (\Auth::user()->hasRole('super_admin')){
            $roles = Role::where('active_status', 1)->orderBy('created_at','ASC')->get();
        }else{
            $roles = Role::where('active_status', 1)->whereNotIn('slug', ['super_admin'])->orderBy('created_at','ASC')->get();
        }
        return view('user_management.users_form', compact('roles'));
    }

     /**
     * Save inputs.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function saveUser(Request $request)
    {
        (!\Auth::user()->can('add_user')) ? abort(403, 'Unauthorized action.') : null;

        $rules = array(
            'name'=>['required','min:1','max:200','string'],
            "email" => 'required|email|unique:users,email',
            'password'=>['required','min:1','max:255','confirmed','string'],
            'role'=>['required', 'integer'],
        );

        $User=new User;
        if($User->rules){
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return false;
            }
        }

        $User->name = $request->name;
        $User->email = $request->email;
        $User->password = bcrypt($request->password);
        $User->name = $request->name;
        $User->save();
        $user_role = Role::where('id',$request->role)->first();
        $User->roles()->attach($user_role);
        
        $last_audit = $User->audits()->latest()->first();
        activity()->performedOn($User)->withProperties(['audit_id' => $last_audit->id])->log("$User->name record created");

        if ($User) {
            return response()->json(['resp_code'=>'000', 'resp_desc' => 'Record has been saved successfully']);
        }else {
            return response()->json(['resp_code'=>'999', 'resp_desc' => 'Record could not be saved successfully']);
        } 
    }
        


    public function user_edit($user)
    {   
        (!\Auth::user()->can('edit_user_details')) ? abort(403, 'Unauthorized action.') : null;

        $user_details = \DB::table('users')
            ->leftJoin('users_roles', 'users.id', '=', 'users_roles.user_id')
            ->leftJoin('roles', 'users_roles.role_id', '=', 'roles.id')
            ->select('users.id', 'users.name', 'users.email', 'users.created_at', 'users.active_status', 'users_roles.role_id', 'roles.name as role_name')
            ->where('users.id',$user)
            ->orderBy('users.created_at','DESC')
            ->first();

        $roles = Role::where('active_status', 1)->orderBy('created_at','ASC')->get();
        return view('user_management.users_edit_form', ['data' => $user_details, 'user_id'=>$user, 'roles' => $roles]);
    }


    public function user_view($user)
    {   
        (!\Auth::user()->can('view_user_details')) ? abort(403, 'Unauthorized action.') : null;

        $user_details = \DB::table('users')
            ->leftJoin('users_roles', 'users.id', '=', 'users_roles.user_id')
            ->leftJoin('roles', 'users_roles.role_id', '=', 'roles.id')
            ->select('users.id', 'users.name', 'users.email', 'users.created_at', 'users.active_status', 'roles.name as role_name')
            ->where('users.id',$user)
            ->orderBy('users.created_at','DESC')
            ->first();

        $roles = Role::where('active_status', 1)->orderBy('created_at','ASC')->get();
        return view('user_management.users_view_form', ['data' => $user_details, 'user_id' => $user, 'roles' => $roles]);
    }



    public function updateUser(Request $request)
    {
        (!\Auth::user()->can('edit_user_details')) ? abort(403, 'Unauthorized action.') : null;

        $rules = array(
            'user_id'=>['required','integer'],
            'name'=>['required','min:1','max:200','string'],
            'email'=>['required','min:1','max:200','email'],
            'role_id'=>['required','min:1','max:200','integer'],
        );

        if($rules){
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return false;
            }
        }

        $User = User::find($request->user_id);
        $User->name = $request->name;
        $User->email = $request->email;
        $User->save();

        if ($User) {
            $userrole=UserRole::where('user_id', $request->user_id)->update(['role_id' => $request->role_id]);
        }

        $last_audit = $User->audits()->latest()->first();
        activity()->performedOn($User)->withProperties(['audit_id' => $last_audit->id])->log("$User->name record updated");
  
  
        if ($User) {
            return response()->json(['resp_code'=>'000', 'resp_desc' => 'Record has been updated successfully']);
        }else {
            return response()->json(['resp_code'=>'999', 'resp_desc' => 'Record could not be updated successfully']);
        } 
    }
    


    public function updateUserAction(Request $request)
    {
        (!\Auth::user()->can('deactivate_user')) ? abort(403, 'Unauthorized action.') : null;

        $rules = array(
            'user_id'=>['required','integer'],
            'action'=>['required','min:1','max:10','string'],
        );

        if($rules){
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return false;
            }
        }

        $User = User::find($request->user_id);
        $User->active_status = ($request->action == "A") ? true : false;
        $User->save();

        $last_audit = $User->audits()->latest()->first();
        activity()->performedOn($User)->withProperties(['audit_id' => $last_audit->id])->log("$User->name's status updated.");


        if ($User) {
            return response()->json(['resp_code'=>'000', 'resp_desc' => 'Record has been updated successfully']);
        }else {
            return response()->json(['resp_code'=>'999', 'resp_desc' => 'Record could not be updated successfully']);
        } 
    }

}
