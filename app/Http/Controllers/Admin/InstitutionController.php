<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\EntityMaster;
use App\EntityExtra;
use App\Institution;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Validator;


class InstitutionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (\Auth::user()->can('view_institutions')) {
            $institutions = Institution::orderBy("created_at", "desc")->paginate(10);
            return View('setups.institutions')->with('data',$institutions);
        }else{
            abort(403, 'Unauthorized action.');
        }
    }


    public function institution_view($entity_id)
    {   
        if (!\Auth::user()->can('view_institution_details')) {
            abort(403, 'Unauthorized action.');
        }

        $institutions = Institution::where('entity_id',$entity_id)->orderBy("created_at", "desc")->first();

        $Institution=new Institution;
        $Institution->logActivity("$institutions->entity_name record viewed");

        return view('setups.institutions_view_form', ['data' => $institutions, 'entity_id' => $entity_id]);
    }


    /**
     * Show the application new form.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function new()
    {
        return View('setups.institutions_form', []);
    } 
    
    
    /**
     * Save inputs.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function saveInstitution(Request $request)
    {

        (!\Auth::user()->can('add_institution')) ? abort(403, 'Unauthorized action.') : null;

        // dd($request->all());

        $rules = array(
            'name'=>['required','min:1','max:200','string'],
            'alias'=>['required','min:1','max:200','string'],
            'cat_type'=>['required','min:1','max:100','string'],
            "phone" => ['required','min:9','max:50',"string"],
            'email'=>['email','max:200'],
            // 'image_path'=>['min:1','max:100','string'],
            // 'image_data'=>['min:1','max:100','string'],
            // 'image_type'=>['min:1','max:100','string'],
        );

        $EntityMaster=new EntityMaster;
        $EntityExtra=new EntityExtra;
        if($EntityMaster->rules){
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
               return false;
            }
        }
        
        $user_id = (\Auth::user()) ? \Auth::user()->id : null ;

        $entity = EntityMaster::orderBy('created_at','DESC')->first();
        $entity_id = ($entity) ? $entity->id : 0;
        $entity_id = $this->new_entity_id($entity_id);

        $EntityMaster->entity_id = $entity_id;
        $EntityMaster->entity_name = $request->name;
        $EntityMaster->entity_alias = $request->alias;
        $EntityMaster->cat_type = $request->cat_type;
        $EntityMaster->user_id = $user_id;
        $EntityMaster->save();

        $EntityExtra->entity_id = $entity_id;
        $EntityExtra->phone_number = $request->phone;
        $EntityExtra->email = $request->email;
        $EntityExtra->other_details = $request->other_details;
        // $EntityExtra->image_path = $request->cat_type;
        // $EntityExtra->image_data = $request->cat_type;
        // $EntityExtra->image_type = $request->cat_type;
        $EntityExtra->user_id = $user_id;
        $EntityExtra->save();

        // Get last Audit
        $last_audit = $EntityMaster->audits()->latest()->first();
        activity()->performedOn($EntityMaster)->withProperties(['audit_id' => $last_audit->id])->log("$EntityMaster->entity_name record created");

        // return response()->json(['resp_code'=>'000', 'resp_desc' => 'Record has been saved successfully']);
        $msg='Record has been saved successfully!';
        toastr()->success($msg);
        return \Redirect::route('institutions')->withMessage($msg);
    }



    public function updateInstitutionAction(Request $request)
    {
        (!\Auth::user()->can('deactivate_institution')) ? abort(403, 'Unauthorized action.') : null;

        $rules = array(
            'entity_id'=>'required|string|exists:entity_masters,entity_id',
            'action'=>['required','min:1','max:10','string'],
        );

        if($rules){
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return false;
            }
        }

        $entity_details = EntityMaster::where('entity_id', $request->entity_id)->first();
        $EntityMaster = EntityMaster::find($entity_details->entity_id);
        $EntityMaster->active_status = ($request->action == "A") ? true : false;
        $EntityMaster->save();

        // Get last Audit
        $last_audit = $entity_details->audits()->latest()->first();
        activity()->performedOn($entity_details)->withProperties(['audit_id' => $last_audit->id])->log("$entity_details->entity_name's status updated.");

        if ($EntityMaster) {
            return response()->json(['resp_code'=>'000', 'resp_desc' => 'Record has been updated successfully']);
        }else {
            return response()->json(['resp_code'=>'999', 'resp_desc' => 'Record could not be updated successfully']);
        } 
    }


    public function new_entity_id($id){
        return ''.str_pad($id + 1, 8, "0", STR_PAD_LEFT);
        // return IdGenerator::generate(['table' => 'entity_masters', 'length' => 8, 'prefix' => 1, 'reset_on_prefix_change' => true]);
    }
}
