<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\EntityDivision;
use App\EntityServiceConfig;
use App\Category;
use Validator;
use Carbon\Carbon;
use OwenIt\Auditing\Facades\Auditor;
use \OwenIt\Auditing\Models\Audit;
use \Spatie\Activitylog\Models\Activity;


class AuditTrailsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {      
        if (!\Auth::user()->can('view_audit_trails')) {
            abort(403, 'Unauthorized action.');
        }

        $audits = Activity::orderBy('created_at', 'desc')->paginate(10);
        return view('security.audit_trail', ['audits' => $audits, 'service_label' => '', 'entity_div_code' => '']);
    }


}
