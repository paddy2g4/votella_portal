<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\EntityDivision;
use App\EntityServiceConfig;
use App\Category;
use App\Electorate;
use App\VoterAuth;
use Validator;
use Carbon\Carbon;
use App\Imports\ElectoratesImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ElectoratesExport;


class ElectorateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($entity_div_code)
    {   
        (!\Auth::user()->can('view_electorates')) ? abort(403, 'Unauthorized action.') : null;

        $event_details = EntityDivision::where('assigned_code', $entity_div_code)->where('active_status', 1)->first();
        $service_label = $event_details->service_label;
        
        $electorates = Electorate::where('entity_div_code', $entity_div_code)->where('active_status', 1)->paginate(10);

        return view('setups.electorate', ['data' => $electorates, 'service_label' => $service_label, 'entity_div_code' => $entity_div_code]); 
    }


    public function electorate_view($voter_id)
    {   
        (!\Auth::user()->can('view_electorate_details')) ? abort(403, 'Unauthorized action.') : null;

        $electorates = Electorate::where('voter_id', $voter_id)->orderBy("created_at", "desc")->first();
        // Get last Audit
        $last_audit = $electorates->audits()->latest()->first();
        activity()->performedOn($electorates)->withProperties(['audit_id' => $last_audit->id])->log("$electorates->name record viewed");

        return view('setups.electorate_view_form', ['data' => $electorates, 'voter_id' => $voter_id]);
    }


    /**
     * Show the application new form.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function new($entity_div_code)
    {
        (!\Auth::user()->can('add_electorates')) ? abort(403, 'Unauthorized action.') : null;

        $event_details = EntityDivision::where('assigned_code', $entity_div_code)->where('active_status', 1)->first();
        $service_label = $event_details->service_label;

        return View('setups.electorate_form', [ 'service_label' => $service_label, 'entity_div_code' => $entity_div_code]);
    } 
    
    
    /**
     * Save inputs.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function saveElectorate(Request $request)
    {
        (!\Auth::user()->can('add_electorates')) ? abort(403, 'Unauthorized action.') : null;

        $rules = array(
            'entity_div_code'=>['required','min:1','max:30','string'],
            'voter_name'=>['required','string'],
            'voter_phone'=>['string', 'max:255'],
            'voter_email'=>['email', 'max:255'],
            'voter_department'=>['string', 'max:255'],
            'voter_level'=>['string', 'max:255']
        );

        $Electorate=new Electorate;
        if($Electorate->rules){
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return false;
            }
        }

        $user_id = (\Auth::user()) ? \Auth::user()->id : null ;

        $electorate_rec = Electorate::orderBy('created_at','DESC')->first();
        $electorate_rec_id = ($electorate_rec) ? $electorate_rec->id : 0 ;
        $voter_id = $this->new_id($electorate_rec_id);

        $Electorate->entity_div_code = $request->entity_div_code;
        $Electorate->voter_id = $voter_id;
        $Electorate->name = $request->voter_name;
        $Electorate->mobile_number = $request->voter_phone;
        $Electorate->email = $request->voter_email;
        $Electorate->department = $request->voter_department;
        $Electorate->level = $request->voter_level;
        $Electorate->user_id = $user_id;
        $Electorate->save();

        if ($Electorate) {

            $VoterAuth=new VoterAuth;
            $VoterAuth->voter_id = $voter_id;
            $VoterAuth->username = $request->voter_phone;
            $VoterAuth->user_id = $user_id;
            $VoterAuth->save();

            $last_audit = $Electorate->audits()->latest()->first();
            activity()->performedOn($Electorate)->withProperties(['audit_id' => $last_audit->id])->log("$Electorate->name record created");
            

            $resp_code = "000";
            $resp_desc = "Record has been created successfully";
        }else{
            $resp_code = "999";
            $resp_desc = "Record creation could not be completed." ;
        }

        return response()->json(['resp_code'=>$resp_code, 'resp_desc' => $resp_desc]);
    }



    public function updateElectorateAction(Request $request)
    {
        (!\Auth::user()->can('deactivate_electorates')) ? abort(403, 'Unauthorized action.') : null;

        $rules = array(
            'voter_id'=>'required|string|exists:voting_electorates,voter_id',
            'action'=>['required','min:1','max:10','string'],
        );

        if($rules){
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return false;
            }
        }

        $elec = Electorate::where('voter_id', $request->voter_id)->first();
        $Electorate = Electorate::find($elec->id);
        $Electorate->active_status = ($request->action == "A") ? true : false;
        $Electorate->save();

        $voter = VoterAuth::where('voter_id', $request->voter_id)->first();
        $VoterAuth = VoterAuth::find($voter->voter_id);
        $VoterAuth->active_status = ($request->action == "A") ? true : false;
        $VoterAuth->save();

        $last_audit = $Electorate->audits()->latest()->first();
        activity()->performedOn($Electorate)->withProperties(['audit_id' => $last_audit->id])->log("$elec->name record updated");

        if ($Electorate) {
            return response()->json(['resp_code'=>'000', 'resp_desc' => 'Record has been updated successfully']);
        }else {
            return response()->json(['resp_code'=>'999', 'resp_desc' => 'Record could not be updated successfully']);
        } 
    }


    public function import_electorate(Request $request) {
        // dd($request->import_file);
        $user_id = (\Auth::user()) ? \Auth::user()->id : null ;
        Excel::import(new ElectoratesImport($request->entity_div_code, $user_id),$request->import_file);

        $msg='Electorate Records have been uploaded and saved successfully!';
        toastr()->success($msg);
        return \Redirect::route('electorates', ['entity_div_code' => $request->entity_div_code])->withMessage($msg);
    }


    public function export_electorate_sample_upload() 
    {
        $file= public_path(). "/votella_electorate_sample.xlsx";
        $headers = ['Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',];
        return \Response::download($file, 'electorates_sample.xlsx', $headers);
    }



    public function new_id($id){
        return ''.str_pad($id + 1, 8, "0", STR_PAD_LEFT);
        // return IdGenerator::generate(['table' => 'entity_masters', 'length' => 8, 'prefix' => 1, 'reset_on_prefix_change' => true]);
    }

}
