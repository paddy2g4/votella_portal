<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\EntityDivision;
use App\EntityServiceConfig;
use App\Category;
use Validator;
use Carbon\Carbon;
use OwenIt\Auditing\Facades\Auditor;


class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($entity_div_code)
    {   
        if (!\Auth::user()->can('view_categories')) {
            abort(403, 'Unauthorized action.');
        } 

        $event_details = EntityDivision::where('assigned_code', $entity_div_code)->where('active_status', 1)->first();
        $service_label = $event_details->service_label;
        
        $categories = \DB::table('voting_categories')
            ->leftJoin('voting_nominees', 'voting_categories.cat_id', '=', 'voting_nominees.cat_id')
            ->select('voting_categories.entity_div_code', 'voting_categories.cat_id', 'voting_categories.cat_name', 'voting_categories.active_status', 'voting_categories.created_at',  \DB::raw('count(voting_nominees.id) nom_counts'))
            ->where('voting_categories.entity_div_code', $entity_div_code)
            ->orderBy('voting_categories.created_at','ASC')
            ->orderBy('voting_categories.active_status','DESC')
            ->orderBy('voting_categories.created_at','DESC')
            ->groupBy('voting_categories.entity_div_code', 'voting_categories.cat_id', 'voting_categories.cat_name', 'voting_categories.active_status', 'voting_categories.created_at')
            ->paginate(10);

        $Category=new Category;
        $Category->logActivity("$service_label Categories was viewed");

        return view('setups.category', ['data' => $categories, 'service_label' => $service_label, 'entity_div_code' => $entity_div_code]);
    }


    /**
     * Show the application new form.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function new($entity_div_code)
    {
        (!\Auth::user()->can('add_categories')) ? abort(403, 'Unauthorized action.') : null;

        $event_details = EntityDivision::where('assigned_code', $entity_div_code)->where('active_status', 1)->first();
        $service_label = $event_details->service_label;

        return View('setups.category_form', [ 'service_label' => $service_label, 'entity_div_code' => $entity_div_code]);
    } 
    
    
    /**
     * Save inputs.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function saveCategory(Request $request)
    {
        (!\Auth::user()->can('add_categories')) ? abort(403, 'Unauthorized action.') : null;

        $rules = array(
            'entity_div_code'=>['required','min:1','max:30','string'],
            'cat_name'=>['required'],
            'cat_desc'=>['nullable','max:200',"string"],
            // 'image_path'=>['min:1','max:100','string'],
            // 'image_data'=>['min:1','max:100','string'],
            // 'image_type'=>['min:1','max:100','string'],
        );

        $searchForValue = ',';
        if( strpos($request->cat_name, $searchForValue) !== false ) {
            $cat_arr = explode (",", $request->cat_name);

            foreach ($cat_arr as $cat_name){

                $Category=new Category;
                if($Category->rules){
                    $validator = Validator::make($request->all(), $rules);
                    if ($validator->fails()) {
                    return false;
                    }
                }
                
                $user_id = (\Auth::user()) ? \Auth::user()->id : null ;

                $Category->entity_div_code = $request->entity_div_code;
                $Category->cat_name = $cat_name;
                $Category->cat_desc = $cat_name;
                $Category->comment = $request->comment;
                $Category->user_id = $user_id;
                $Category->save();

                // Get last Audit
                $last_audit = $Category->audits()->latest()->first();
                activity()->performedOn($Category)->withProperties(['audit_id' => $last_audit->id])->log("$Category->cat_name record created");
                
                // $Category=new Category;
                // $Category->logActivityDetail($Category, "$Category->cat_name record created");

            }

        }else{

            $Category=new Category;
            if($Category->rules){
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    return false;
                }
            }
            
            $user_id = (\Auth::user()) ? \Auth::user()->id : null ;

            $Category->entity_div_code = $request->entity_div_code;
            $Category->cat_name = $request->cat_name;
            $Category->cat_desc = $request->cat_desc;
            $Category->comment = $request->comment;
            $Category->user_id = $user_id;
            $Category->save();

            // Get last Audit
            $last_audit = $Category->audits()->latest()->first();
            activity()->performedOn($Category)->withProperties(['audit_id' => $last_audit->id])->log("$Category->cat_name record created");
            
        }


        return response()->json(['resp_code'=>'000', 'resp_desc' => 'Record has been saved successfully']);
    }



    public function category_edit($cat_id)
    {   
        (!\Auth::user()->can('edit_categories')) ? abort(403, 'Unauthorized action.') : null;

        $category_details = Category::where('cat_id', $cat_id)->where('active_status', 1)->first();
        $entity_div_code = $category_details->entitydivision->assigned_code;
        return view('setups.category_edit_form', ['data' => $category_details, 'cat_id' => $cat_id, 'entity_div_code'=>$entity_div_code]);
    }


    public function category_view($cat_id)
    {   
        (!\Auth::user()->can('view_category_details')) ? abort(403, 'Unauthorized action.') : null;

        $category_details = Category::where('cat_id', $cat_id)->first();
        
        // Get last Audit
        // $last_audit = $category_details->audits()->latest()->first();
        // activity()->performedOn($category_details)->withProperties(['audit_id' => $last_audit->id])->log("$category_details->cat_name record viewed");

        $Category=new Category;
        $Category->logActivity("$category_details->cat_name record viewed");

        return view('setups.category_view_form', ['data' => $category_details, 'cat_id' => $cat_id]);
    }

    

    public function updateCategory(Request $request)
    {
        (!\Auth::user()->can('edit_categories')) ? abort(403, 'Unauthorized action.') : null;

        $rules = array(
            "cat_id" => 'required|string|exists:voting_categories,cat_id',
            "cat_name" => ['required','min:1','max:200','string'],
            'cat_desc'=>['nullable','max:200',"string"],
        );

        if($rules){
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return false;
            }
        }


        $category_details = Category::where('cat_id', $request->cat_id)->first();
        $Category = Category::find($category_details->id);
        $Category->cat_name = $request->cat_name;
        $Category->cat_desc = $request->cat_desc;
        $Category->save();

        // Get last Audit
        $last_audit = $Category->audits()->latest()->first();
        activity()->performedOn($Category)->withProperties(['audit_id' => $last_audit->id])->log("$category_details->cat_name record updated");
  
        if ($Category) {
            return response()->json(['resp_code'=>'000', 'resp_desc' => 'Record has been updated successfully']);
        }else {
            return response()->json(['resp_code'=>'999', 'resp_desc' => 'Record could not be updated successfully']);
        } 
    }


    public function updateCategoryAction(Request $request)
    {
        (!\Auth::user()->can('edit_categories')) ? abort(403, 'Unauthorized action.') : null;

        $rules = array(
            "cat_id" => 'required|string|exists:voting_categories,cat_id',
            'action'=>['required','min:1','max:10','string'],
        );

        if($rules){
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return false;
            }
        }

        $status = ($request->action == "A") ? true : false;
        $category_details = Category::where('cat_id', $request->cat_id)->first();
        $Category = Category::find($category_details->id);
        $Category->active_status = $status;
        $Category->save();

        // Get last Audit
        $last_audit = $category_details->audits()->latest()->first();
        activity()->performedOn($category_details)->withProperties(['audit_id' => $last_audit->id])->log("$category_details->cat_name's status updated.");

        if ($Category) {
            return response()->json(['resp_code'=>'000', 'resp_desc' => 'Record has been updated successfully']);
        }else {
            return response()->json(['resp_code'=>'999', 'resp_desc' => 'Record could not be updated successfully']);
        } 
    }

    public function new_id($id){
        return ''.str_pad($id + 1, 8, "0", STR_PAD_LEFT);
        // return IdGenerator::generate(['table' => 'entity_masters', 'length' => 8, 'prefix' => 1, 'reset_on_prefix_change' => true]);
    }

}
