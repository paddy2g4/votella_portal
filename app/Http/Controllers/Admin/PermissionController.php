<?php
    namespace App\Http\Controllers\Admin;

    use App\Http\Controllers\Controller;
    use App\Permission;
    use App\Role;
    use App\User;
    use Illuminate\Http\Request;
    use Validator;
    use Carbon\Carbon;
    
    class PermissionController extends Controller
    {   

        /**
         * Create a new controller instance.
         *
         * @return void
         */
        public function __construct()
        {
            $this->middleware('auth');
        }

        /**
         * Show the application dashboard.
         *
         * @return \Illuminate\Contracts\Support\Renderable
         */
        public function permissions_index()
        {   
            (!\Auth::user()->can('view_permissions')) ? abort(403, 'Unauthorized action.') : null;

            $data = Permission::where('del_status', 0)
            ->orderBy('active_status','DESC')
            ->orderBy('created_at','DESC')
            ->paginate(10);
            return view('admin.permissions', ['data' => $data]);
        }


        public function permissions_view($permission)
        {   
            (!\Auth::user()->can('view_permission_details')) ? abort(403, 'Unauthorized action.') : null;
            
            $permission_details = Permission::find($permission);
            return view('admin.permissions_view_form', ['data' => $permission_details, 'permission_id' => $permission]);
        }


        /**
         * Show the application dashboard.
         *
         * @return \Illuminate\Contracts\Support\Renderable
         */
        public function permissions_new()
        {   
            (!\Auth::user()->can('add_permission')) ? abort(403, 'Unauthorized action.') : null;

            return view('admin.permissions_form', []);  
        }


        /**
         * Save inputs.
         *
         * @return \Illuminate\Contracts\Support\Renderable
         */
        public function savePermission(Request $request)
        {
            (!\Auth::user()->can('add_permission')) ? abort(403, 'Unauthorized action.') : null;

            $rules = array(
                'name'=>['required','min:1','max:200','string'],
                'slug'=>['required','min:1','max:200','string'],
            );

            $Permission=new Permission;
            if($Permission->rules){
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    return false;
                }
            }

            $Permission->slug = trim($request->slug);
            $Permission->name = $request->name;
            $Permission->save();

            if ($Permission) {
                return response()->json(['resp_code'=>'000', 'resp_desc' => 'Record has been saved successfully']);
            }else {
                return response()->json(['resp_code'=>'999', 'resp_desc' => 'Record could not be saved successfully']);
            } 
        }
        

        public function permissions_edit($permission)
        {   
            (!\Auth::user()->can('edit_permission_details')) ? abort(403, 'Unauthorized action.') : null;
            
            $permission_details = Permission::find($permission);

            return view('admin.permissions_edit_form', ['data' => $permission_details, 'permission_id' => $permission]);
        }


        public function updatePermission(Request $request)
        {
            (!\Auth::user()->can('edit_permission_details')) ? abort(403, 'Unauthorized action.') : null;

            $rules = array(
                'permission_id'=>['required','integer'],
                'name'=>['required','min:1','max:200','string'],
                'slug'=>['required','min:1','max:200','string'],
            );

            if($rules){
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    return false;
                }
            }

            $Permission = Permission::find($request->permission_id);
            $Permission->name = $request->name;
            $Permission->slug = trim($request->slug);
            $Permission->save();

            if ($Permission) {
                return response()->json(['resp_code'=>'000', 'resp_desc' => 'Record has been updated successfully']);
            }else {
                return response()->json(['resp_code'=>'999', 'resp_desc' => 'Record could not be updated successfully']);
            } 
        }
    


        public function updatePermissionAction(Request $request)
        {

            (!\Auth::user()->can('deactivate_permission')) ? abort(403, 'Unauthorized action.') : null;

            $rules = array(
                'permission_id'=>['required','integer'],
                'action'=>['required','min:1','max:10','string'],
            );

            if($rules){
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    return false;
                }
            }

            $Permission = Permission::find($request->permission_id);
            $Permission->active_status = ($request->action == "A") ? true : false;
            $Permission->save();

            if ($Permission) {
                return response()->json(['resp_code'=>'000', 'resp_desc' => 'Record has been updated successfully']);
            }else {
                return response()->json(['resp_code'=>'999', 'resp_desc' => 'Record could not be updated successfully']);
            } 
        }



        public function roles_index()
        {   
            (!\Auth::user()->can('view_roles')) ? abort(403, 'Unauthorized action.') : null;

            $data = Role::where('del_status', 0)->orderBy('active_status','DESC')->orderBy('created_at','DESC')->paginate(10);
            return view('admin.roles', ['data' => $data]);
        }


        /**
         * Show the application dashboard.
         *
         * @return \Illuminate\Contracts\Support\Renderable
         */
        public function roles_new()
        {   
            (!\Auth::user()->can('add_roles')) ? abort(403, 'Unauthorized action.') : null;

            $permissions = Permission::where('active_status', 1)->orderBy('created_at','ASC')->get();
            return view('admin.roles_form', compact('permissions'));
        }


        /**
         * Save inputs.
         *
         * @return \Illuminate\Contracts\Support\Renderable
         */
        public function saveRole(Request $request)
        {
            (!\Auth::user()->can('add_roles')) ? abort(403, 'Unauthorized action.') : null;

            $rules = array(
                'name'=>['required','min:1','max:200','string'],
                'slug'=>['required','min:1','max:200','string'],
                'role_permission'=>['required'],
            );

            $Role=new Role;
            if($Role->rules){
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    return false;
                }
            }

            $Role->slug = trim($request->slug);
            $Role->name = $request->name;
            $Role->save();

            

            foreach ($request->role_permission as $perm_key => $perm_value) {
                $role_permission = Permission::where('slug',$perm_value)->first();
                if ($role_permission) {
                    $Role->permissions()->attach($role_permission);
                }
            }

            // $last_audit = $Role->audits()->latest()->first();
            activity()->performedOn($Role)->log("$Role->name record with permissions created.");

            if ($Role) {
                return response()->json(['resp_code'=>'000', 'resp_desc' => 'Record has been saved successfully']);
            }else {
                return response()->json(['resp_code'=>'999', 'resp_desc' => 'Record could not be saved successfully']);
            } 
        }



        public function role_edit($role)
        {   

            (!\Auth::user()->can('edit_role_details')) ? abort(403, 'Unauthorized action.') : null;
            
            $role_details = Role::find($role);
            // dd($role_details->permissions);

            $permissions = Permission::where('active_status', 1)->orderBy('created_at','ASC')->get();

            return view('admin.roles_edit_form', ['data' => $role_details, 'role_id' => $role, 'permissions' => $permissions]);
        }


        public function role_view($role)
        {   
            (!\Auth::user()->can('view_role_details')) ? abort(403, 'Unauthorized action.') : null;
            
            $role_details = Role::find($role);
            $permissions = Permission::where('active_status', 1)->orderBy('created_at','ASC')->get();

            return view('admin.roles_view_form', ['data' => $role_details, 'role_id' => $role, 'permissions' => $permissions]);
        }

        

        public function updateRole(Request $request)
        {

            (!\Auth::user()->can('edit_role_details')) ? abort(403, 'Unauthorized action.') : null; 

            $rules = array(
                'role_id'=>['required','integer'],
                'name'=>['required','min:1','max:200','string'],
                'slug'=>['required','min:1','max:200','string'],
                'role_permission'=>['required'],
            );

            if($rules){
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    return false;
                }
            }

            $Role = Role::find($request->role_id);
            $Role->name = $request->name;
            $Role->slug = trim($request->slug);
            $Role->save();

            \DB::table('roles_permissions')->where('role_id', '=', $request->role_id)->delete();

            foreach ($request->role_permission as $perm_key => $perm_value) {
                $role_permission = Permission::where('slug',$perm_value)->first();
                if ($role_permission) {
                    $Role->permissions()->attach($role_permission);
                }
            }

            // $last_audit = $Role->audits()->latest()->first();
            activity()->performedOn($Role)->log("$Role->name record updated. Permissions have been realigned.");

            if ($Role) {
                return response()->json(['resp_code'=>'000', 'resp_desc' => 'Record has been updated successfully']);
            }else {
                return response()->json(['resp_code'=>'999', 'resp_desc' => 'Record could not be updated successfully']);
            } 
        }
    


        public function updateRoleAction(Request $request)
        {

            (!\Auth::user()->can('deactivate_roles')) ? abort(403, 'Unauthorized action.') : null;

            $rules = array(
                'role_id'=>['required','integer'],
                'action'=>['required','min:1','max:10','string'],
            );

            if($rules){
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    return false;
                }
            }

            $status = ($request->action == "A") ? true : false;
            $Role = Role::find($request->role_id);
            $Role->active_status = $status;
            $Role->save();

            // Get last Audit
            $last_audit = $Role->audits()->latest()->first();
            activity()->performedOn($Role)->withProperties(['audit_id' => $last_audit->id])->log("$Role->name's status updated.");

            if ($Role) {
                return response()->json(['resp_code'=>'000', 'resp_desc' => 'Record has been updated successfully']);
            }else {
                return response()->json(['resp_code'=>'999', 'resp_desc' => 'Record could not be updated successfully']);
            } 
        }


        // public function Permission()
        // {   
        //     $super_permission = Permission::where('slug','list-admin')->first();
        //     $admin_permission = Permission::where('slug','list-user')->first();
        //     $verify_permission = Permission::where('slug','verify-electorate')->first();
        //     // $manager_permission = Permission::where('slug', 'edit-users')->first();
    
        //     //RoleTableSeeder.php

        //     $super_admin_role = new Role();
        //     $super_admin_role->slug = 'super_admin';
        //     $super_admin_role->name = 'Super Admin';
        //     $super_admin_role->save();
        //     $super_admin_role->permissions()->attach($super_permission);

        //     $admin_role = new Role();
        //     $admin_role->slug = 'admin';
        //     $admin_role->name = 'Administrator';
        //     $admin_role->save();
        //     $admin_role->permissions()->attach($admin_permission);

        //     $verify_role = new Role();
        //     $verify_role->slug = 'verifier';
        //     $verify_role->name = 'Verification Officer';
        //     $verify_role->save();
        //     $verify_role->permissions()->attach($verify_permission);
    
    
        //     $super_admin_role = Role::where('slug','super_admin')->first();
        //     $admin_role = Role::where('slug','admin')->first();
        //     $verify_role = Role::where('slug','verifier')->first();
        //     // $manager_role = Role::where('slug', 'manager')->first();

            
    
        //     $createTasks = new Permission();
        //     $createTasks->slug = 'list-admin';
        //     $createTasks->name = 'List All Administrators';
        //     $createTasks->save();
        //     $createTasks->roles()->attach($super_admin_role);

        //     $createTasks = new Permission();
        //     $createTasks->slug = 'list-user';
        //     $createTasks->name = 'List All Users';
        //     $createTasks->save();
        //     $createTasks->roles()->attach($admin_role);
    
        //     $editUsers = new Permission();
        //     $editUsers->slug = 'verify-electorate';
        //     $editUsers->name = 'Verify An Electorate';
        //     $editUsers->save();
        //     $editUsers->roles()->attach($verify_role);
    
        //     $super_admin_role = Role::where('slug','super_admin')->first();
        //     $admin_role = Role::where('slug','admin')->first();
        //     $verify_role = Role::where('slug','verifier')->first();
        //     $super_perm = Permission::where('slug','list-admin')->first();
        //     $admin_perm = Permission::where('slug','list-user')->first();
        //     $verifier_perm = Permission::where('slug','erify-electorate')->first();
    
        //     $developer = new User();
        //     $developer->name = 'Paddy Super Admin';
        //     $developer->email = 'paddy@gmail.com';
        //     $developer->password = bcrypt('12345678');
        //     $developer->save();
        //     $developer->roles()->attach($super_admin_role);
        //     $developer->permissions()->attach($super_perm);
    
        //     $admin = new User();
        //     $admin->name = 'Jitesh Admin';
        //     $admin->email = 'jadmin@gmail.com';
        //     $admin->password = bcrypt('123456');
        //     $admin->save();
        //     $admin->roles()->attach($admin_role);
        //     $admin->permissions()->attach($admin_perm);

        //     $verifier = new User();
        //     $verifier->name = 'Meniya Verifier';
        //     $verifier->email = 'verifier@gmail.com';
        //     $verifier->password = bcrypt('123456');
        //     $verifier->save();
        //     $verifier->roles()->attach($verify_role);
        //     $verifier->permissions()->attach($verifier_perm);
    
            
        //     return redirect()->back();
        // }
    }