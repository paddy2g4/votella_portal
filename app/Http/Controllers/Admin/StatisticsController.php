<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\EntityDivision;
use App\VoterAuth;
use Validator;
use Carbon\Carbon;
use OwenIt\Auditing\Facades\Auditor;
use \OwenIt\Auditing\Models\Audit;
use \Spatie\Activitylog\Models\Activity;


class StatisticsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {      
        (!\Auth::user()->can('view_statistics')) ? abort(403, 'Unauthorized action.') : null;

        $electorates=VoterAuth::
        where('entity_divisions.in_use', true)->
        where('voter_auth.active_status',1)
            ->leftJoin('voting_electorates', 'voter_auth.voter_id', '=', 'voting_electorates.voter_id')
            ->leftJoin('entity_divisions', 'voting_electorates.entity_div_code', '=', 'entity_divisions.assigned_code');

        $total_electorates_count=$electorates->count();
        // dd($electorates->get());
        $electorates1 = clone $electorates; 
        $electorates2 = clone $electorates; 
        $electorates3 = clone $electorates; 
        $electorates4= clone $electorates; 

        $verified_electorates_count=$electorates1->where('is_activated', 1)->count();
        $notverified_electorates_count=$electorates2->where('is_activated', 0)->count();
        $voted_electorates_count=$electorates3->where('is_voted', 1)->count();
        $notvoted_electorates_count=$electorates4->where('is_voted', 0)->count();
        
        return view('stats.statistics', ['total_electorates_count' => $total_electorates_count, 'verified_electorates_count' => $verified_electorates_count, 'notverified_electorates_count' => $notverified_electorates_count, 'voted_electorates_count' => $voted_electorates_count, 'notvoted_electorates_count' => $notvoted_electorates_count]);
    }
    
    
    public function get_data()
    {      
        (!\Auth::user()->can('view_statistics')) ? abort(403, 'Unauthorized action.') : null;

        $electorates=VoterAuth::where('entity_divisions.in_use', true)->where('voter_auth.active_status',1)
            ->leftJoin('voting_electorates', 'voter_auth.voter_id', '=', 'voting_electorates.voter_id')
            ->leftJoin('entity_divisions', 'voting_electorates.entity_div_code', '=', 'entity_divisions.assigned_code');

        $total_electorates_count=$electorates->count();
        $electorates1 = clone $electorates; 
        $electorates2 = clone $electorates; 
        $electorates3 = clone $electorates; 
        $electorates4= clone $electorates; 

        $verified_electorates_count=$electorates1->where('is_activated', 1)->count();
        $notverified_electorates_count=$electorates2->where('is_activated', 0)->count();
        $voted_electorates_count=$electorates3->where('is_voted', 1)->count();
        $notvoted_electorates_count=$electorates4->where('is_voted', 0)->count();


        return response()->json(['total_electorates_count' => $total_electorates_count, 'verified_electorates_count' => $verified_electorates_count, 'notverified_electorates_count' => $notverified_electorates_count, 'voted_electorates_count' => $voted_electorates_count, 'notvoted_electorates_count' => $notvoted_electorates_count]);        
    }


    public function public_stats_index()
    {      
        (!\Auth::user()->can('public_graph_view')) ? abort(403, 'Unauthorized action.') : null;

        $electorates=VoterAuth::where('entity_divisions.in_use', true)->where('voter_auth.active_status',1)
            ->leftJoin('voting_electorates', 'voter_auth.voter_id', '=', 'voting_electorates.voter_id')
            ->leftJoin('entity_divisions', 'voting_electorates.entity_div_code', '=', 'entity_divisions.assigned_code');

        $total_electorates_count=$electorates->count();
        $electorates1 = clone $electorates; 
        $electorates2 = clone $electorates; 
        $electorates3 = clone $electorates; 
        $electorates4= clone $electorates; 

        $verified_electorates_count=$electorates1->where('is_activated', 1)->count();
        $notverified_electorates_count=$electorates2->where('is_activated', 0)->count();
        $voted_electorates_count=$electorates3->where('is_voted', 1)->count();
        $notvoted_electorates_count=$electorates4->where('is_voted', 0)->count();
        
        return view('stats.public_graph', ['total_electorates_count' => $total_electorates_count, 'verified_electorates_count' => $verified_electorates_count, 'notverified_electorates_count' => $notverified_electorates_count, 'voted_electorates_count' => $voted_electorates_count, 'notvoted_electorates_count' => $notvoted_electorates_count]);
    }
    
    
    public function public_stats_get_data()
    {      
        (!\Auth::user()->can('public_graph_view')) ? abort(403, 'Unauthorized action.') : null;

        $electorates=VoterAuth::where('entity_divisions.in_use', true)->where('voter_auth.active_status',1)
            ->leftJoin('voting_electorates', 'voter_auth.voter_id', '=', 'voting_electorates.voter_id')
            ->leftJoin('entity_divisions', 'voting_electorates.entity_div_code', '=', 'entity_divisions.assigned_code');

        $total_electorates_count=$electorates->count();
        $electorates1 = clone $electorates; 
        $electorates2 = clone $electorates; 
        $electorates3 = clone $electorates; 
        $electorates4= clone $electorates; 

        $verified_electorates_count=$electorates1->where('is_activated', 1)->count();
        $notverified_electorates_count=$electorates2->where('is_activated', 0)->count();
        $voted_electorates_count=$electorates3->where('is_voted', 1)->count();
        $notvoted_electorates_count=$electorates4->where('is_voted', 0)->count();


        return response()->json(['total_electorates_count' => $total_electorates_count, 'verified_electorates_count' => $verified_electorates_count, 'notverified_electorates_count' => $notverified_electorates_count, 'voted_electorates_count' => $voted_electorates_count, 'notvoted_electorates_count' => $notvoted_electorates_count]);        
    }

}
