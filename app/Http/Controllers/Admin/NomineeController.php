<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\EntityDivision;
use App\EntityServiceConfig;
use App\Category;
use App\Nominee;
use Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
// Use Image;



class NomineeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($cat_id)
    {   
        (!\Auth::user()->can('view_nominees')) ? abort(403, 'Unauthorized action.') : null;

        $category_details = Category::where('cat_id', $cat_id)->where('active_status', 1)->first();
        $category_name = $category_details->cat_name;
        $event_name = $category_details->entitydivision->service_label;
        $entity_div_code = $category_details->entitydivision->assigned_code;
        
        $nominees = Nominee::where('cat_id', $cat_id)
        ->orderBy('active_status','DESC')
        ->orderBy('created_at','DESC')
        ->paginate(10);

        return view('setups.nominee', ['data' => $nominees, 'category_name' => $category_name, 'event_name'=>$event_name, 'entity_div_code'=>$entity_div_code, 'cat_id' => $cat_id]);
    }


    
    public function nominee_edit($nom_id)
    {   
        (!\Auth::user()->can('edit_nominees')) ? abort(403, 'Unauthorized action.') : null;

        $nom_details = \DB::table('voting_nominees')
            ->leftJoin('voting_categories', 'voting_nominees.cat_id', '=', 'voting_categories.cat_id')
            ->select('voting_nominees.id', 'voting_nominees.nom_id', 'voting_nominees.nom_name', 'voting_nominees.image_path as nom_pic', 'voting_nominees.active_status', 'voting_nominees.created_at', 'voting_categories.cat_name', 'voting_categories.cat_id')
            ->where('voting_nominees.nom_id',$nom_id)
            ->orderBy('voting_nominees.created_at','DESC')
            ->first();

        $cat_details = Category::where('cat_id', $nom_details->cat_id)->first();
        $entity_div_code = $cat_details->entity_div_code;

        $categories = Category::where('entity_div_code', $entity_div_code)->where('active_status', 1)->get();

        return view('setups.nominees_edit_form', ['data' => $nom_details, 'categories'=>$categories, 'nom_id'=>$nom_id]);
    }


    public function nominee_view($nom_id)
    {   
        (!\Auth::user()->can('view_nominee_details')) ? abort(403, 'Unauthorized action.') : null;

        $nom_details = \DB::table('voting_nominees')
            ->leftJoin('voting_categories', 'voting_nominees.cat_id', '=', 'voting_categories.cat_id')
            ->select('voting_nominees.id', 'voting_nominees.nom_id', 'voting_nominees.nom_name', 'voting_nominees.image_path as nom_pic', 'voting_nominees.active_status', 'voting_nominees.created_at', 'voting_categories.cat_name')
            ->where('voting_nominees.nom_id',$nom_id)
            ->orderBy('voting_nominees.created_at','DESC')
            ->first();

        $Nominee=new Nominee;
        $Nominee->logActivity("$nom_details->cat_name record viewed");

        return view('setups.nominees_view_form', ['data' => $nom_details, 'nom_id' => $nom_id]);
    }


    /**
     * Show the application new form.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function new($cat_id)
    {
        (!\Auth::user()->can('add_nominees')) ? abort(403, 'Unauthorized action.') : null;

        $category_details = Category::where('cat_id', $cat_id)->where('active_status', 1)->first();
        $category_name = $category_details->cat_name;
        $event_name = $category_details->entitydivision->service_label;
        $service_label = $event_name;
        return View('setups.nominee_form', [ 'service_label'=>$service_label, 'category_name' => $category_name, 'event_name'=>$event_name, 'cat_id' => $cat_id]);
    } 
    
    
    /**
     * Save inputs.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function saveNominee(Request $request)
    {
        (!\Auth::user()->can('add_nominees')) ? abort(403, 'Unauthorized action.') : null;

        // dd($request->all());
        $rules = array(
            'cat_id'=>['required','min:1','max:30','string'],
            'nom_name'=>['required','string', 'min:1','max:100'],
            'profile_avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            // 'image_path'=>['min:1','max:100','string'],
            // 'image_data'=>['min:1','max:100','string'],
            // 'image_type'=>['min:1','max:100','string'],
        );

        // dd($request->all());

        // $searchForValue = ',';
        // if( strpos($request->nom_name, $searchForValue) !== false ) {
        //     $item_arr = explode (",", $request->nom_name);

        //     foreach ($item_arr as $nom_name){

        //         $Nominee=new Nominee;
        //         if($Nominee->rules){
        //             $validator = Validator::make($request->all(), $rules);
        //             if ($validator->fails()) {
        //             return false;
        //             }
        //         }
                
        //         if (\Auth::user()) {
        //             $user_id = \Auth::user()->id;
        //         }

        //         $nom_pic_path="";
        //         $nom_pic_name="";

        //         $this->saveNomineeData($Nominee, $request->cat_id, $nom_name, $nom_pic_path, $nom_pic_name);
            
        //     }

        // }else{

            $Nominee=new Nominee;
            if($Nominee->rules){
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    return false;
                }
            }

            $nom_pic_path="";
            $nom_pic_name="";

            $nominee_data=$this->generate_nominee_id();
            $nom_id=$nominee_data[0];
            $nom_code=$nominee_data[1];

            $absolute_path=config('app.media_path');
            $db_absolute_path='/media_assets/nominees';

            $imageName = $nom_id.'_'.time().'.'.$request->profile_avatar->extension();
            $moved=$request->profile_avatar->move($absolute_path, $imageName);

            $nom_pic_name=$imageName;
            $nom_pic_path=$db_absolute_path.'/'.$nom_pic_name;
            $nom_full_path=$absolute_path.'/'.$nom_pic_name;

            copy($nom_full_path, public_path('/assets/votella/').$nom_pic_name);

            $this->saveNomineeData($Nominee, $nom_id, $nom_code, $request->cat_id, $request->nom_name, $nom_pic_path, $nom_pic_name);
        
        // }

        toastr()->success('Record has been saved successfully!');
        return \Redirect::back();

        // return response()->json(['resp_code'=>'000', 'resp_desc' => 'Record has been saved successfully']);
    }


    public function updateNominee(Request $request)
    {
        (!\Auth::user()->can('edit_nominees')) ? abort(403, 'Unauthorized action.') : null; 

        $rules = array(
            "nom_id" => 'required|string|exists:voting_nominees,nom_id',
            "cat_id" => 'required|string|exists:voting_categories,cat_id',
            'nom_name'=>['required','min:1','max:200','string'],
        );

        if($rules){
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return false;
            }
        }

        $nominee_details = Nominee::where('nom_id', $request->nom_id)->first();
        $Nominee = Nominee::find($nominee_details->id);
        $Nominee->cat_id = $request->cat_id;
        $Nominee->nom_name = $request->nom_name;
        $Nominee->save();

        $last_audit = $Nominee->audits()->latest()->first();
        activity()->performedOn($Nominee)->withProperties(['audit_id' => $last_audit->id])->log("$nominee_details->nom_name record updated");
  
        if ($Nominee) {
            return response()->json(['resp_code'=>'000', 'resp_desc' => 'Record has been updated successfully']);
        }else {
            return response()->json(['resp_code'=>'999', 'resp_desc' => 'Record could not be updated successfully']);
        } 
    }



    public function saveNomineeData($Nominee, $nom_id, $nom_code, $cat_id, $nom_name, $nom_pic_path, $nom_pic_name) {

        if (\Auth::user()) {
            $user_id = \Auth::user()->id;
        }

        $Nominee->cat_id = $cat_id;
        $Nominee->nom_id = $nom_id;
        $Nominee->nom_name = $nom_name;
        $Nominee->nom_code = $nom_code;
        $Nominee->image_path = $nom_pic_path;
        $Nominee->image_data = $nom_pic_name;
        $Nominee->user_id = $user_id;
        $Nominee->save();

        $last_audit = $Nominee->audits()->latest()->first();
        activity()->performedOn($Nominee)->withProperties(['audit_id' => $last_audit->id])->log("$Nominee->nom_name record created");
        
        return $Nominee;
    }



    public function updateNomineeAction(Request $request)
    {
        (!\Auth::user()->can('deactivate_nominees')) ? abort(403, 'Unauthorized action.') : null;

        $rules = array(
            "nom_id" => 'required|string|exists:voting_nominees,nom_id',
            'action'=>['required','min:1','max:10','string'],
        );

        if($rules){
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return false;
            }
        }

        $status = ($request->action == "A") ? true : false;
        $nominee_details = Nominee::where('nom_id', $request->nom_id)->first();
        $Nominee = Nominee::find($nominee_details->id);
        $Nominee->active_status = $status;
        $Nominee->save();

        $last_audit = $Nominee->audits()->latest()->first();
        activity()->performedOn($Nominee)->withProperties(['audit_id' => $last_audit->id])->log("$nominee_details->nom_name's status updated.");

        if ($Nominee) {
            return response()->json(['resp_code'=>'000', 'resp_desc' => 'Record has been updated successfully']);
        }else {
            return response()->json(['resp_code'=>'999', 'resp_desc' => 'Record could not be updated successfully']);
        } 
    }


    public function generate_nominee_id(){
        $nom_rec = Nominee::orderBy('created_at','DESC')->first();
        $id = ($nom_rec) ? $nom_rec->id : 0 ;
        $nom_id = $this->new_id($id);
        $nom_code = $this->new_id($id);
        return [$nom_id, $nom_code];
    }


    public function new_id($id){
        return ''.str_pad($id + 1, 7, "0", STR_PAD_LEFT);
        // return IdGenerator::generate(['table' => 'entity_masters', 'length' => 8, 'prefix' => 1, 'reset_on_prefix_change' => true]);
    }

    // public function new_nom_code($id){
    //     return ''.str_pad($id + 1, 3, "0", STR_PAD_LEFT);
    // }

}
