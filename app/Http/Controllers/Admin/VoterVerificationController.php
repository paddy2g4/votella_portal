<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\EntityDivision;
use App\EntityServiceConfig;
use App\Category;
use App\Electorate;
use App\VoterAuth;
use Validator;
use Carbon\Carbon;


class VoterVerificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {      
        (!\Auth::user()->can('search_electorate_details')) ? abort(403, 'Unauthorized action.') : null;

        $voters_list = Electorate::where('entity_divisions.in_use', true)->where('voting_electorates.active_status', 1)
        ->leftJoin('entity_divisions', 'voting_electorates.entity_div_code', '=', 'entity_divisions.assigned_code')
        ->orderBy('voting_electorates.created_at')->select('voting_electorates.voter_id', 'voting_electorates.id', 'voting_electorates.name', 'voting_electorates.mobile_number', 'voting_electorates.index_number', 'voting_electorates.email', 'voting_electorates.level', 'voting_electorates.department')->get();

        return view('admin.verify_voter_form', ['voters_list'=> $voters_list]);
    }


    /**
     * Show the application find_voter form.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function find_voter(Request $request)
    {
        (!\Auth::user()->can('search_electorate_details')) ? abort(403, 'Unauthorized action.') : null;

        $rules = array(
            'voter_id'=>['required','min:1','max:100','string'],
        );
        $Electorate=new Electorate;
        if($Electorate->rules){
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return false;
            }
        }

        $user_id = (\Auth::user()) ? \Auth::user()->id : null ;

        $voters_list = Electorate::where('voter_id', $request->voter_id)->where('active_status', 1)->orderBy('created_at', 'DESC')->with('voter')->select('voter_id', 'id', 'name', 'mobile_number', 'index_number', 'email', 'level', 'department')->first();
        $vote_status = "";
        $account_status = "";
        $account_status_val = "";

        $account_status_val=$voters_list->voter->is_activated;

        $Electorate->logActivity("$voters_list->name record searched.");
        
        $vote_status = ($voters_list->voter->is_voted == 1) ? "Voted" : "Not Voted" ;
        $account_status = ($account_status_val == 1) ? "Account Activated" : "Pending Activation" ;

        
        return response()->json(['resp_code'=>'000', 'resp_desc' => 'Voter Record found', 'voter_id' => $voters_list->voter_id, 'name' => $voters_list->name, 'mobile_number' => $voters_list->mobile_number, 'index_number'=>$voters_list->index_number, 'email' => $voters_list->email, 'level' => $voters_list->level, 'department' => $voters_list->department, 'vote_status'=>$vote_status, 'account_status' => $account_status, 'account_status_val'=>$account_status_val]);
        // return View('admin.verify_voter_form', [ 'voter_id' => $voters_list->voter_id, 'name' => $voters_list->name, 'mobile_number' => $voters_list->mobile_number, 'index_number'=>$voters_list->index_number, 'email' => $voters_list->email, 'level' => $voters_list->level, 'department' => $voters_list->department]);
    } 
    
    
    /**
     * Save inputs.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function account_action(Request $request)
    {
        (!\Auth::user()->can('activate_electorate_details')) ? abort(403, 'Unauthorized action.') : null;

        $rules = array(
            'voter_id'=>['required','min:1','max:100','string'],
            'action_status'=>['required'],
        );

        $VoterAuth=new VoterAuth;
        if($VoterAuth->rules){
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return false;
            }
        }

        $user_id = (\Auth::user()) ? \Auth::user()->id : null ;

        $voter_auth = VoterAuth::where('voter_id', $request->voter_id)->first();
        $VoterAuth = VoterAuth::find($voter_auth->id);
        $VoterAuth->is_activated = $request->action_status;
        $VoterAuth->save();

        $voters_list = Electorate::where('voter_id', $request->voter_id)->where('active_status', 1)->orderBy('created_at', 'DESC')->with('voter')->select('voter_id', 'id', 'name', 'mobile_number', 'index_number', 'email', 'level', 'department')->first();

        $last_audit = $VoterAuth->audits()->latest()->first();
        activity()->performedOn($VoterAuth)->withProperties(['audit_id' => $last_audit->id])->log("$voters_list->name record updated");

        if ($voter_auth) {
            $resp_code = "000";
            $resp_desc = ($request->action_status == true) ? "Account has been activated successfully" : "Account has been deactivated successfully" ;
        }else{
            $resp_code = "999";
            $resp_desc = ($request->action_status == true) ? "Account activation could not be completed." : "Account deactivation could not be completed" ;
        }

        return response()->json(['resp_code'=>$resp_code, 'resp_desc' => $resp_desc]);
    }



    public function new_id($id){
        return ''.str_pad($id + 1, 8, "0", STR_PAD_LEFT);
        // return IdGenerator::generate(['table' => 'entity_masters', 'length' => 8, 'prefix' => 1, 'reset_on_prefix_change' => true]);
    }

}
