<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\EntityDivision;
use App\VoterAuth;
use Validator;
use Carbon\Carbon;
use OwenIt\Auditing\Facades\Auditor;
use \OwenIt\Auditing\Models\Audit;
use \Spatie\Activitylog\Models\Activity;
use DB;
use PDF;
use DateTime;


class ResultsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {      
        (!\Auth::user()->can('view_results')) ? abort(403, 'Unauthorized action.') : null;

        $events=EntityDivision::where('in_use', true)->orderBy('created_at','DESC')->get();
        return view('stats.results', ['events' => $events]);
    }


    public function generate_result($entity_div_code){

        $event=EntityDivision::join('entity_masters', 'entity_masters.entity_id', '=', 'entity_divisions.entity_id')
                ->where('entity_divisions.assigned_code', $entity_div_code)
                ->select(['entity_divisions.*', 'entity_masters.entity_name'])->first();

        $event_name=$event->service_label;
        $event_start_date=$event->start_date;
        $event_end_date=$event->end_date;
        $entity_name=$event->entity_name;

        $positions = DB::table('voting_categories')->select(DB::raw('DISTINCT(cat_name)'))->where('entity_div_code',$entity_div_code)->get();

        $results = \DB::table('voting_nominees')
            ->leftJoin('voting_summary', 'voting_nominees.nom_id', '=', 'voting_summary.nom_id')
            ->leftJoin('voting_categories', 'voting_nominees.cat_id', '=', 'voting_categories.cat_id')
            ->leftJoin('entity_divisions', 'voting_categories.entity_div_code', '=', 'entity_divisions.assigned_code')
            ->select('entity_divisions.service_label','entity_divisions.assigned_code as entity_div_code',
            'voting_categories.cat_name', 'voting_categories.cat_id', 'voting_nominees.nom_name', 'voting_nominees.nom_code', 'voting_nominees.nom_id', 'voting_summary.vote_count')
            ->where('entity_divisions.assigned_code',$entity_div_code)
            ->where('entity_divisions.active_status',1)
            ->where('entity_divisions.del_status',0)
            ->where('voting_nominees.active_status',1)
            ->where('voting_nominees.del_status',0)
            ->where('voting_categories.active_status',1)
            ->where('voting_categories.del_status',0)
            ->orderBy('voting_categories.cat_name','DESC')
            ->orderBy('voting_summary.vote_count','DESC');

        $total_votes = 0;

        $result_view = '';
        $result_view .= '<table class="table table-bordered" style="font-size: 0.725rem; margin-left:auto;margin-right:auto;">';
        $result_view .=  '<tbody>';

        if ($positions){

            foreach ($positions as  $key=>$positions) {
                $result_view .=  '<tr><td colspan="3" style="text-align:center;">CATEGORY : <strong>'.strtoupper($positions->cat_name).'</strong></td></tr>';
                $result_view .=  '<tr>';
                $result_view .=  '<td>#</td>';
                $result_view .=  '<td style="font-weight: bold;">CANDIDATE</td>';
                $result_view .=  '<td style="font-weight: bold;">VOTES</td>';
                $result_view .=  '</tr>';

                $cat_name =  $positions->cat_name;

                $results1=clone $results;

                $filter_result=$results1->where('cat_name',$cat_name)->get();
                
                $total_vote_per_category = 0;
                $sn=0;
                foreach ($filter_result as $filter_result) {
                    $sn+=1;
                    if ($filter_result->vote_count) {
                        $vote_count = intval($filter_result->vote_count);
                    }else{
                        $vote_count = 0;
                    }

                    $result_view .=  '<tr><td>'.$sn.'</td><td><strong>'.strtoupper($filter_result->nom_name).'</strong></td><td><strong>'.htmlentities($vote_count).'</strong></td></tr>';

                    $total_vote_per_category = $total_vote_per_category + $vote_count;
                    $total_votes+=$total_vote_per_category;

                }

                $result_view .= '<tr><td colspan="2"></td><td><strong>TOTAL =  '.intval($total_vote_per_category).'</strong></td></tr>';
                $result_view .= '<tr><td colspan="3"></td></tr>';

            }
        }

        $result_view .=  '</tbody>';
        $result_view .=  '</table>';


        return [$result_view, $event_name, $entity_name, $total_votes, $event_start_date, $event_end_date];

    }
    
    
    public function get_result(Request $request)
    {     
        (!\Auth::user()->can('view_results')) ? abort(403, 'Unauthorized action.') : null;

        $rules = array(
            "entity_div_code" => 'required|string|exists:entity_divisions,assigned_code',
        );

        if($rules){
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return false;
            }
        }

        $entity_div_code=$request->entity_div_code;
        $res=$this->generate_result($entity_div_code);
        $result_view=$res[0];
        $event_name=$res[1];

        return response()->json(['result_view'=>$result_view, 'event_name'=>$event_name, 'entity_div_code'=>$entity_div_code]);        
    }    
    

    public function download_result(Request $request)
    {      
        // (!\Auth::user()->can('download_results')) ? abort(403, 'Unauthorized action.') : null;

        $rules = array(
            "entity_div_code" => 'required|string|exists:entity_divisions,assigned_code',
        );

        if($rules){
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return false;
            }
        }

        $entity_div_code=$request->entity_div_code;

        $res=$this->generate_result($entity_div_code);
        $result_view=$res[0];
        $event_name=$res[1];
        $entity_name=$res[2];
        $total_votes=$res[3];
        $event_start_date1=new DateTime($res[4]);
        $event_end_date1=new DateTime($res[5]);

        $event_date=$event_start_date1->format('l jS \of F Y');
        $event_start_time=$event_start_date1->format('g:ia');
        $event_end_time=$event_end_date1->format('g:ia');

        $electorates=VoterAuth::where('voting_electorates.entity_div_code', $entity_div_code)->where('voter_auth.active_status',1)
            ->leftJoin('voting_electorates', 'voter_auth.voter_id', '=', 'voting_electorates.voter_id');

        $total_electorates_count=$electorates->count();
        $electorates1 = clone $electorates; 
        $electorates2 = clone $electorates; 
        $electorates3 = clone $electorates; 
        $electorates4= clone $electorates; 

        $verified_electorates_count=$electorates1->where('is_activated', 1)->count();
        $notverified_electorates_count=$electorates2->where('is_activated', 0)->count();
        $voted_electorates_count=$electorates3->where('is_voted', 1)->count();
        $notvoted_electorates_count=$electorates4->where('is_voted', 0)->count();

        \Log::info("downloading the result");

        $data = ['content' => $result_view, 'entity_name' => $entity_name, 'event_name' => $event_name, 'voted_electorates_count' => $voted_electorates_count, 'total_electorates_count' => $total_electorates_count, 'event_date' => $event_date, 'event_start_time' => $event_start_time, 'event_end_time' => $event_end_time];
        $pdf = PDF::loadView('stats.results_downloadable', $data);
        $pdf_file_name = $event_name."_result.pdf";
        // return $pdf->stream($pdf_file_name);
        return $pdf->download($pdf_file_name);
    }


    public function generate_result_data(){

        $event=EntityDivision::join('entity_masters', 'entity_masters.entity_id', '=', 'entity_divisions.entity_id')
                ->where('entity_divisions.in_use', true)
                ->select(['entity_divisions.*', 'entity_masters.entity_name'])
                ->first();

        $event_name=$event->service_label;
        $entity_name=$event->entity_name;
        $entity_div_code=$event->assigned_code;

        $positions = DB::table('voting_categories')->select(DB::raw('DISTINCT(cat_name)'))->where('entity_div_code',$entity_div_code);
        $positions1=clone $positions;
        $positions=$positions->get();

        $results = \DB::table('voting_nominees')
            ->leftJoin('voting_summary', 'voting_nominees.nom_id', '=', 'voting_summary.nom_id')
            ->leftJoin('voting_categories', 'voting_nominees.cat_id', '=', 'voting_categories.cat_id')
            ->leftJoin('entity_divisions', 'voting_categories.entity_div_code', '=', 'entity_divisions.assigned_code')
            ->select('entity_divisions.service_label','entity_divisions.assigned_code as entity_div_code',
            'voting_categories.cat_name', 'voting_categories.cat_id', 'voting_nominees.nom_name', 'voting_nominees.nom_code', 'voting_nominees.nom_id', 'voting_summary.vote_count')
            ->where('entity_divisions.assigned_code',$entity_div_code)
            ->where('entity_divisions.active_status',1)
            ->where('entity_divisions.del_status',0)
            ->where('voting_nominees.active_status',1)
            ->where('voting_nominees.del_status',0)
            ->where('voting_categories.active_status',1)
            ->where('voting_categories.del_status',0)
            ->orderBy('voting_categories.cat_name','DESC')
            ->orderBy('voting_summary.vote_count','DESC');

        $final_results=[];

        if ($positions){

            foreach ($positions as  $key=>$positions) {

                $cat_name =  $positions->cat_name;
                $results1=clone $results;
                $filter_result=$results1->where('cat_name',$cat_name)->get();
                $final_results += [$cat_name => $filter_result];
            }
        }

        $category_count=$positions1->count();

        return [$category_count, $final_results];

                
    }


    public function generate_category_result_data($cat_id){

        $event=EntityDivision::join('entity_masters', 'entity_masters.entity_id', '=', 'entity_divisions.entity_id')
                ->where('entity_divisions.in_use', true)
                ->select(['entity_divisions.*', 'entity_masters.entity_name'])
                ->first();

        $event_name=$event->service_label;
        $entity_name=$event->entity_name;
        $entity_div_code=$event->assigned_code;

        $category = DB::table('voting_categories')->select('cat_name')->where('cat_id',$cat_id)->first();

        $results = \DB::table('voting_nominees')
            ->leftJoin('voting_summary', 'voting_nominees.nom_id', '=', 'voting_summary.nom_id')
            ->leftJoin('voting_categories', 'voting_nominees.cat_id', '=', 'voting_categories.cat_id')
            ->leftJoin('entity_divisions', 'voting_categories.entity_div_code', '=', 'entity_divisions.assigned_code')
            ->select('entity_divisions.service_label','entity_divisions.assigned_code as entity_div_code',
            'voting_categories.cat_name', 'voting_categories.cat_id', 'voting_nominees.nom_name', 'voting_nominees.nom_code', 'voting_nominees.nom_id', 'voting_summary.vote_count')
            ->where('voting_nominees.cat_id',$cat_id)
            ->where('entity_divisions.active_status',1)
            ->where('entity_divisions.del_status',0)
            ->where('voting_nominees.active_status',1)
            ->where('voting_nominees.del_status',0)
            ->where('voting_categories.active_status',1)
            ->where('voting_categories.del_status',0)
            ->orderBy('voting_categories.cat_name','DESC')
            ->orderBy('voting_summary.vote_count','DESC');

        $final_results=[];
        $cat_name = null;

        if ($category){
            $cat_name =  $category->cat_name;
            $results1=clone $results;
            $filter_result=$results1->get();
            $final_results=$filter_result;
        }

        return [$cat_name, $final_results];

                
    }

    public function results_graph_index()
    {      
        (!\Auth::user()->can('strongroom_graph_view')) ? abort(403, 'Unauthorized action.') : null;

        $vals=$this->generate_result_data();
        $category_count=$vals[0];
        $final_results=$vals[1];

        $electorates=VoterAuth::where('entity_divisions.in_use', true)->where('voter_auth.active_status',1)
            ->leftJoin('voting_electorates', 'voter_auth.voter_id', '=', 'voting_electorates.voter_id')
            ->leftJoin('entity_divisions', 'voting_electorates.entity_div_code', '=', 'entity_divisions.assigned_code');

        $total_electorates_count=$electorates->count();
        $electorates1 = clone $electorates; 
        $electorates2 = clone $electorates; 
        $electorates3 = clone $electorates; 
        $electorates4= clone $electorates; 

        $verified_electorates_count=$electorates1->where('is_activated', 1)->count();
        $notverified_electorates_count=$electorates2->where('is_activated', 0)->count();
        $voted_electorates_count=$electorates3->where('is_voted', 1)->count();
        $notvoted_electorates_count=$electorates4->where('is_voted', 0)->count();
        
        // return view('stats.results_graph', ['category_count' => $category_count, 'final_results' => $final_results]);
        return view('stats.results_graph', ['category_count' => $category_count, 'final_results' => $final_results, 'total_electorates_count' => $total_electorates_count, 'verified_electorates_count' => $verified_electorates_count, 'notverified_electorates_count' => $notverified_electorates_count, 'voted_electorates_count' => $voted_electorates_count, 'notvoted_electorates_count' => $notvoted_electorates_count]);

    }


    public function results_graph_get_categories()
    {      
        (!\Auth::user()->can('strongroom_graph_view')) ? abort(403, 'Unauthorized action.') : null;

        $event=EntityDivision::join('entity_masters', 'entity_masters.entity_id', '=', 'entity_divisions.entity_id')
                ->where('entity_divisions.in_use', true)
                ->select(['entity_divisions.*', 'entity_masters.entity_name'])
                ->first();

        $event_name=$event->service_label;
        $entity_name=$event->entity_name;
        $entity_div_code=$event->assigned_code;

        $category_val = DB::table('voting_categories')->select(DB::raw('DISTINCT(cat_id), cat_name'))->where('entity_div_code',$entity_div_code);
        $category_val=$category_val->get();


        $vals=$this->generate_result_data();
        $category_count=$vals[0];
        $final_results=$vals[1];

        $electorates=VoterAuth::where('entity_divisions.in_use', true)->where('voter_auth.active_status',1)
        ->leftJoin('voting_electorates', 'voter_auth.voter_id', '=', 'voting_electorates.voter_id')
        ->leftJoin('entity_divisions', 'voting_electorates.entity_div_code', '=', 'entity_divisions.assigned_code');

        $total_electorates_count=$electorates->count();
        $electorates1 = clone $electorates; 
        $electorates2 = clone $electorates; 
        $electorates3 = clone $electorates; 
        $electorates4= clone $electorates; 

        $verified_electorates_count=$electorates1->where('is_activated', 1)->count();
        $notverified_electorates_count=$electorates2->where('is_activated', 0)->count();
        $voted_electorates_count=$electorates3->where('is_voted', 1)->count();
        $notvoted_electorates_count=$electorates4->where('is_voted', 0)->count();

        return response()->json(['category_val'=>$category_val, 'category_count' => $category_count, 'final_results' => $final_results, 'total_electorates_count' => $total_electorates_count, 'verified_electorates_count' => $verified_electorates_count, 'notverified_electorates_count' => $notverified_electorates_count, 'voted_electorates_count' => $voted_electorates_count, 'notvoted_electorates_count' => $notvoted_electorates_count]);
    }
    
    
    public function results_graph_get_data()
    {      
        (!\Auth::user()->can('strongroom_graph_view')) ? abort(403, 'Unauthorized action.') : null;

        $event=EntityDivision::join('entity_masters', 'entity_masters.entity_id', '=', 'entity_divisions.entity_id')
                ->where('entity_divisions.in_use', true)
                ->select(['entity_divisions.*', 'entity_masters.entity_name'])
                ->first();

        $event_name=$event->service_label;
        $entity_name=$event->entity_name;
        $entity_div_code=$event->assigned_code;

        $category_val = DB::table('voting_categories')->select(DB::raw('DISTINCT(cat_id)'))->where('entity_div_code',$entity_div_code);
        // $positions1=clone $positions;
        $category_val=$category_val->get();


        $vals=$this->generate_result_data();
        $category_count=$vals[0];
        $final_results=$vals[1];


        $electorates=VoterAuth::where('entity_divisions.in_use', true)->where('voter_auth.active_status',1)
        ->leftJoin('voting_electorates', 'voter_auth.voter_id', '=', 'voting_electorates.voter_id')
        ->leftJoin('entity_divisions', 'voting_electorates.entity_div_code', '=', 'entity_divisions.assigned_code');

        $total_electorates_count=$electorates->count();
        $electorates1 = clone $electorates; 
        $electorates2 = clone $electorates; 
        $electorates3 = clone $electorates; 
        $electorates4= clone $electorates; 

        $verified_electorates_count=$electorates1->where('is_activated', 1)->count();
        $notverified_electorates_count=$electorates2->where('is_activated', 0)->count();
        $voted_electorates_count=$electorates3->where('is_voted', 1)->count();
        $notvoted_electorates_count=$electorates4->where('is_voted', 0)->count();

        return response()->json(['category_val'=>$category_val, 'category_count' => $category_count, 'final_results' => $final_results, 'total_electorates_count' => $total_electorates_count, 'verified_electorates_count' => $verified_electorates_count, 'notverified_electorates_count' => $notverified_electorates_count, 'voted_electorates_count' => $voted_electorates_count, 'notvoted_electorates_count' => $notvoted_electorates_count]);
    }


    public function results_graph_category_results(Request $request)
    {      
        // (!\Auth::user()->can('strongroom_graph_view')) ? abort(403, 'Unauthorized action.') : null;
        $cat_id=$request->cat_id;

        $res=$this->generate_category_result_data($cat_id);
        $cat_name=$res[0];
        $final_results=$res[1];
        
        return response()->json(['resp_code' => '000', 'cat_name'=>$cat_name, 'final_results' => $final_results]);
    }
}
