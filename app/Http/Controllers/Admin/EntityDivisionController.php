<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\EntityMaster;
use App\EntityExtra;
use App\EntityDivision;
use App\EntityServiceConfig;
use Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class EntityDivisionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($entity_id)
    {
        (!\Auth::user()->can('view_events')) ? abort(403, 'Unauthorized action.') : null;

        $entity_details=EntityMaster::where('entity_id',$entity_id)->where('active_status',1)->first();
        $entity_name = $entity_details->entity_name;
        
        $events = EntityDivision::where('entity_id', $entity_id)->where('active_status', 1)->paginate(10);

        return view('setups.events', ['data' => $events, 'entity_name' => $entity_name, 'entity_id' => $entity_id]);
    }


    /**
     * Show the application new form.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function new($entity_id)
    {
        (!\Auth::user()->can('add_events')) ? abort(403, 'Unauthorized action.') : null;

        $entity_details=EntityMaster::where('entity_id',$entity_id)->where('active_status',1)->first();
        $entity_name = $entity_details->entity_name;

        return View('setups.events_form', ['entity_name' => $entity_name, 'entity_id' => $entity_id]);
    } 
    
    
    /**
     * Save inputs.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function saveEvent(Request $request)
    {
        (!\Auth::user()->can('add_events')) ? abort(403, 'Unauthorized action.') : null;

        $rules = array(
            'entity_id'=>['required','min:1','max:30','string'],
            'div_name'=>['required','min:1','max:150','string'],
            'service_label'=>['required','min:1','max:150','string'],
            'cat_type'=>['required','min:1','max:100','string'],
            'start_date'=>['required','string'],
            "end_date" => ['required',"string"],
            'sms_sender_id'=>['required','string','max:11'],
            'show_client_result'=>['required','string','max:5'],
            'auth_identify'=>['required','string','max:5'],
            'auth_class'=>['required','string','max:5'],
            'event_logo' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            // 'image_path'=>['min:1','max:100','string'],
            // 'image_data'=>['min:1','max:100','string'],
            // 'image_type'=>['min:1','max:100','string'],
        );

    
        $EntityDivision=new EntityDivision;
        $EntityServiceConfig=new EntityServiceConfig;
        if($EntityDivision->rules){
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
               return false;
            }
        }


        // return DB::transaction(function() use ($request) {

            $start_at = preg_split("#-#", $request->start_date);
            $startdayformat = date('Y-m-d',strtotime($start_at[0]));
            $starttimeformat = date('H:i:s',strtotime($start_at[1]));

            $end_at = preg_split("#-#", $request->end_date); 
            $enddayformat = date('Y-m-d',strtotime($end_at[0]));
            $endtimeformat = date('H:i:s',strtotime($end_at[1]));

            $final_start_date = date ('Y-m-d H:i:s', strtotime($startdayformat.' '.$starttimeformat));
            $final_end_date = date ('Y-m-d H:i:s', strtotime($enddayformat.' '.$endtimeformat));
            

            $user_id = (\Auth::user()) ? \Auth::user()->id : null ;
            $show_client_result = null;

            $entity_div = EntityDivision::orderBy('created_at','DESC')->first();
            $entity_div_id = ($entity_div) ? $entity_div->id : 0;
            
            $entity_div_code = $this->new_id($entity_div_id);

            $absolute_path=config('app.logo_media_path');
            $db_absolute_path='/media_assets/logos';

            $imageName = $entity_div_code.'_'.time().'.'.$request->event_logo->extension();
            $moved=$request->event_logo->move($absolute_path, $imageName);
            $pic_name=$imageName;
            $pic_path=$db_absolute_path.'/'.$pic_name;
            $event_logo_full_path=$absolute_path.'/'.$pic_name;

            copy($event_logo_full_path, public_path('/assets/votella/').$pic_name);

            $EntityDivision->assigned_code = $entity_div_code;
            $EntityDivision->entity_id = $request->entity_id;
            $EntityDivision->div_name = $request->div_name;
            $EntityDivision->service_label = $request->service_label;
            $EntityDivision->activity_type_code = $request->cat_type;
            $EntityDivision->start_date = $final_start_date;
            $EntityDivision->end_date = $final_end_date;
            $EntityDivision->image_path = $pic_path;
            $EntityDivision->image_data = $pic_name;
            $EntityDivision->user_id = $user_id;
            $EntityDivision->save();

            switch ($request->show_client_result) {
                case 'true':
                    $show_client_result = 1;
                    break;
                case 'false':
                    $show_client_result = 0;
                    break;
                default:
                    $show_client_result = 1;
                    break;
            }


            // Get last Audit
            $last_audit = $EntityDivision->audits()->latest()->first();
            activity()->performedOn($EntityDivision)->withProperties(['audit_id' => $last_audit->id])->log("$EntityDivision->div_name record created");
        

            $EntityServiceConfig->entity_div_code = $entity_div_code;
            $EntityServiceConfig->sms_sender_id = $request->sms_sender_id;
            $EntityServiceConfig->show_client_result = $show_client_result;
            $EntityServiceConfig->auth_identify = $request->auth_identify;
            $EntityServiceConfig->auth_class = $request->auth_class;
            // $EntityExtra->image_path = $request->cat_type;
            // $EntityExtra->image_data = $request->cat_type;
            // $EntityExtra->image_type = $request->cat_type;
            $EntityServiceConfig->user_id = $user_id;
            $EntityServiceConfig->save();

            // Get last Audit
            $last_audit = $EntityServiceConfig->audits()->latest()->first();
            activity()->performedOn($EntityServiceConfig)->withProperties(['audit_id' => $last_audit->id])->log("$EntityServiceConfig->entity_div_code record created");
        

            return response()->json(['resp_code'=>'000', 'resp_desc' => 'Record has been saved successfully']);

        // });
        
    }



    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function active_event_index()
    {
        (!\Auth::user()->can('view_events')) ? abort(403, 'Unauthorized action.') : null;

        $event_list=EntityDivision::join('entity_masters', 'entity_masters.entity_id', '=', 'entity_divisions.entity_id')
                ->where('entity_divisions.active_status', true)->orderBy('entity_divisions.created_at','DESC')
                ->get(['entity_divisions.*', 'entity_masters.entity_name']);

        $events = EntityDivision::where('in_use', true)->where('active_status', 1)->paginate(10);

        return view('setups.active_events', ['data' => $events, 'event_list' =>$event_list]);
    }


    public function setEventInUse(Request $request) {
        (!\Auth::user()->can('add_events')) ? abort(403, 'Unauthorized action.') : null;

        $rules = array(
            'entity_div_code'=>['required','min:1','max:30','string']
        );

        if($rules){
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
               return false;
            }
        }

        // return DB::transaction(function() use ($request) {
            $user_id = (\Auth::user()) ? \Auth::user()->id : null ;

            $entity_div_code = $request->entity_div_code;

            $entity_div = EntityDivision::where('assigned_code', $entity_div_code)->orderBy('created_at','DESC')->first();
            
            EntityDivision::where('in_use', true)->update(['in_use' => false]);
            EntityDivision::where('assigned_code', $entity_div_code)->update(['in_use' => true]);

            return response()->json(['resp_code'=>'000', 'resp_desc' => 'Event has been made active.']);
        // });
   
    }



    // public function event_edit($entity_div_code)
    // {   
    //     (!\Auth::user()->can('edit_event')) ? abort(403, 'Unauthorized action.') : null;

    //     $details = \DB::table('event_full_view')
    //         ->select('*')
    //         ->where('entity_div_code',$entity_div_code)
    //         ->orderBy('created_at','DESC')
    //         ->first();

    //     return view('setups.event_edit_form', ['data' => $details]);
    // }


    // public function nominee_view($nom_id)
    // {   
    //     (!\Auth::user()->can('view_event_details')) ? abort(403, 'Unauthorized action.') : null;

    //     $nom_details = \DB::table('voting_nominees')
    //         ->leftJoin('voting_categories', 'voting_nominees.cat_id', '=', 'voting_categories.cat_id')
    //         ->select('voting_nominees.id', 'voting_nominees.nom_id', 'voting_nominees.nom_name', 'voting_nominees.image_path as nom_pic', 'voting_nominees.active_status', 'voting_nominees.created_at', 'voting_categories.cat_name')
    //         ->where('voting_nominees.nom_id',$nom_id)
    //         ->orderBy('voting_nominees.created_at','DESC')
    //         ->first();

    //     $Nominee=new Nominee;
    //     $Nominee->logActivity("$nom_details->cat_name record viewed");

    //     return view('setups.nominees_view_form', ['data' => $nom_details, 'nom_id' => $nom_id]);
    // }


    public function new_id($id){
        return ''.str_pad($id + 1, 8, "0", STR_PAD_LEFT);
        // return IdGenerator::generate(['table' => 'entity_masters', 'length' => 8, 'prefix' => 1, 'reset_on_prefix_change' => true]);
    }

}
