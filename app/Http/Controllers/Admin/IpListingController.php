<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\EntityDivision;
use App\EntityServiceConfig;
use App\Whitelist;
use Validator;
use Carbon\Carbon;


class IpListingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function whitelisting_index()
    {   
        (!\Auth::user()->can('view_whitelisted_IPs')) ? abort(403, 'Unauthorized action.') : null;

        $data = Whitelist::where('activity_type','WHT')->where('active_status', 1)->orderBy('created_at','DESC')->paginate(10);
        return view('security.whitelist', ['data' => $data]);
    }


    /**
     * Show the application new form.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function newWhitelisting()
    {
        (!\Auth::user()->can('add_whitelsted_IPs')) ? abort(403, 'Unauthorized action.') : null;

        $events = EntityDivision::where('active_status', 1)->orderBy('created_at','DESC')->get();
        return View('security.whitelist_form', ['events'=>$events]);
    } 
    
    
    /**
     * Save inputs.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function saveWhitelisting(Request $request)
    {

        (!\Auth::user()->can('add_whitelsted_IPs')) ? abort(403, 'Unauthorized action.') : null;

        $trans_type="WHT";

        $searchForValue = ',';
        if( strpos($request->remote_ip, $searchForValue) !== false ) {
            $item_arr = explode (",", $request->remote_ip);

            foreach ($item_arr as $remote_ip){
                $remote_ip=trim($remote_ip);

                $Whitelist=new Whitelist;
                if($Whitelist->rules){
                    $validator = Validator::make($request->all(), $Whitelist->rules);
                    if ($validator->fails()) {
                        return false;
                    }
                }
       
                $this->saveIplistData($Whitelist, $trans_type, $request->entity_div_code, $remote_ip, $request->phone_number, $request->desc, null, null );
            
            }

        }else{

            $Whitelist=new Whitelist;
            if($Whitelist->rules){
                $validator = Validator::make($request->all(), $Whitelist->rules);
                if ($validator->fails()) {
                    return false;
                }
            }

            $this->saveIplistData($Whitelist, $trans_type, $request->entity_div_code, $request->remote_ip, $request->phone_number, $request->desc, null, null );
            
        }

        return response()->json(['resp_code'=>'000', 'resp_desc' => 'Record has been saved successfully']);
    }



     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function blacklisting_index()
    {   
        (!\Auth::user()->can('view_blacklisted_Ips')) ? abort(403, 'Unauthorized action.') : null;

        $data = Whitelist::where('activity_type','BLK')->where('active_status', 1)->orderBy('created_at','DESC')->paginate(10);
        return view('security.blacklist', ['data' => $data]);
    }


    /**
     * Show the application new form.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function newBlacklisting()
    {
        (!\Auth::user()->can('add_blacklisted_IPs')) ? abort(403, 'Unauthorized action.') : null;

        $events = EntityDivision::where('active_status', 1)->orderBy('created_at','DESC')->get();
        return View('security.blacklist_form', ['events'=>$events]);
    } 
    
    
    /**
     * Save inputs.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function saveBlacklisting(Request $request)
    {
        (!\Auth::user()->can('add_blacklisted_IPs')) ? abort(403, 'Unauthorized action.') : null;

        $trans_type="BLK";

        $searchForValue = ',';
        if( strpos($request->remote_ip, $searchForValue) !== false ) {
            $item_arr = explode (",", $request->remote_ip);

            foreach ($item_arr as $remote_ip){
                $remote_ip=trim($remote_ip);

                $Whitelist=new Whitelist;
                if($Whitelist->rules){
                    $validator = Validator::make($request->all(), $Whitelist->rules);
                    if ($validator->fails()) {
                        return false;
                    }
                }
       
                $this->saveIplistData($Whitelist, $trans_type, $request->entity_div_code, $remote_ip, $request->phone_number, $request->desc, null, null );
            
            }

        }else{

            $Whitelist=new Whitelist;
            if($Whitelist->rules){
                $validator = Validator::make($request->all(), $Whitelist->rules);
                if ($validator->fails()) {
                    return false;
                }
            }

            $this->saveIplistData($Whitelist, $trans_type, $request->entity_div_code, $request->remote_ip, $request->phone_number, $request->desc, null, null );
            
        }

        return response()->json(['resp_code'=>'000', 'resp_desc' => 'Record has been saved successfully']);
    }


    public function saveIplistData($Whitelist, $trans_type, $entity_div_code, $remote_ip, $phone_number, $desc, $start_date, $end_date ) {

        $user_id = \Auth::user() ? \Auth::user()->id : null;

        $Whitelist->entity_div_code = $entity_div_code;
        $Whitelist->remote_ip = $remote_ip;
        $Whitelist->phone_number = $phone_number;
        $Whitelist->desc = $desc;
        $Whitelist->activity_type = $trans_type;
        $Whitelist->start_date = $start_date ? $start_date : null;
        $Whitelist->end_date = $end_date ? $end_date : null;
        $Whitelist->user_id = $user_id;
        $Whitelist->save();

        $last_audit = $Whitelist->audits()->latest()->first();
        activity()->performedOn($Whitelist)->withProperties(['audit_id' => $last_audit->id])->log("$Whitelist->entity_div_code record created");

        return $Whitelist;
    }

}
