<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class EntityExtra extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //only the `deleted` event will get logged automatically
    protected static $recordEvents = ['retrieved','created','updated','deleted'];

    public function entitymaster()
    {
        return $this->belongsTo('App\EntityMaster', 'entity_id');
    }
    
    public $fillable = [
        'entity_id',
        'phone_number',
        'email',
        'other_details',
        'image_path',
        'image_data',
        'image_type',
        'comment',
        'user_id',
        'act_status',
        'del_status',
        'created_at',
        'updated_at'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public   $rules = [
        'entity_id'=>['required','min:1','max:100','string'],
        "phone_number" => ['required','min:9','max:15',"regex:/^(\+\d{12,15}|\d{10}|\+\d{2}-\d{3}-\d{7})$/"],
        'email'=>['email','max:200'],
        'other_details'=>['min:1','max:1000','string'],
        'image_path'=>['min:1','max:100','string'],
        'image_data'=>['min:1','max:100','string'],
        'image_type'=>['min:1','max:100','string'],
        'comment'=>['min:1','max:255','string'],
        "user_id" => 'required|integer|exists:users,id',
        'act_status'=>['integer','max:2'],
        'del_status'=>['integer','max:2']
    ];

    public function logActivity($activity){
        activity()->performedOn($this)->log($activity);
    }
}
