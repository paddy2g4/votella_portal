<?php

namespace App\Exports;

use App\Electorate;
use Maatwebsite\Excel\Concerns\FromCollection;

class ElectoratesExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Electorate::all();
    }
}
