<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class VoterAuth extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //only the `deleted` event will get logged automatically
    protected static $recordEvents = ['retrieved','created','updated','deleted'];

    protected $table = 'voter_auth';

    public function electorate()
    {
        return $this->belongsTo('App\Electorate', 'voter_id', 'voter_id');
    }

    public $fillable = [
        'voter_id',
        'username',
        'password',
        'is_activated',
        'is_notify',
        'active_status',
        'del_status',
        'created_at',
        'updated_at'
    ];

 
    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'voter_id'=>['required','min:1','max:100','string'],
        "user_id" => 'required|integer|exists:users,id',
        'is_activated'=>['integer','max:2'],
        'act_status'=>['integer','max:2'],
        'del_status'=>['integer','max:2'],
    ];

    public function logActivity($activity){
        activity()->performedOn($this)->log($activity);
    }

}
