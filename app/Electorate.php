<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Electorate extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //only the `deleted` event will get logged automatically
    protected static $recordEvents = ['retrieved','created','updated','deleted'];

    protected $table = 'voting_electorates';

    public function entitydivision()
    {
        return $this->belongsTo('App\EntityDivision', 'entity_div_code', 'assigned_code');
    }

    public function voter()
    {
        return $this->hasOne('App\VoterAuth', 'voter_id', 'voter_id');
    }

    public $fillable = [
        'entity_div_code',
        'voter_id',
        'index_number',
        'name',
        'mobile_number',
        'email',
        'level',
        'department',
        'other',
        'user_id',
        'active_status',
        'del_status',
        'created_at',
        'updated_at'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'entity_div_code'=>['required','min:1','max:100','string'],
        'voter_id'=>['required','min:1','max:100','string'],
        'index_number'=>['min:1','max:200',"string"],
        'name'=>['required','min:1','max:200',"string"],
        'mobile_number'=>['min:1','max:200',"string"],
        'email'=>['min:1','max:200',"string"],
        'level'=>['required','min:1','max:200',"string"],
        'department'=>['min:1','max:255',"string"],
        'other'=>['min:1','max:255',"string"],
        "user_id" => 'required|integer|exists:users,id',
        'act_status'=>['integer','max:2'],
        'del_status'=>['integer','max:2'],
    ];

    public function logActivity($activity){
        activity()->performedOn($this)->log($activity);
    }

}
