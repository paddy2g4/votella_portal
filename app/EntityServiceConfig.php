<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class EntityServiceConfig extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //only the `deleted` event will get logged automatically
    protected static $recordEvents = ['retrieved','created','updated','deleted'];

    public function entitydivision()
    {
        return $this->belongsTo('App\EntityDivision', 'assigned_code', 'entity_div_code');
    }

    public $fillable = [
        'entity_div_code',
        'sms_sender_id',
        'show_client_result',
        'auth_identify',
        'auth_class',
        'comment',
        'user_id',
        'active_status',
        'del_status',
        'created_at',
        'updated_at'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'entity_div_code'=>['required','min:1','max:100','string'],
        'sms_sender_id'=>['required','min:1','max:15',"string"],
        'show_client_result'=>['integer','max:2'],
        'auth_identify'=>['required','min:1','max:50',"string"],
        'auth_class'=>['required','min:1','max:50',"string"],
        'comment'=>['min:1','max:255','string'],
        "user_id" => 'required|integer|exists:users,id',
        'act_status'=>['integer','max:2'],
        'del_status'=>['integer','max:2'],
    ];

    public function logActivity($activity){
        activity()->performedOn($this)->log($activity);
    }
}
