<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::get('/home', 'App\Http\Controllers\Admin\StatisticsController@index')->name('home');
// Route::get('/home', 'HomeController@index')->name('home');


Route::get('/users', 'App\Http\Controllers\Admin\UserController@users_index')->name('users');
Route::get('/users/new', 'App\Http\Controllers\Admin\UserController@users_new')->name('users_new');
Route::post('/users/save', 'App\Http\Controllers\Admin\UserController@saveUser')->name('users_save');
Route::get('/users/view/{user_id}', 'App\Http\Controllers\Admin\UserController@user_view')->name('user_view');
Route::get('/users/{user_id}', 'App\Http\Controllers\Admin\UserController@user_edit')->name('user_edit');
Route::post('/users/update', 'App\Http\Controllers\Admin\UserController@updateUser')->name('user_update');
Route::post('/users/actions', 'App\Http\Controllers\Admin\UserController@updateUserAction')->name('user_action_update');


Route::get('/permissions', 'App\Http\Controllers\Admin\PermissionController@permissions_index')->name('permissions');
Route::get('/permissions/new', 'App\Http\Controllers\Admin\PermissionController@permissions_new')->name('permissions_new');
Route::post('/permissions/save', 'App\Http\Controllers\Admin\PermissionController@savePermission')->name('permission_save');
Route::get('/permissions/view/{permission}', 'App\Http\Controllers\Admin\PermissionController@permissions_view')->name('permission_view');
Route::get('/permissions/{permission}', 'App\Http\Controllers\Admin\PermissionController@permissions_edit')->name('permission_edit');
Route::post('/permissions/update', 'App\Http\Controllers\Admin\PermissionController@updatePermission')->name('permission_update');
Route::post('/permissions/actions', 'App\Http\Controllers\Admin\PermissionController@updatePermissionAction')->name('permission_action_update');


Route::get('/roles', 'App\Http\Controllers\Admin\PermissionController@roles_index')->name('roles');
Route::get('/roles/new', 'App\Http\Controllers\Admin\PermissionController@roles_new')->name('roles_new');
Route::post('/roles/save', 'App\Http\Controllers\Admin\PermissionController@saveRole')->name('roles_save');
Route::get('/roles/view/{role_id}', 'App\Http\Controllers\Admin\PermissionController@role_view')->name('role_view');
Route::get('/roles/{role_id}', 'App\Http\Controllers\Admin\PermissionController@role_edit')->name('role_edit');
Route::post('/roles/update', 'App\Http\Controllers\Admin\PermissionController@updateRole')->name('role_update');
Route::post('/roles/actions', 'App\Http\Controllers\Admin\PermissionController@updateRoleAction')->name('role_action_update');

Route::get('/logout', 'App\Http\Controllers\Auth\LoginController@logout');


Route::get('/institutions', 'App\Http\Controllers\Admin\InstitutionController@index')->name('institutions');
Route::get('/institutions/new', 'App\Http\Controllers\Admin\InstitutionController@new')->name('institutions_new');
Route::post('/institutions/save', 'App\Http\Controllers\Admin\InstitutionController@saveInstitution')->name('institutions_save');
Route::get('/institutions/view/{entity_id}', 'App\Http\Controllers\Admin\InstitutionController@institution_view')->name('institution_view');
Route::post('/institutions/actions', 'App\Http\Controllers\Admin\InstitutionController@updateInstitutionAction')->name('institution_action_update');


Route::get('/events/{entity_id}', 'App\Http\Controllers\Admin\EntityDivisionController@index')->name('events');
Route::get('/events/{entity_id}/new', 'App\Http\Controllers\Admin\EntityDivisionController@new')->name('events_new');
Route::post('/events/save', 'App\Http\Controllers\Admin\EntityDivisionController@saveEvent')->name('events_save');

Route::get('/active_events', 'App\Http\Controllers\Admin\EntityDivisionController@active_event_index')->name('active_event');
Route::post('/events/set_in_use', 'App\Http\Controllers\Admin\EntityDivisionController@setEventInUse')->name('events_set_in_use');



Route::get('/categories/{entity_id}', 'App\Http\Controllers\Admin\CategoryController@index')->name('categories');
Route::get('/categories/{entity_div_code}/new', 'App\Http\Controllers\Admin\CategoryController@new')->name('category_new');
Route::post('/categories/save', 'App\Http\Controllers\Admin\CategoryController@saveCategory')->name('category_save');
Route::get('/categories/view/{cat_id}', 'App\Http\Controllers\Admin\CategoryController@category_view')->name('category_view');
Route::get('/categories/edit/{nom_id}', 'App\Http\Controllers\Admin\CategoryController@category_edit')->name('category_edit');
Route::post('/categories/update', 'App\Http\Controllers\Admin\CategoryController@updateCategory')->name('category_update');
Route::post('/categories/actions', 'App\Http\Controllers\Admin\CategoryController@updateCategoryAction')->name('category_action_update');


Route::get('/voter_verification', 'App\Http\Controllers\Admin\VoterVerificationController@index')->name('voter_verification');
Route::post('/voter_verification/queryVoter', 'App\Http\Controllers\Admin\VoterVerificationController@find_voter')->name('voter_verification_search');
Route::post('/voter_verification/account_action', 'App\Http\Controllers\Admin\VoterVerificationController@account_action')->name('voter_verification_account_action');

Route::get('/nominees/{cat_id}', 'App\Http\Controllers\Admin\NomineeController@index')->name('nominees');
Route::get('/nominees/{cat_id}/new', 'App\Http\Controllers\Admin\NomineeController@new')->name('nominee_new');
Route::post('/nominees/save', 'App\Http\Controllers\Admin\NomineeController@saveNominee')->name('nominee_save');
Route::get('/nominees/view/{user_id}', 'App\Http\Controllers\Admin\NomineeController@nominee_view')->name('nominee_view');
Route::get('/nominees/edit/{nom_id}', 'App\Http\Controllers\Admin\NomineeController@nominee_edit')->name('nominee_edit');
Route::post('/nominees/update', 'App\Http\Controllers\Admin\NomineeController@updateNominee')->name('nominee_update');
Route::post('/nominees/actions', 'App\Http\Controllers\Admin\NomineeController@updateNomineeAction')->name('nominee_action_update');


Route::get('/electorates/{entity_div_code}', 'App\Http\Controllers\Admin\ElectorateController@index')->name('electorates');
Route::get('/electorates/{entity_div_code}/new', 'App\Http\Controllers\Admin\ElectorateController@new')->name('electorates_new');
Route::post('/electorates/save', 'App\Http\Controllers\Admin\ElectorateController@saveElectorate')->name('electorate_save');
Route::get('/electorates/view/{voter_id}', 'App\Http\Controllers\Admin\ElectorateController@electorate_view')->name('electorate_view');
Route::get('/electorates/edit/{voter_id}', 'App\Http\Controllers\Admin\ElectorateController@electorate_edit')->name('electorate_edit');
Route::post('/electorates/import/{entity_div_code}', 'App\Http\Controllers\Admin\ElectorateController@import_electorate')->name('import_electorate');
Route::get('electorates/export/sample', 'App\Http\Controllers\Admin\ElectorateController@export_electorate_sample_upload')->name('export_electorate_sample_upload');

Route::get('/whitelisting', 'App\Http\Controllers\Admin\IpListingController@whitelisting_index')->name('whitelisting');
Route::get('/whitelisting/new', 'App\Http\Controllers\Admin\IpListingController@newWhitelisting')->name('whitelisting_new');
Route::post('/whitelisting/save', 'App\Http\Controllers\Admin\IpListingController@saveWhitelisting')->name('whitelisting_save');

Route::get('/blacklisting', 'App\Http\Controllers\Admin\IpListingController@blacklisting_index')->name('blacklisting');
Route::get('/blacklisting/new', 'App\Http\Controllers\Admin\IpListingController@newBlacklisting')->name('blacklisting_new');
Route::post('/blacklisting/save', 'App\Http\Controllers\Admin\IpListingController@saveBlacklisting')->name('blacklisting_save');

Route::get('/audits', 'App\Http\Controllers\Admin\AuditTrailsController@index')->name('audits');
// Route::get('/blacklisting/new', 'App\Http\Controllers\Admin\IpListingController@newBlacklisting')->name('blacklisting_new');
// Route::post('/blacklisting/save', 'App\Http\Controllers\Admin\IpListingController@saveBlacklisting')->name('blacklisting_save');

Route::get('/statistics', 'App\Http\Controllers\Admin\StatisticsController@index')->name('statistics');
Route::get('/statistics/data', 'App\Http\Controllers\Admin\StatisticsController@get_data')->name('statistics_data');

Route::get('/public_stats', 'App\Http\Controllers\Admin\StatisticsController@public_stats_index')->name('public_stats');
Route::get('/public_stats/data', 'App\Http\Controllers\Admin\StatisticsController@public_stats_get_data')->name('public_stats_data');

Route::get('/results_graph', 'App\Http\Controllers\Admin\ResultsController@results_graph_index')->name('results_graph_index');
Route::get('/results_graph/cat_data', 'App\Http\Controllers\Admin\ResultsController@results_graph_get_categories')->name('results_graph_cat_data');
Route::post('/results_graph/cat_results', 'App\Http\Controllers\Admin\ResultsController@results_graph_category_results')->name('results_graph_category_results');
Route::get('/results_graph/data', 'App\Http\Controllers\Admin\ResultsController@results_graph_get_data')->name('results_graph_get_data');


Route::get('/results', 'App\Http\Controllers\Admin\ResultsController@index')->name('results');
Route::post('/results/get_result', 'App\Http\Controllers\Admin\ResultsController@get_result')->name('get_result');
Route::post('/results/download_result', 'App\Http\Controllers\Admin\ResultsController@download_result')->name('download_result');


